package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.ContactsInformation;

import java.util.ArrayList;

/**
 * Created by Vishal Chugh on 024 , 24/Feb/2016.
 */
public class ContactListViewAdapter extends BaseAdapter {

    private Context mContext;
    //    private OnClickListener callback;
    private ArrayList<ContactsInformation> mContactList;

    public ContactListViewAdapter(Context mContext, ArrayList<ContactsInformation> mContactList) {
        this.mContext = mContext;
        this.mContactList = mContactList;
//        this.callback=callback;
    }

    @Override
    public int getCount() {
        return mContactList.size();
    }

    @Override
    public Object getItem(int position) {
        return mContactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view=null;
        if(convertView==null)
        {
            view = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_row_get_contact,parent,false);
        }
        else
        view=convertView;

        ((TextView)view.findViewById(R.id.tv_contact_name)).setText(mContactList.get(position).getmPersonName());
        ((TextView)view.findViewById(R.id.tv_contact_number)).setText(mContactList.get(position).getmPersonPhoneNumber());

        ImageButton ibEdit = (ImageButton) view.findViewById(R.id.ib_edit_contact);

        ibEdit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });
        return view;
    }
}
