package com.medlifeinfinity.medlifeinfinity.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.medlifeinfinity.medlifeinfinity.fragments.LoginFragment;
import com.medlifeinfinity.medlifeinfinity.fragments.RegisterFragment;

public class LoginPageAdapter extends FragmentStatePagerAdapter {
    public final static String REGISTER = "Register";
    public final static String LOGIN = "Login";
    int mPageCount;

    public LoginPageAdapter(FragmentManager fm, int num) {
        super(fm);
        mPageCount = num;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment mPageFragment = null;
        switch (position) {
            case 0:
                mPageFragment = new LoginFragment();
                break;
            case 1:
                mPageFragment = new RegisterFragment();
                break;
        }
        return mPageFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String mTitilString = null;
        switch (position) {
            case 0:
                mTitilString = LOGIN;
                break;
            case 1:
                mTitilString = REGISTER;
                break;
        }
        return mTitilString;
    }
}