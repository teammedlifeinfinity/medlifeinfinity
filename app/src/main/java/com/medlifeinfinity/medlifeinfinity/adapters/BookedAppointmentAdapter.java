package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.DoctorSlotMakerActivity;
import com.medlifeinfinity.medlifeinfinity.activities.findDoctors.DoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.customClasses.Appointment;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Vaio on 5/17/2016.
 */
public class BookedAppointmentAdapter extends BaseAdapter {
    Context mContext;
    String mActivityName;

    public BookedAppointmentAdapter(Context applicationContext, String activityName) {
        mContext = applicationContext;
        this.mActivityName = activityName;
    }

    public ArrayList<Appointment> getmAppointmentList() {
        return mAppointmentList;
    }

    ArrayList<Appointment> mAppointmentList = new ArrayList<Appointment>();

    public BookedAppointmentAdapter() {
    }

    public BookedAppointmentAdapter(Context mContext, JSONArray appointmetnArray) {
        this.mContext = mContext;
        for (int i = 0; i < appointmetnArray.length(); i++) {
            try {
                mAppointmentList.add(new Appointment(appointmetnArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getCount() {
        return mAppointmentList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAppointmentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_booked_appointment_row_item, parent, false);
        } else {
            view = convertView;
        }
        ((TextView) (view.findViewById(R.id.tv_time_slots_booked_appointment_row_item))).setText(mAppointmentList.get(position).getmSlotTime());
        if (mAppointmentList.get(position).getmAppointmentStatus()) {
            view.setBackgroundColor(Color.RED);
            ((TextView) (view.findViewById(R.id.tv_status_booked_appointment_row_item))).setText("Booked");
        } else {
            view.setBackgroundColor(Color.GREEN);
            ((TextView) (view.findViewById(R.id.tv_status_booked_appointment_row_item))).setText("Free");
        }

        String date=mAppointmentList.get(position).getmBookingDate();
        date=date.substring(6)+"-"+date.substring(4,6)+"-"+date.substring(0,4);
        ((TextView) (view.findViewById(R.id.tv_date_booked_appointment_row_item))).setText(date);

        ImageButton ib_overflow_menu = (ImageButton) view.findViewById(R.id.ib_overflow_menu_booked_appointment_row_item);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });

        ib_overflow_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });
        return view;
    }


}
