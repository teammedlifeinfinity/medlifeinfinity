package com.medlifeinfinity.medlifeinfinity.adapters;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;


public class ImageSlideAdapter extends PagerAdapter {
    String[] mViewImages;
    Context mContext;
    private LayoutInflater mLayoutInflater;
    private NetworkImageView niv_mOpenImage;
    private ImageLoader mImageLoader;
    private int mPosition;

    public ImageSlideAdapter(Context mContext, String[] mViewImages, int mPosition) {
        this.mViewImages = mViewImages;
        this.mContext = mContext;
     //   this.mPosition = mPosition;
    }

    @Override
    public int getCount() {
        return mViewImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = mLayoutInflater.inflate(R.layout.custom_layout_swipe_image, container, false);
        niv_mOpenImage = (NetworkImageView) item_view.findViewById(R.id.niv_open_image_layout_swipe_image);
        mImageLoader = VolleySingleton.getInstance(this.mContext).getImageLoader();
        niv_mOpenImage.setImageUrl("http://mani11-001-site1.ctempurl.com/images/" + mViewImages[position], mImageLoader);
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

