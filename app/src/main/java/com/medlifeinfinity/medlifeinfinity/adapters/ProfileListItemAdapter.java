package com.medlifeinfinity.medlifeinfinity.adapters;

/**
 * Created by Vaio on 4/23/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.ViewAppointmentActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileListItemAdapter extends BaseAdapter {

    private static final String TAG = ProfileListItemAdapter.class.getSimpleName();
    Context mContext;
    String mRollType;
    ArrayList<User> mUserProfilesId=new ArrayList<User>();
    private String mActivityName;

    public ProfileListItemAdapter(Context mContext, String mRollType,String activityName ) {
        this.mContext = mContext;
        this.mRollType = mRollType;
        this.mActivityName=activityName;
    }

    public ProfileListItemAdapter() {
    }

    public void addProfile(User user)
    {
        mUserProfilesId.add(user);
    }

    @Override
    public int getCount() {
        return mUserProfilesId.size();
    }

    @Override
    public Object getItem(int position) {
        return mUserProfilesId.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View cardView = null;
        if (convertView == null) {
            cardView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_profile, parent, false);
        } else {
            cardView = convertView;
        }
        ((TextView) cardView.findViewById(R.id.tv_name_card_view_profile)).setText(mUserProfilesId.get(position).getmName());
        ((TextView) cardView.findViewById(R.id.tv_address_card_view_profile)).setText(mUserProfilesId.get(position).getmAddress());
        Button btn_call = (Button) cardView.findViewById(R.id.btn_call_doctor_card_view_profile);
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((mUserProfilesId.get(position).getmContactNumber()) != null) {
                    Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mUserProfilesId.get(position).getmContactNumber().toString()));
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        mContext.startActivity(in);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Log.i("test", ex.toString());
                        Toast.makeText(mContext, "yourActivity is not founded", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "phone number is not available.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        CircleImageView imageView=((CircleImageView)cardView.findViewById(R.id.iv_profile_photo_card_view_profile));

        if(mUserProfilesId.get(position).getmProfliePhoto()!="NA") {
            String imageURL = ApiCall.IMAGE_URL.toString() + mUserProfilesId.get(position).getmProfliePhoto();
            ImageLoader mImageLoader = VolleySingleton.getInstance(mContext).getImageLoader();
            mImageLoader.get(imageURL, ImageLoader.getImageListener(imageView, R.drawable.ic_loading_animation, R.drawable.ic_no_image_found));
        }


        ImageButton ib_overflowMenu = (ImageButton) cardView.findViewById(R.id.ib_overflow_menu_card_view_profile);
        ib_overflowMenu.setVisibility(View.GONE);
        TextView tv_qualification = (TextView) cardView.findViewById(R.id.tv_qualification_card_view_profile);
        TextView tv_availability = (TextView) cardView.findViewById(R.id.tv_availability_card_view_profile);
        tv_availability.setText(mUserProfilesId.get(position).getmEmail());
        Button btn_bookAppointment = (Button) cardView.findViewById(R.id.btn_book_appointment_card_view_profile);
        btn_bookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext.getApplicationContext(), ViewAppointmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("doctorId",mUserProfilesId.get(position).getmUserId());
                intent.putExtra("class",mActivityName);
                mContext.startActivity(intent);
            }
        });

        if (mRollType.equals(User.PATIENT_ROLE)) {
            btn_bookAppointment.setVisibility(View.GONE);
            tv_availability.setVisibility(View.GONE);
            tv_qualification.setVisibility(View.GONE);
        }
        if (mRollType.equals(User.DOCTOR_ROLE)) {
            tv_qualification.setText(mUserProfilesId.get(position).getmQualification());
            tv_availability.setText("NA");
        }

        return cardView;
    }
}
