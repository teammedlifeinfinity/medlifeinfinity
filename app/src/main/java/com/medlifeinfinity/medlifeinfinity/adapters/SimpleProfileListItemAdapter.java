package com.medlifeinfinity.medlifeinfinity.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.myDoctorList.MyDoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.myPatientList.MyPatientListActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Vaio on 4/23/2016.
 */
public class SimpleProfileListItemAdapter extends BaseAdapter {

    private static final String TAG = SimpleProfileListItemAdapter.class.getSimpleName();
    Context mContext;
    LinearLayout mRootLayout;

    public ArrayList<User> mUserProfilesId = new ArrayList<User>();
    private Snackbar mSnkBar;
    private ProgressDialog progressDialog;

    public SimpleProfileListItemAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public SimpleProfileListItemAdapter() {
    }

    public void addProfile(User user) {
        mUserProfilesId.add(user);
    }

    @Override
    public int getCount() {
        return mUserProfilesId.size();
    }

    @Override
    public Object getItem(int position) {
        return mUserProfilesId.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View cardView = null;
        if (convertView == null) {
            cardView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_simple_profile, parent, false);
        } else {
            cardView = convertView;
        }
        ((TextView) cardView.findViewById(R.id.tv_name_card_view_simple_profile)).setText(mUserProfilesId.get(position).getmName());
        ((TextView) cardView.findViewById(R.id.tv_address_card_view_simple_profile)).setText(mUserProfilesId.get(position).getmAddress());

        final ImageButton ib_overflowMenu = (ImageButton) cardView.findViewById(R.id.ib_overflow_menu_card_view_simple_profile);

        CircleImageView imageView=((CircleImageView)cardView.findViewById(R.id.iv_profile_photo_card_view_simple_profile));

        if(mUserProfilesId.get(position).getmProfliePhoto()!="NA") {
            String imageURL = ApiCall.IMAGE_URL.toString() + mUserProfilesId.get(position).getmProfliePhoto();
            ImageLoader mImageLoader = VolleySingleton.getInstance(mContext).getImageLoader();
            mImageLoader.get(imageURL, ImageLoader.getImageListener(imageView, R.drawable.ic_loading_animation, R.drawable.ic_no_image_found));
        }

        ib_overflowMenu.setVisibility(View.VISIBLE);
        ib_overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, ib_overflowMenu);
                popupMenu.inflate(R.menu.menu_remove);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (mContext.getClass().getSimpleName().equals(MyDoctorListActivity.class.getSimpleName())) {
                            if (item.getItemId() == R.id.menu_remove) {
//                                mRootLayout=(LinearLayout)parent.findViewById(R.id.root_doctor_list_activity);
                                removeRelation(mUserProfilesId.get(position).getmUserId(), User.getInstance().getmUserId());
                                mUserProfilesId.remove(position);
                                notifyDataSetChanged();
                            }
                        } else if (mContext.getClass().getSimpleName().equals(MyPatientListActivity.class.getSimpleName())) {
                            if (item.getItemId() == R.id.menu_remove) {
//                                mRootLayout=(LinearLayout)parent.findViewById(R.id.root_my_patient_list_activity);
                                removeRelation(User.getInstance().getmUserId(), mUserProfilesId.get(position).getmUserId());
                                mUserProfilesId.remove(position);
                                notifyDataSetChanged();
                            }
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }

        });

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });

        ib_overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        });
        return cardView;
    }

    private void removeRelation(final String doctorId, final String patientId) {
        String url = ApiCall.HOST_URL + "DeleteFromRelationship";

        JSONObject object = new JSONObject();
        try {
            object.put("Patient_Id", patientId);
            object.put("Doctor_id", doctorId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                String temp = "";
                try {
                    temp = response.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (temp.equals("deleted")) {
                    Toast.makeText(mContext, "relation deleted", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                Toast.makeText(mContext,"removed",Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(mContext).getRequestQueue().add(request);
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
