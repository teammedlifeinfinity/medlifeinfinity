package com.medlifeinfinity.medlifeinfinity.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.Message;

public class ChatAdapter extends BaseAdapter {

    Context messageContext;
    List<Message> messageList;

    public ChatAdapter(Context context, List<Message> messages){
        messageList = messages;
        messageContext = context;
    }

    public ChatAdapter() {
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder;
        if (convertView == null){
            LayoutInflater messageInflater = (LayoutInflater) messageContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = messageInflater.inflate(R.layout.custom_chat_row, null);
            holder = new MessageViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.img_thumbnail_custom_chat_row);
            holder.senderView = (TextView) convertView.findViewById(R.id.tv_message_sender_custom_chat_row);
            holder.bodyView = (TextView) convertView.findViewById(R.id.tv_message_body_custom_chat_row);
            holder.Time=(TextView)convertView.findViewById(R.id.tv_time_custom_chat_row);


            convertView.setTag(holder);
        } else {
            holder = (MessageViewHolder) convertView.getTag();
        }

        Message message = (Message) getItem(position);

        holder.bodyView.setText(message.text);
        holder.senderView.setText(message.name);
        holder.Time.setText(getdata(message.time));
        return convertView;
    }

    private String getdata(long time)
    { SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return (formatter.format(calendar.getTime()));
    }

    public void add(Message message){
        messageList.add(message);
        notifyDataSetChanged();
    }

    private static class MessageViewHolder {
        public ImageView thumbnailImageView;
        public TextView senderView;
        public TextView bodyView;
        public TextView Time;
    }
}