package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.PillsReminderInformation;


import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class PillReminderListViewAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PillsReminderInformation> pillsReminderInformationList;


    public PillReminderListViewAdapter(Context context, ArrayList<PillsReminderInformation> pillsReminderInformationList) {
        this.mContext = context;
        this.pillsReminderInformationList = pillsReminderInformationList;
    }

    @Override
    public int getCount() {
        return pillsReminderInformationList.size();
    }

    @Override
    public Object getItem(int position) {
        return pillsReminderInformationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.pills_listing_custom_layout, parent, false);
        } else
            view = convertView;

        ((TextView) view.findViewById(R.id.tv_med_name)).setText(pillsReminderInformationList.get(position).getmMedicineName());
        ((TextView) view.findViewById(R.id.tv_med_next_time)).setText(pillsReminderInformationList.get(position).getmMedicineNextTime());
        ((TextView) view.findViewById(R.id.tv_dosage)).setText(pillsReminderInformationList.get(position).getmMedicineDosage());
        int color = pillsReminderInformationList.get(position).getmMedicineColor();
        ((CircleImageView) view.findViewById(R.id.iv_med_image)).setBorderColor(color);

        return view;
    }
}
