package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

/**
 * Created by Vaio on 4/18/2016.
 */
public class ReactionHistoryListAdapter extends BaseAdapter {

    User mUserProfile;
    private Context mContext;
    private String[] mMedicine_Name = null;
    private String[] mDescription = null;

    public ReactionHistoryListAdapter() {

    }

    public ReactionHistoryListAdapter(Context mContext, String[] mMedicine_Name, String[] mDescription) {
        this.mContext = mContext;
        this.mMedicine_Name = mMedicine_Name;
        this.mDescription = mDescription;
        mUserProfile.getInstance();
    }

    @Override
    public int getCount() {
        return mDescription.length;
    }

    @Override
    public Object getItem(int position) {
        return mDescription[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_reaction_details_row, parent, false);
        } else {
            view = convertView;
        }
        ((TextView) (view.findViewById(R.id.tv_medicine_name_reaction_details_row_card_view))).setText(mMedicine_Name[position]);
        ((TextView) (view.findViewById(R.id.tv_reaction_detail_reaction_details_row_card_view))).setText(mDescription[position]);

        String letter = String.valueOf(mMedicine_Name[position].charAt(0));
        int color = ColorGenerator.MATERIAL.getColor(getItem(position));
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(letter, color);
        ((ImageView) view.findViewById(R.id.img_background_reaction_details_row_card_view)).setImageDrawable(drawable);

        final ImageButton ib_overflow_menu = (ImageButton) view.findViewById(R.id.ib_overflow_menu__reaction_details_row_card_view);
        ib_overflow_menu.setVisibility(View.GONE);
        ib_overflow_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(mContext, ib_overflow_menu);
                popupMenu.inflate(R.menu.menu_reaction_details_list_item_overflow);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu_edit) {
                            Toast.makeText(mContext, "Edit", Toast.LENGTH_SHORT).show();
                        } else if (item.getItemId() == R.id.menu_delete) {

                            Toast.makeText(mContext, "delete", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });
        return view;
    }
}
