package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

public class ViewPrescriptionListAdapter extends BaseAdapter {
    public static LayoutInflater inflater = null;
    //   ArrayList<MedicineDetails> result;
    Context context;
    User mUserProfile;
    private String[] MedicineName;
    private String[] Quantity;
    private String[] Dose;
    private String[] CreatedBy;
    private String[] CreatedAt;
    private String[] PatientId;
    private Boolean[] Noon;
    private Boolean[] Morning;
    private Boolean[] Night;


    public ViewPrescriptionListAdapter() {
    }

    public ViewPrescriptionListAdapter(Context context, String[] MedicineName, String[] Quantity, String[] Dose, String[] CreatedBy, String[] CreatedAt, String[] PatientId, Boolean[] noon, Boolean[] morning, Boolean[] night) {
        this.context = context;
        this.MedicineName = MedicineName;
        this.Quantity = Quantity;
        this.Dose = Dose;
        this.CreatedBy = CreatedBy;
        this.CreatedAt = CreatedAt;
        this.PatientId = PatientId;
        this.Night=night;
        this.Noon=noon;
        this.Morning=morning;
        mUserProfile = User.getInstance();
    }


    @Override
    public int getCount() {
        return MedicineName.length;
    }

    @Override
    public Object getItem(int position) {
        return MedicineName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
        String temp="";
        if (convertView == null) {
            row = ((LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_view_prescription_list, parent, false);
        } else {
            row = convertView;
        }

        ((TextView) row.findViewById(R.id.tv_medicine_name_custom_row_view_prescription_list)).setText(MedicineName[position]);
        ((TextView) row.findViewById(R.id.tv_quantity_custom_row_view_prescription_list)).setText(Quantity[position]);
        if (Morning[position]) {
            temp=temp+"Morning  ";
        }
        if (Noon[position])
        {
            temp=temp+"Afternoon  ";
        }
        if(Night[position])
        {
            temp=temp+"Night  ";
        }


        ((TextView) row.findViewById(R.id.tv_dose_custom_row_view_prescription_list)).setText(temp);
        String letter = String.valueOf(((TextView) row.findViewById(R.id.tv_medicine_name_custom_row_view_prescription_list)).getText().charAt(0));
        ColorGenerator generator = ColorGenerator.MATERIAL;
        // generate random color
        int color = generator.getColor(getItem(position));

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(letter, color);

        ((ImageView) row.findViewById(R.id.iv_first_letter_custom_row_view_prescription_list)).setImageDrawable(drawable);
        final ImageButton ib_overflowMenu = (ImageButton) row.findViewById(R.id.ib_overflow_menu_custom_row_view_prescription_list);

        ib_overflowMenu.setVisibility(View.GONE);
        ib_overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, ib_overflowMenu);
                popupMenu.inflate(R.menu.menu_reaction_details_list_item_overflow);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu_edit) {
                            Toast.makeText(context, "Edit", Toast.LENGTH_SHORT).show();
                        } else if (item.getItemId() == R.id.menu_delete) {

                            Toast.makeText(context, "delete", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                });

                popupMenu.show();
            }
        });

        return row;
    }


}



