package com.medlifeinfinity.medlifeinfinity.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.medlifeinfinity.medlifeinfinity.R;

import java.util.List;

/**
 * Created by Vishal Chugh on 011 , 11/Apr/2016.
 */
public class DashboardGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mDashboardTitleList;
    private List<Integer> mDashboardIconList;

    public DashboardGridViewAdapter(Context mContext, List<String> titleList, List<Integer> iconList) {
        this.mContext = mContext;
        this.mDashboardTitleList = titleList;
        this.mDashboardIconList = iconList;
    }

    @Override
    public int getCount() {
        return mDashboardTitleList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDashboardTitleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = null;
        int[] lightColor = mContext.getResources().getIntArray(R.array.light_colors);
        if (convertView == null) {
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_dashboard_grid_view_item, parent, false);
        } else {
            view = convertView;
        }
        ((TextView) (view.findViewById(R.id.tv_grid_view_item_title))).setText(mDashboardTitleList.get(position).toString());
        ((ImageView) (view.findViewById(R.id.iv_grid_view_item_icon))).setImageResource(mDashboardIconList.get(position));
        ((LinearLayout) (view.findViewById(R.id.ll_grid_view_item_background))).setBackgroundColor(lightColor[position]);
        return view;
    }
}
