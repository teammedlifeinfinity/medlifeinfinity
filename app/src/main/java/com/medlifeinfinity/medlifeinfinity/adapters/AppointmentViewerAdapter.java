package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.DoctorSlotMakerActivity;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.ViewAppointmentActivity;
import com.medlifeinfinity.medlifeinfinity.activities.findDoctors.DoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.customClasses.Appointment;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Vaio on 4/15/2016.
 */
public class AppointmentViewerAdapter extends BaseAdapter {
    Context mContext;
    String mRoleType;
    String mActivityName;

    public AppointmentViewerAdapter(Context applicationContext,String activityName) {
        mContext=applicationContext;
        this.mActivityName=activityName;
    }

    public ArrayList<Appointment> getmAppointmentList() {
        return mAppointmentList;
    }

    ArrayList<Appointment> mAppointmentList = new ArrayList<Appointment>();

    public AppointmentViewerAdapter() {
    }

    public AppointmentViewerAdapter(Context mContext, JSONArray appointmetnArray, String simpleName) {
        this.mContext = mContext;
        this.mActivityName=simpleName;
        for(int i=0;i<appointmetnArray.length();i++)
        {
            try {
                mAppointmentList.add(new Appointment(appointmetnArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void addAppointment(Appointment appointment) {
        mAppointmentList.add(appointment);
    }

    @Override
    public int getCount() {
        return mAppointmentList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAppointmentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.card_view_appointment_doctor_list_item, parent, false);
        } else {
            view = convertView;
        }
        ((TextView) (view.findViewById(R.id.tv_time_slots_appointment_doctor_list_item_card_view))).setText(mAppointmentList.get(position).getmSlotTime());
        if(mAppointmentList.get(position).getmAppointmentStatus()) {
            view.setBackgroundColor(Color.RED);
            ((TextView) (view.findViewById(R.id.tv_status_appointment_doctor_list_item_card_view))).setText("Booked");
        }
        else {
            view.setBackgroundColor(Color.GREEN);
            ((TextView) (view.findViewById(R.id.tv_status_appointment_doctor_list_item_card_view))).setText("Free");
        }
        ImageButton ib_overflow_menu = (ImageButton) view.findViewById(R.id.ib_overflow_menu_appointment_doctor_list_item_card_view);

        if (mActivityName.equals(DoctorSlotMakerActivity.class.getSimpleName())) {
            ib_overflow_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(mContext,v);
                    popupMenu.inflate(R.menu.menu_remove);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            if (item.getItemId() == R.id.menu_remove) {
                                mAppointmentList.remove(position);
                                notifyDataSetChanged();
                            }
                            return true;
                        }
                    });
                    popupMenu.show();
                }
            });
        }
        if(mActivityName.equals(DoctorListActivity.class.getSimpleName()))
        {
            ib_overflow_menu.setVisibility(View.INVISIBLE);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView)parent).performItemClick(v, position, 0);
            }
        });

        ib_overflow_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListView)parent).performItemClick(v, position, 0);
            }
        });
        return view;
    }


}
