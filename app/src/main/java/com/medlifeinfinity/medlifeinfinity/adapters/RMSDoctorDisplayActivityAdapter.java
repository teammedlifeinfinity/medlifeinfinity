package com.medlifeinfinity.medlifeinfinity.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.medlifeinfinity.medlifeinfinity.R;

/**
 * Created by HUSSAIN'S on 4/10/2016.
 */
public class RMSDoctorDisplayActivityAdapter extends BaseAdapter {

    public String[] Description;
    public String[] MessageType;
    public String[] Id;
    private Context context;
    private LayoutInflater inflater;


    public RMSDoctorDisplayActivityAdapter(Context context, String[] description, String[] messageType, String[] id) {
        this.context = context;
        this.Description = description;
        this.MessageType = messageType;
        this.Id=id;
    }

    @Override
    public int getCount() {
        return this.Id.length;
    }

    @Override
    public Object getItem(int position) {
        return Id.length;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        class ViewHolder {
            TextView  tv_Complain, tv_MessageType;
        }

        ViewHolder holder = null;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            try {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.card_view_rms_doctor_display_list, null);
                holder.tv_Complain = (TextView) convertView.findViewById(R.id.tv_description_card_view_rms);
                holder.tv_MessageType = (TextView) convertView.findViewById(R.id.tv_message_type_card_view_rms);
                convertView.setTag(holder);
            } catch (Exception e) {
                Log.d("EXCEPTION RAISE", e.toString());
            }

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {
            holder.tv_MessageType.setText(MessageType[position]);
            holder.tv_Complain.setText(Description[position]);
        } catch (Exception e) {
            Log.d("EXCEPTION RAISE2", e.toString());
        }
        return convertView;
    }

}
