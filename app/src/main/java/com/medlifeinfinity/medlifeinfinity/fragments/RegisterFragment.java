package com.medlifeinfinity.medlifeinfinity.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.LoginAndRegisterActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.Validate;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.AlertDialogListOfAddressInNearBy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class RegisterFragment extends Fragment implements OnConnectionFailedListener, LocationListener, GoogleApiClient.ConnectionCallbacks {

    public final String TAG = RegisterFragment.class.getCanonicalName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private TextInputLayout til_name, til_contactNumber, til_email, til_password;
    private EditText et_name, et_contactNumber, et_email, et_password;
    private RadioButton rb_doctor, rb_patient, rb_chemist;
    private Button btn_register;
    private RadioGroup rg_radioGroup;
    private User mUserProfile;

    private ProgressDialog progressDialog;
    private AlertDialog.Builder specializationPickerBuilder;
    private AlertDialog optionDialog;
    private Spinner spn_specializationSpinner;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private String mLatitude;
    private String mLongitude;
    private LinearLayout mRootLayout;
    private Snackbar mSnkBar;
    public GoogleApiClient mGoogleApiClientInNearBy;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0;
    private double currentLongitude = 0;


    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();


        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        createViewsObject(view);
        setListeners();
        return view;
    }

    private void createViewsObject(View view) {
        mRootLayout = (LinearLayout) view.findViewById(R.id.root_register_fragment);
        til_name = (TextInputLayout) view.findViewById(R.id.til_name_fragment_register);
        til_password = (TextInputLayout) view.findViewById(R.id.til_password_fragment_register);
        til_email = (TextInputLayout) view.findViewById(R.id.til_email_fragment_register);
        til_contactNumber = (TextInputLayout) view.findViewById(R.id.til_contactNumber_fragment_register);
        et_name = (EditText) view.findViewById(R.id.et_name_fragment_register);
        et_email = (EditText) view.findViewById(R.id.et_email_fragment_register);
        et_contactNumber = (EditText) view.findViewById(R.id.et_contactNumber_fragment_register);
        et_password = (EditText) view.findViewById(R.id.et_password_fragment_register);
        rb_doctor = (RadioButton) view.findViewById(R.id.rb_doctor_fragment_register);
        rb_chemist = (RadioButton) view.findViewById(R.id.rb_chemist_fragment_register);
        rb_patient = (RadioButton) view.findViewById(R.id.rb_patient_fragment_register);
        btn_register = (Button) view.findViewById(R.id.btn_register_fragment_register);
        rg_radioGroup = (RadioGroup) view.findViewById(R.id.rg_radiogroup_fragment_register);
        spn_specializationSpinner = (Spinner) view.findViewById(R.id.spn_specialization_fragment_register);
    }

    private void setListeners() {

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(getActivity().getApplicationContext())) {
                    if (validateFields()) {
                        registerWithApi(getUserDetails());
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Internet is not connected", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rb_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rb_doctor.isChecked()) {
                    spn_specializationSpinner.setVisibility(View.VISIBLE);
                } else
                    spn_specializationSpinner.setVisibility(View.GONE);
            }
        });
        rb_chemist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spn_specializationSpinner.setVisibility(View.GONE);
            }
        });
        rb_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spn_specializationSpinner.setVisibility(View.GONE);
            }
        });
    }


    private boolean validateFields() {
        if (!(Validate.isValid(et_name.getText().toString(), Validate.NAME_PATTERN))) {
            et_name.setError("entered name is not valid");
            return false;
        }
        if (!(Validate.isValid(et_contactNumber.getText().toString(), Validate.PHONE_NUMBER_PATTERN))) {
            et_contactNumber.setError("please enter a valid indian number");
            return false;
        }
        if (!Validate.isValid(et_email.getText().toString(), Validate.EMAIL_PATTERN)) {
            et_email.setError(getActivity().getString(R.string.error_email_address_not_valid));
            return false;
        }
        if (!(Validate.isValid(et_password.getText().toString(), Validate.PASSWORD_PATTERN))) {
            et_password.setError("password should contain a uppercase ,a lower case, a numeric and ,a symbol.");
            return false;
        }
        if ((rg_radioGroup.getCheckedRadioButtonId() <= 0)) {
            Toast.makeText(getActivity(), "Please Select Doctor either Patient", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void registerWithApi(final JSONObject userDetails) {

        String url = ApiCall.HOST_URL.toString() + "InsertSignupDetail";
        createProgressDialog(getActivity(), "registering user", false);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, userDetails, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    if (parseResponse(response)) {
                        Toast.makeText(getActivity(), "User Registered", Toast.LENGTH_SHORT).show();
                        clearAllFields();
                        ((LoginAndRegisterActivity) getActivity()).getmViewPager().setCurrentItem(0, true);
                    } else {
                        Toast.makeText(getActivity(), "User already Registered", Toast.LENGTH_SHORT).show();
                    }
                    hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar = Snackbar.make(mRootLayout, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        registerWithApi(userDetails);
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private void clearAllFields() {
        et_name.setText("");
        et_password.setText("");
        et_contactNumber.setText("");
        et_email.setText("");
    }

    private boolean parseResponse(JSONObject response) throws JSONException {
        int id = response.getInt("status1");
        if (id == 0)
            return false;
        return true;
    }


    private JSONObject getUserDetails() {
        JSONObject userInfo = new JSONObject();
        try {
            onBoardScreen();
            userInfo.put("Contact_No", et_contactNumber.getText().toString());
            userInfo.put("Email", et_email.getText().toString());
            sendFirstTimeEmail(et_email.getText().toString(), et_password.getText().toString());
            userInfo.put("Password", et_password.getText().toString());
            if (rb_doctor.isChecked()) {
                userInfo.put("Specialization", spn_specializationSpinner.getSelectedItem().toString());
            } else {

                userInfo.put("Specialization", "null");
            }
            if (rb_doctor.isChecked()) {
                userInfo.put("Roll_Id", User.DOCTOR_ROLE);
            } else if (rb_patient.isChecked()) {
                userInfo.put("Roll_Id", User.PATIENT_ROLE);
            }
            userInfo.put("Name", et_name.getText().toString());

            userInfo.put("Latitude", currentLatitude);
            userInfo.put("Longitude", currentLongitude);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, userInfo.toString());
        return userInfo;
    }

    private void onBoardScreen() {
        SharedPreferences getPrefs = PreferenceManager
                .getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor e = getPrefs.edit();
        //  Edit preference to make it false because we don't want this to run again
        e.putBoolean("firstStart", false);
        //  Apply changes
        e.apply();
    }

    private void sendFirstTimeEmail(String mail, String pwd) {
        Session session = createSessionObject();

        try {
            String email = mail;
            String subject = "Welcome to MedLife Infinity";
            String messageBody = "Thank you for Signing up to our Service\nMedLife Infinity welcomes you to the Marvelous World of Medical Help.Hope you don't need our app, But in case of Emergency, You need anything,We are Happy to Help.\nYour User Details are:\n" +
                    "\t\tUser id :" + email + "\n\t\tPassword : " + pwd + "\n\n\nThank You,\nThe MedLife Infinity Team.";
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {

            e.printStackTrace();
        } catch (MessagingException e) {

            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("info.medlifeinfinity@gmail.com", "MedLife Infinity"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        // TODO: 4/27/2016 put the account password from which you wana send email
        final String password = "medlife@INFOTECH";
        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("info.medlifeinfinity@gmail.com", password);
            }
        });
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = ProgressDialog.show(ForgetPasswordActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
//                Toast.makeText(getApplicationContext(), e.getStackTrace().toString(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

//            Toast.makeText(getContext().getApplicationContext(), currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }


    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

//        Toast.makeText(getContext().getApplicationContext(), currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }


}
