package com.medlifeinfinity.medlifeinfinity.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.activities.ForgetPasswordActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.Validate;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    private Button btn_Login, btn_ForgetPassword;
    private EditText et_email, et_password;
    private TextInputLayout til_eamil, til_password;
    ;
    private User mUserProfile;
    private ProgressDialog progressDialog;
    private LinearLayout mRootLayout;
    private Snackbar mSnkBar;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        createViewsObject(view);
        setListeners();
        return view;
    }

    private void createViewsObject(View view) {
        btn_Login = (Button) view.findViewById(R.id.btn_login_fragment_login);
        btn_ForgetPassword = (Button) view.findViewById(R.id.btn_forget_password_fragment_login);
        et_email = (EditText) view.findViewById(R.id.et_email_fragment_login);
        et_password = (EditText) view.findViewById(R.id.et_password_fragment_login);
        til_eamil = (TextInputLayout) view.findViewById(R.id.til_email_fragment_login);
        til_password = (TextInputLayout) view.findViewById(R.id.til_password_fragment_login);
        mRootLayout= (LinearLayout) view.findViewById(R.id.root_login_fragment);
    }

    private void setListeners() {
        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(getActivity().getApplicationContext())) {
                    if (validateFields()) {
                        loginWithApi(et_email.getText().toString(), et_password.getText().toString());
                    }
                }
                else {
                    Toast.makeText(getActivity().getApplicationContext(),"Internet is not connected",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btn_ForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ForgetPasswordActivity.class));
            }
        });

    }

    private boolean validateFields() {
        boolean isValid;
        if (Validate.isValid(et_email.getText().toString(), Validate.EMAIL_PATTERN)) {
            isValid = true;
        } else {
            isValid = false;
            et_email.setError(getActivity().getString(R.string.error_email_address_not_valid));
        }
        return isValid;
    }


    private void loginWithApi(final String userEmail, final String password) {
        String url = ApiCall.HOST_URL.toString() + "LoginAccessToUser";
        createProgressDialog(getActivity(), "Logging in", false);
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("Email", userEmail);
            jsonBody.put("Password", password);
            Log.i(TAG, "userr puting " + jsonBody.toString() + "\n");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    getmUserProfile(paresResponse(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar= Snackbar.make(mRootLayout,"Something went wrong",Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loginWithApi(userEmail,password);
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });

        VolleySingleton.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private void getmUserProfile(final String userId) {
        String url = ApiCall.HOST_URL.toString() + "GetUserDetailswithId/" + userId;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    User.getUserFromJsonArray(response);
                    mUserProfile = User.getInstance();
                    User.storeInPreference(response.toString(), getActivity().getApplicationContext());
                    Log.i(TAG, mUserProfile.toString());
                    startActivity(new Intent(getActivity().getApplicationContext(), DashboardActivity.class));
                    hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mSnkBar= Snackbar.make(mRootLayout,"Something went wrong",Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getmUserProfile(userId);
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(getActivity()).getRequestQueue().add(request);
    }

    private String paresResponse(JSONObject response) throws JSONException {
        String userid = "";
        if (response.getString("status").equals("null")) {
            Toast.makeText(getActivity().getApplicationContext(), "pls provide correct Email and Password", Toast.LENGTH_SHORT).show();
            et_email.setText("");
            et_password.setText("");
            return null;
        } else {
            userid = response.getString("status1");
            Log.i(TAG, userid + "");
        }
        return userid;
    }


    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
