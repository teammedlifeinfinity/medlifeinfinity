package com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.medlifeinfinity.medlifeinfinity.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public  class FragmentTwoInNearBy extends android.support.v4.app.Fragment {

    static Context mContextFragmentTwoNearBy;
    static ListView mListViewFragmentTwoNearBy;
    static ArrayList<String> nearByNames;

    public FragmentTwoInNearBy() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContextFragmentTwoNearBy =getActivity();
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_two_in_nearby, container, false);
        // get the listview
       //expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

       mListViewFragmentTwoNearBy =(ListView)view.findViewById(R.id.mylistInFragmentTwoNearBy);

        // preparing list data
//        prepareListData();

//        listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);
//
//        // setting list adapter
//        expListView.setAdapter(listAdapter);
        mListViewFragmentTwoNearBy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Profile of  " + nearByNames.get(position) + " will be shown", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
    /*
    * Preparing the list data
    */
    public  static void prepareListData() {

        nearByNames =new ArrayList<>();
        nearByNames = FragmentOneInNearBy.getNearByNames();

        ArrayAdapter<String> mAdapter=new ArrayAdapter<String>(mContextFragmentTwoNearBy,android.R.layout.simple_list_item_1, nearByNames);
        mListViewFragmentTwoNearBy.setAdapter(mAdapter);

    }


}



