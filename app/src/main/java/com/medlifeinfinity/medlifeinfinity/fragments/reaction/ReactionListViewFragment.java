package com.medlifeinfinity.medlifeinfinity.fragments.reaction;


import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.reaction.ReactionHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.ReactionHistoryListAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReactionListViewFragment extends ListFragment {


    private static final String TAG = ReactionListViewFragment.class.getCanonicalName();
    private ProgressDialog progressDialog;
    private String[] mMedicine_Name;
    private String[] mDescription;
    private ReactionHistoryListAdapter mReactionHistoryListAdapter;
    private Snackbar mSnkBar;

    public ReactionListViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reaction_list_view, container, false);
        getReactionDetailsById(((ReactionHistoryActivity)getActivity()).mUserId);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void getReactionDetailsById(final String userId) {
        if (Utility.isNetworkAvailable(getActivity().getApplicationContext())) {

            String url = ApiCall.HOST_URL + "GetMedicineName/" + userId;

            createProgressDialog(getActivity(), "Loading... ", false);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e(TAG, response.toString());
                    try {
                        parseResponse(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar= Snackbar.make(((ReactionHistoryActivity)getActivity()).mRootLayout,"Something went wrong",Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getReactionDetailsById(userId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(getActivity().getApplicationContext()).getRequestQueue().add(request);
        }
        else {
            mSnkBar = Snackbar.make(((ReactionHistoryActivity)getActivity()).mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    getReactionDetailsById(userId);
                }
            });
            mSnkBar.show();
        }
    }

    private void parseResponse(JSONArray response) throws JSONException {
        if (response.length() != 0) {
            mMedicine_Name = new String[response.length()];
            mDescription = new String[response.length()];
            for (int i = 0; i < response.length(); i++) {
                mMedicine_Name[i] = response.getJSONObject(i).getString("Medicine_Name");
                mDescription[i] = response.getJSONObject(i).getString("Description");
            }
            mReactionHistoryListAdapter = new ReactionHistoryListAdapter(getActivity().getApplicationContext(), mMedicine_Name, mDescription);
            setListAdapter(mReactionHistoryListAdapter);
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity().getApplicationContext());
//                    builder.setTitle(mMedicine_Name[position]);
//                    builder.setMessage(mDescription[position]);
//                    builder.setCancelable(true);
//                    builder.show();
                }
            });
        } else {

        }
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
