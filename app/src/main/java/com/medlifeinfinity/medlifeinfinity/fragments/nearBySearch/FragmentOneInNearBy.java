package com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.FindDoctorOrChemistActivity;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.FindNearByPlacesActivity;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.TabTestInNearByActivity;

import java.util.ArrayList;


public class FragmentOneInNearBy extends android.support.v4.app.Fragment implements OnMapReadyCallback {

    public static String TAG=FragmentOneInNearBy.class.getSimpleName();
    double mCurrrentlatFragmentOne, mCurrentlngFragmentOne;
    private static GoogleMap mapFragmentOneNearBy;

    public static Context mContextFragMentOne;

    EditText editTextRangeFragmentOneInNearBy;
    Button btn_nearbyFragmentOneInNearBy;
    private int rangeFragmentOneInNearBy =0;


    SupportMapFragment mSupportMapFragmentFragmentOneNearBy;
    private FindNearByPlacesActivity findNearByPlaces;

    static ArrayList<String> nearByNames =new ArrayList<>();
    static ArrayList<String> nearByLng=new ArrayList<>();
    static ArrayList<String> nearByLat=new ArrayList<>();
    static ArrayList<String> nearByAddress=new ArrayList<>();

    private static LatLng currentLatLng;
    private static String currentPlace;


    public FragmentOneInNearBy() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        findNearByPlaces=new FindNearByPlacesActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContextFragMentOne =getContext();
        final View view = inflater.inflate(R.layout.fragment_one_in_near_by, container, false);
        btn_nearbyFragmentOneInNearBy = (Button) view.findViewById(R.id.btn_findDistanceInFragmentOneInNearBy);
        editTextRangeFragmentOneInNearBy = (EditText) view.findViewById(R.id.et_rangeFragmentOneInNearBy);

        btn_nearbyFragmentOneInNearBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String rangeEntered = editTextRangeFragmentOneInNearBy.getText().toString();
                boolean rangeError = false;

                if (rangeEntered.isEmpty()) {
                    Toast.makeText(getActivity(), "Please enter rangeFragmentOneInNearBy to search", Toast.LENGTH_SHORT).show();
                    rangeError = true;
                    editTextRangeFragmentOneInNearBy.setText("");
                    editTextRangeFragmentOneInNearBy.requestFocus();
                }


                if (!rangeError) {
                    rangeFragmentOneInNearBy = Integer.parseInt(rangeEntered);
                    nearByNames.clear();
                    nearByLat.clear();
                    nearByLng.clear();
                    nearByAddress.clear();

                    findNearByPlaces.fetchData(getContext(), rangeFragmentOneInNearBy);
                }
            }
        });



        mSupportMapFragmentFragmentOneNearBy = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapwhereInFragmentOneInNearBy);
        if (mSupportMapFragmentFragmentOneNearBy == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragmentFragmentOneNearBy = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.mapwhereInFragmentOneInNearBy, mSupportMapFragmentFragmentOneNearBy).commit();

        }

        if (mSupportMapFragmentFragmentOneNearBy != null) {
            mSupportMapFragmentFragmentOneNearBy.getMapAsync(this);

        }
       return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mapFragmentOneNearBy =googleMap;
       // googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        addMarkers();
        adjustCamera();

    }
    private void adjustCamera() {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mCurrrentlatFragmentOne, mCurrentlngFragmentOne))      // Sets the center of the mapFragmentOneNearBy to Mountain View
                .zoom(10)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mapFragmentOneNearBy.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void addMarkers()
    {
        mapFragmentOneNearBy.clear();
        currentLatLng= TabTestInNearByActivity.getCurrentLatLng();
        mCurrrentlatFragmentOne =currentLatLng.latitude;
        mCurrentlngFragmentOne =currentLatLng.longitude;
        currentPlace= TabTestInNearByActivity.getCurrentPlace();
        Log.i("currentPlaceLatlng F1", String.valueOf(currentLatLng));
        Log.i("currentPlace F1", currentPlace);

        mapFragmentOneNearBy.addMarker(new MarkerOptions().position(currentLatLng).title(currentPlace).snippet("You are currently here"));
    }
    public static void addMarkerForNearByPlaces()
    {

        mapFragmentOneNearBy.clear();

        mapFragmentOneNearBy.addMarker(new MarkerOptions().position(currentLatLng).title(currentPlace).snippet("You are currently here"));

        if (nearByLng.size() != 0) {
            Log.i(TAG, "Size greater than 0 and size is " + nearByLng.size());
            for (int i = 0; i < nearByLng.size(); i++) {
                Log.i(TAG, " Latitude " + i + nearByLat.get(i) + "");
                mapFragmentOneNearBy.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(nearByLat.get(i)), Double.parseDouble(nearByLng.get(i))))
                        .title(nearByNames.get(i))
                        .snippet(nearByAddress.get(i)));

              }
        }
        else {
            Toast.makeText(mContextFragMentOne, "No near by "+ FindDoctorOrChemistActivity.mSelectedUserToSearch+"Found !!!", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "size = 0");
        }

    }
    public static  ArrayList<String> getNearByNames()
    {
        return nearByNames;
    }

    public static void setData()
    {
        nearByNames = FindNearByPlacesActivity.getNearByNames();
        nearByLat = FindNearByPlacesActivity.getNearByLat();
        nearByLng = FindNearByPlacesActivity.getNearByLng();
        nearByAddress=FindNearByPlacesActivity.getNearByPlaces();


        Log.i(TAG,"nearbyPlacesInF1"+ nearByNames + "");
        Log.i(TAG,"nearbyLatInF1"+ nearByLat + "");
        Log.i(TAG,"nearbyLngInF1"+ nearByLng + "");

    }
}
