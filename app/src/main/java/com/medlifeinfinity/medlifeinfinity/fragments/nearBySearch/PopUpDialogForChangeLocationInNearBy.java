package com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.MainActivityOfFindNearByDoctors;

/**
 * Created by student on 3/8/2016.
 */
public class PopUpDialogForChangeLocationInNearBy extends DialogFragment {
    public static final String TAG = PopUpDialogForChangeLocationInNearBy.class.getSimpleName();


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();


        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        Button btnCurrentPlace = (Button) inflater.inflate(R.layout.custom_menu_for_change_location_menu_in_nearby, null).findViewById(R.id.btn_currentPlace_in_custom_menu_for_changeLocation);
        builder.setView(inflater.inflate(R.layout.custom_menu_for_change_location_menu_in_nearby, null));

        btnCurrentPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new MainActivityOfFindNearByDoctors().showingCurrentPlaces();

            }
        });

        final CharSequence[] charSequence = {"search", "current location"};


        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        })
        ;
//        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "You Pressed Ok", Toast.LENGTH_SHORT).show();
//            }
//        })
//        ;
        return builder.create();
    }
}
