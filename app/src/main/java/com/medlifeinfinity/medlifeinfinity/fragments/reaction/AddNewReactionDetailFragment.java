package com.medlifeinfinity.medlifeinfinity.fragments.reaction;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.LoginAndRegisterActivity;
import com.medlifeinfinity.medlifeinfinity.activities.reaction.ReactionHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewReactionDetailFragment extends android.app.Fragment {


    public static final String TAG = AddNewReactionDetailFragment.class.getSimpleName();
    EditText et_medName;
    EditText et_description;
    Button btn_save;
    String medName, descDetail;
    private TextInputLayout til_medicineName, til_Description;
    private ProgressDialog progressDialog;

    //    CustomDialogFragment customDialogFragment;
    private User mUserProfile;
    private Snackbar mSnkBar;


    public AddNewReactionDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_reaction_detail, container, false);
        til_Description = (TextInputLayout) view.findViewById(R.id.til_description_add_new_reaction_details_fragment);
        til_medicineName = (TextInputLayout) view.findViewById(R.id.til_medicine_name_add_new_reaction_details_fragment);
        et_description = (EditText) view.findViewById(R.id.et_reaction_detail_add_new_reaction_details_fragment);
        et_medName = (EditText) view.findViewById(R.id.et_medicine_name_add_new_reaction_details_fragment);
        btn_save = (Button) view.findViewById(R.id.btn_save_detail_add_new_reaction_details_fragment);

        mUserProfile = User.getInstance();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medName = et_medName.getText().toString();
                descDetail = et_description.getText().toString();
                if (medName.isEmpty()) {
                    til_medicineName.setError("pls provide name");
                    return;
                }
                if (descDetail.isEmpty()) {
                    til_Description.setError("pls provide desc");
                    return;
                }
                saveReactionDetails(mUserProfile.getmUserId(), medName, descDetail);
            }
        });

        return view;
    }

    private void saveReactionDetails(final String userId, final String medName, final String descDetail) {
        if (Utility.isNetworkAvailable(getActivity().getApplicationContext())) {
            String url = ApiCall.HOST_URL + "InsertMedicine";
            createProgressDialog(getActivity(), "saving details", false);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", userId);
                jsonObject.put("Medicine_Name", medName);
                jsonObject.put("Description", descDetail);
            } catch (JSONException exp) {
            }
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i(TAG, response.toString());
                    String status = null;
                    try {
                        status = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getActivity().getApplicationContext(), status, Toast.LENGTH_SHORT).show();
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(((ReactionHistoryActivity)getActivity()).mRootLayout, "Something went wrong.", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSnkBar.dismiss();
                            saveReactionDetails(userId,medName,descDetail);
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(getActivity().getApplicationContext()).getRequestQueue().add(request);
            et_description.setText("");
            et_medName.setText("");
        }
        else {
            mSnkBar = Snackbar.make(((ReactionHistoryActivity)getActivity()).mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    saveReactionDetails(userId,medName,descDetail);
                }
            });
            mSnkBar.show();
        }
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
