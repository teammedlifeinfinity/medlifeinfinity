package com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.TabTestInNearByActivity;

import java.util.ArrayList;
import java.util.List;

public class AlertDialogListOfAddressInNearBy extends DialogFragment {
    public static final String TAG = AlertDialogListOfAddressInNearBy.class.getSimpleName();
    ArrayList<String> placeLikelihoodArrayOfLat = new ArrayList<>();
    ArrayList<String> placeLikelihoodArrayOfLng = new ArrayList<>();

    private int mSelectedIndex = -1;
    private ArrayList<String> currentLatlng = new ArrayList<>();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        placeLikelihoodArrayOfLat = bundle.getStringArrayList("KeyForLat");
        placeLikelihoodArrayOfLng = bundle.getStringArrayList("KeyForLng");

        List<String> list = (List<String>) bundle.get("KeyForPlaces");
        final CharSequence[] cs = list.toArray(new CharSequence[list.size()]);
        //dialog.setSingleChoiceItems(cs, position, selectItemListener);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (mSelectedIndex != -1)
                            Toast.makeText(getActivity(), "Your Current Location is - " + cs[mSelectedIndex], Toast.LENGTH_SHORT).show();
                        Bundle bundle1 = new Bundle();
                        currentLatlng.clear();
                        currentLatlng.add(placeLikelihoodArrayOfLat.get(mSelectedIndex));
                        currentLatlng.add(placeLikelihoodArrayOfLng.get(mSelectedIndex));
                        bundle1.putStringArrayList("mCurrentPlaceLatLngTabTestInNearBy", currentLatlng);
                        bundle1.putString("currentPlace", String.valueOf(cs[mSelectedIndex]));

                        Log.i("currentPlaceLatLngInA", currentLatlng.toString());
                        Intent intent = new Intent(getContext(), TabTestInNearByActivity.class);
                        intent.putExtra("bundle", bundle1);
                        startActivity(intent);

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                })
                .setSingleChoiceItems(cs, -1, new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(TAG, " Selected index " + which);
                        Log.i(TAG, " Selected Item Name " + cs[which]);
                        mSelectedIndex = which;
                    }
                })
        ;

        // Create the AlertDialog object and return it
        return builder.create();
    }

}
