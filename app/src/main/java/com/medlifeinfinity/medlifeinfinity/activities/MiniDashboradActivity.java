package com.medlifeinfinity.medlifeinfinity.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.dietplan.DrDietPlanActivity;
import com.medlifeinfinity.medlifeinfinity.activities.dietplan.PtShowDietPlanDetailsActivity;
import com.medlifeinfinity.medlifeinfinity.activities.feedback.GiveFeedbackToDrActivity;
import com.medlifeinfinity.medlifeinfinity.activities.feedback.ViewFeedbackDoctorActivity;
import com.medlifeinfinity.medlifeinfinity.activities.history.viewHistory.ViewHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.activities.prescription.ViewPrescriptionActivity;
import com.medlifeinfinity.medlifeinfinity.activities.reaction.ReactionHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.activities.rms.RMSDoctorDisplayActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.MiniDashboardAdapter;
import com.medlifeinfinity.medlifeinfinity.customClasses.Constants;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

import java.util.Arrays;

public class MiniDashboradActivity extends AppCompatActivity {

    private static final String TAG = MiniDashboradActivity.class.getSimpleName();
    public static final int DOCTOR = 1;
    public static final int PATIENT = 2;

    Toolbar mToolbar;
    GridView gv_miniDashBoard;
    MiniDashboardAdapter mAdapter;
    String doctorId = "", patientId = "";
    String type = "";
    private String mName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mini_dashborad);
        Log.i(TAG, "onCreate");
        type = getIntent().getStringExtra("type");
        mName=getIntent().getStringExtra("name");
        setup();
    }


    private void setup() {
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        mToolbar.setTitle(mName);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        gv_miniDashBoard = (GridView) findViewById(R.id.gv_dashboard_mini_dashboard_activity);
        if(type.equals("doctor"))
        {
            String[] titleList = Constants.MINI_DASHBOARD_ITEM_PATIENT_LIST.clone();
            Integer[] iconList = Constants.MINI_DASHBOARD_IMAGES_PATIENT_LIST.clone();
            mAdapter = new MiniDashboardAdapter(getApplicationContext(), Arrays.asList(titleList), Arrays.asList(iconList));
            gv_miniDashBoard.setAdapter(mAdapter);
        }
        if(type.equals("patient"))
        {
            String[] titleList = Constants.MINI_DASHBOARD_ITEM_DOCTOR_LIST.clone();
            Integer[] iconList = Constants.MINI_DASHBOARD_IMAGES_DOCTOR_LIST.clone();
            mAdapter = new MiniDashboardAdapter(getApplicationContext(), Arrays.asList(titleList), Arrays.asList(iconList));
            gv_miniDashBoard.setAdapter(mAdapter);
        }
        gv_miniDashBoard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String temp = gv_miniDashBoard.getAdapter().getItem(position).toString();
                Log.i(TAG, temp + " Id : " + id);
                if (type.equals("doctor")) {
                    patientId = getIntent().getStringExtra("patientId");
                    if (temp.equals("Prescription")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, ViewPrescriptionActivity.class);
                        intent.putExtra("doctorId", User.getInstance().getmUserId());
                        intent.putExtra("patientId", patientId);
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                    if (temp.equals("Diet Plan")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, DrDietPlanActivity.class);
                        intent.putExtra("doctorId", User.getInstance().getmUserId());
                        intent.putExtra("patientId", patientId);
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                    if (temp.equals("RMS")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, RMSDoctorDisplayActivity.class);
                        intent.putExtra("doctorId", User.getInstance().getmUserId());
                        intent.putExtra("patientId", patientId);
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                    if (temp.equals("Feedback")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, ViewFeedbackDoctorActivity.class);
                        intent.putExtra("doctorId", User.getInstance().getmUserId());
                        intent.putExtra("patientId", patientId);
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                    if (temp.equals("Reaction")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, ReactionHistoryActivity.class);
                        intent.putExtra("userId", patientId);
                        intent.putExtra("class",MiniDashboradActivity.class.getSimpleName());
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                    if (temp.equals("History")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, ViewHistoryActivity.class);
                        intent.putExtra("userId", patientId);
                        intent.putExtra("class",MiniDashboradActivity.class.getSimpleName());
                        intent.setFlags(DOCTOR);
                        startActivity(intent);
                    }
                }
                if (type.equals("patient")) {
                    doctorId = getIntent().getStringExtra("doctorId");
                    if (temp.equals("Prescription")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, ViewPrescriptionActivity.class);
                        intent.putExtra("doctorId", doctorId);
                        intent.putExtra("patientId", User.getInstance().getmUserId());
                        intent.setFlags(PATIENT);
                        startActivity(intent);
                    }
                    if (temp.equals("Diet Plan")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, PtShowDietPlanDetailsActivity.class);
                        intent.putExtra("doctorId", doctorId);
                        intent.putExtra("patientId", User.getInstance().getmUserId());
                        intent.setFlags(PATIENT);
                        startActivity(intent);
                    }
                    if (temp.equals("RMS")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, RMSDoctorDisplayActivity.class);
                        intent.putExtra("doctorId", doctorId);
                        intent.putExtra("patientId", User.getInstance().getmUserId());
                        intent.setFlags(PATIENT);
                        startActivity(intent);
                    }
                    if (temp.equals("Feedback")) {
                        Intent intent = new Intent(MiniDashboradActivity.this, GiveFeedbackToDrActivity.class);
                        intent.putExtra("doctorId", doctorId);
                        intent.putExtra("patientId", User.getInstance().getmUserId());
                        intent.setFlags(PATIENT);
                        startActivity(intent);
                    }
                }
            }
        });


    }
}
