package com.medlifeinfinity.medlifeinfinity.activities.prescription;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddPrescriptionActivity extends AppCompatActivity {

    public static final String TAG = AddPrescriptionActivity.class.getSimpleName();

    CheckBox cb_Morning, cb_Noon, cb_Evening, cb_other;
    Spinner spn_Quantity, spn_Other;
    EditText et_MedicineName;
    Button btn_Submit, btn_Cancel, btn_ClearPatientName;
    String mMedicineName, mQuantity, mOthers, mDose;
    Boolean mMorning, mNoon, mNight;
    private ProgressDialog progressDialog;
    private Toolbar mToolBar;
    User mUserprofile;
    String patientId;
    String doctorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_prescription);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Add Prescription");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mUserprofile = User.getInstance();
        patientId = getIntent().getStringExtra("patientId");
        doctorId = getIntent().getStringExtra("doctorId");
        setup();
    }

    private void setup() {
//        cb_other = (CheckBox) findViewById(R.id.cb_other_add_prescription_activity);
        spn_Quantity = (Spinner) findViewById(R.id.spn_quantity_add_prescription_activity);
//        spn_Other = (Spinner) findViewById(R.id.spn_others_add_prescription_activity);
        et_MedicineName = (EditText) findViewById(R.id.et_medicine_name_add_prescription_activity);
        spn_Quantity = (Spinner) findViewById(R.id.spn_quantity_add_prescription_activity);
//        spn_Other = (Spinner) findViewById(R.id.spn_others_add_prescription_activity);

//        cb_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    spn_Other = (Spinner) findViewById(R.id.spn_others_add_prescription_activity);
//                    spn_Other.setVisibility(View.VISIBLE);
//
//
//                } else {
//                    spn_Other.setVisibility(View.GONE);
//                    spn_Other.setSelection(0);
//                }
//            }
//        });

        mMorning = false;
        mNoon = false;
        mNight = false;


        List<String> list = new ArrayList<>();
        list.add("Quantity");
        list.add("0.5");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddPrescriptionActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_Quantity.setAdapter(arrayAdapter);

//
//        list = new ArrayList<>();
//        list.add("Dose");
//        list.add("0.5 hr");
//        list.add("1 hr");
//        list.add("1.5 hrs");
//        list.add("2 hrs");
//        list.add("2.5 hrs");
//        list.add("3 hrs");
//        list.add("3.5 hrs");
//        list.add("4 hrs");
//        list.add("4.5 hrs");
//        list.add("5hrs");
//        list.add("5.5 hrs");
//        list.add("6 hrs");
//        list.add("6.5 hrs");
//        list.add("7 hrs");
//        list.add("7.5 hrs");
//        list.add("8 hrs");
//        arrayAdapter = new ArrayAdapter<String>(AddPrescriptionActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
//        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spn_Other.setAdapter(arrayAdapter);


        cb_Morning = (CheckBox) findViewById(R.id.cb_morning_add_prescription_activity);
        cb_Morning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDose = cb_Morning.getText().toString();
                    mMorning = true;

                } else {
                    //mDose = "";
                    mMorning = false;
                }

            }
        });

        cb_Noon = (CheckBox) findViewById(R.id.cb_noon_add_prescription_activity);
        cb_Noon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDose = mDose + ", " + cb_Noon.getText().toString();
                    mNoon = true;
                } else {
                    //mDose = "";
                    mNoon = false;

                }
            }
        });

        cb_Evening = (CheckBox) findViewById(R.id.cb_evening_add_prescription_activity);
        cb_Evening.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mDose = mDose + ", " + cb_Evening.getText().toString();
                    mNight = true;
                } else {
                    //mDose = "";
                    mNight = false;
                }
            }
        });

        btn_Submit = (Button) findViewById(R.id.btn_submit_add_prescription_activity);
        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });
        btn_Cancel = (Button) findViewById(R.id.btn_cancel_add_prescription_activity);
        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cancel();
            }
        });


    }

    public void Cancel() {

        et_MedicineName.setText("");
        spn_Quantity.setSelection(0);
        cb_Morning.setChecked(false);
        cb_Noon.setChecked(false);
        cb_Evening.setChecked(false);
        et_MedicineName.requestFocus();
    }

    public void addData() {

        mMedicineName = et_MedicineName.getText().toString();
        mQuantity = spn_Quantity.getSelectedItem().toString();
//        mOthers = spn_Other.getSelectedItem().toString();

        if (mMedicineName.length() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddPrescriptionActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("ENTER MEDICINE NAME");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    et_MedicineName.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else if (mQuantity.equals("") || mQuantity.equalsIgnoreCase("Quantity")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddPrescriptionActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("SELECT QUANTITY");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    spn_Quantity.setFocusable(true);
                    spn_Quantity.setFocusableInTouchMode(true);
                    spn_Quantity.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else if (!cb_Morning.isChecked() && !cb_Noon.isChecked() && !cb_Evening.isChecked()) {
//            if (cb_other.isChecked()) {
//                if (mOthers.equals("") || mOthers.equalsIgnoreCase("Dose")) {
//                    //  Toast.makeText(getApplicationContext(),"You select Other Option",Toast.LENGTH_LONG).show();
//                    AlertDialog.Builder builder = new AlertDialog.Builder(AddPrescriptionActivity.this);
//                    builder.setTitle("Error Find");
//                    builder.setMessage("SELECT OTHER DOSE");
//                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface arg0, int arg1) {
//                            spn_Other.setFocusable(true);
//                            spn_Other.setFocusableInTouchMode(true);
//                            spn_Other.requestFocus();
//
//                        }
//                    });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
//                } else {
//                    addPrescription();
//                }
            AlertDialog.Builder builder = new AlertDialog.Builder(AddPrescriptionActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("DON't FORGET TO SELECT DOSE");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    spn_Other.setFocusable(true);
                    spn_Other.setFocusableInTouchMode(true);
                    spn_Other.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            addPrescription();
        }
    }


    public void addPrescription() {
        mMedicineName = et_MedicineName.getText().toString();
        mQuantity = spn_Quantity.getSelectedItem().toString();
//        mOthers = spn_Other.getSelectedItem().toString();
        String doctorId = mUserprofile.getmUserId();
//        if (mOthers.equalsIgnoreCase("Dose")) {
//         //   mDose = mDose + "";
//        } else {
//            mDose = mDose + ", " + mOthers;
//        }

//        Toast.makeText(getApplicationContext(), mMedicineName + "\n" + mQuantity + "\n" + mDose + "\n" + "Morning =" + mMorning + "\n" + "Noon =" + mNoon + "\n" + "Night = " + mNight
//                , Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(),patientId,Toast.LENGTH_SHORT).show();

        createProgressDialog(this, "Loading...", false);

        JSONObject requestJSONObject = createRequestJSON(mMedicineName, mQuantity, mDose, mMorning, mNoon, mNight, doctorId, patientId);
        String url = ApiCall.HOST_URL + "InsertIntoPriscription";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestJSONObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "response " + response);
                        Toast.makeText(getApplicationContext(), "Successfully Data Inserted", Toast.LENGTH_LONG).show();
                        onBackPressed();
                        hideProgressDialog();
                    }
                },
                new Response.ErrorListener() {//not response the show the error
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error here " + error);
                        Toast.makeText(AddPrescriptionActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                });

        VolleySingleton.getInstance(this).getRequestQueue().add(request);
        Cancel();
    }

    private JSONObject createRequestJSON(String v_medicineName, String v_quantity, String v_dose, Boolean mMorning, Boolean mNoon, Boolean mNight, String v_createdBy, String v_createdFor) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Medicine_Name", v_medicineName);
            jsonObject.put("Quantity", v_quantity);
            jsonObject.put("DoseDuration", "NA");
            jsonObject.put("Morning", mMorning);
            jsonObject.put("Night", mNight);
            jsonObject.put("Afternoon", mNoon);
            jsonObject.put("Entry_ByDoc", doctorId);
            jsonObject.put("Patient_id", patientId);
            Log.i(TAG, doctorId);
        } catch (JSONException exp) {
            Log.e(TAG, " Error : " + exp);
        }
        return jsonObject;
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}

