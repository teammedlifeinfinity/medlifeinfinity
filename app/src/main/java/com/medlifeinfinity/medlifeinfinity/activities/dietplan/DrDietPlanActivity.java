package com.medlifeinfinity.medlifeinfinity.activities.dietplan;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class DrDietPlanActivity extends AppCompatActivity {

    public static final String TAG = DrDietPlanActivity.class.getSimpleName();

    String mMorning;
    String mAfternoon;
    String mEvening;
    String mNight;
    String mPrecaution;
    private EditText et_morning, et_afternoon, et_evening, et_night, et_precaution;
    private TextView tv_morning, tv_afternoon, tv_evening, tv_night, tv_precaution;
    private LinearLayout ll_morning, ll_afternoon, ll_evening, ll_night, ll_precaution;
    private Button btn_save;
    private ProgressDialog progressDialog;
    private String mPatientId;
    private String mDoctorId;
    private Toolbar mToolBar;
    Drawable toggle_image_up = null, toggle_image_down = null, im = null;
    private Snackbar mSnkBar;
    private NestedScrollView mRootLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dr_diet_plan);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Diet Plan");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        setup();
    }

    private void setup() {
        mRootLayout = (NestedScrollView) findViewById(R.id.root_dr_diet_plan_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toggle_image_up = getDrawable(R.drawable.ic_arrow_up);
            toggle_image_down = getDrawable(R.drawable.ic_arrow_down);
        }

        et_morning = (EditText) findViewById(R.id.et_morning_dr_diet_plan_activity);
        et_afternoon = (EditText) findViewById(R.id.et_afternoon_dr_diet_plan_activity);
        et_evening = (EditText) findViewById(R.id.et_evening_dr_diet_plan_activity);
        et_night = (EditText) findViewById(R.id.et_night_dr_diet_plan_activity);
        et_precaution = (EditText) findViewById(R.id.et_precaution_dr_diet_plan_activity);

        tv_morning = (TextView) findViewById(R.id.tv_morning_dr_diet_plan_activity);
        tv_afternoon = (TextView) findViewById(R.id.tv_afternoon_dr_diet_plan_activity);
        tv_evening = (TextView) findViewById(R.id.tv_evening_dr_diet_plan_activity);
        tv_night = (TextView) findViewById(R.id.tv_night_dr_diet_plan_activity);
        tv_precaution = (TextView) findViewById(R.id.tv_precaution_dr_diet_plan_activity);

        ll_morning = (LinearLayout) findViewById(R.id.ll_morning_dr_diet_plan_activity);
        ll_afternoon = (LinearLayout) findViewById(R.id.ll_afternoon_dr_diet_plan_activity);
        ll_evening = (LinearLayout) findViewById(R.id.ll_evening_dr_diet_plan_activity);
        ll_night = (LinearLayout) findViewById(R.id.ll_night_dr_diet_plan_activity);
        ll_precaution = (LinearLayout) findViewById(R.id.ll_precaution_dr_diet_plan_activity);
        btn_save = (Button) findViewById(R.id.btn_save_dr_diet_plan_activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_morning);
        }
        ll_morning.setVisibility(View.GONE);
        tv_morning.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_afternoon);
        }
        ll_afternoon.setVisibility(View.GONE);
        tv_afternoon.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_evening);
        }
        ll_evening.setVisibility(View.GONE);
        tv_evening.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_night);
        }
        ll_night.setVisibility(View.GONE);
        tv_night.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_precaution);
        }
        ll_precaution.setVisibility(View.GONE);
        tv_precaution.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMorning = et_morning.getText().toString();
                mAfternoon = et_afternoon.getText().toString();
                mEvening = et_evening.getText().toString();
                mNight = et_night.getText().toString();
                mPrecaution = et_precaution.getText().toString();
                validate();
            }
        });

        tv_morning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_morning);
                }
                ll_morning.setVisibility(ll_morning.isShown() ? View.GONE : View.VISIBLE);
                tv_morning.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_morning.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_afternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_afternoon);
                }
                ll_afternoon.setVisibility(ll_afternoon.isShown() ? View.GONE : View.VISIBLE);
                tv_afternoon.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_afternoon.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_evening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_evening);
                }
                ll_evening.setVisibility(ll_evening.isShown() ? View.GONE : View.VISIBLE);
                tv_evening.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_evening.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_night.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_night);
                }
                ll_night.setVisibility(ll_night.isShown() ? View.GONE : View.VISIBLE);
                tv_night.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_night.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_precaution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_precaution);
                }
                ll_precaution.setVisibility(ll_precaution.isShown() ? View.GONE : View.VISIBLE);
                tv_precaution.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_precaution.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
    }

    private void validate() {
        if (mMorning.length() == 0) {
            Toast.makeText(this, "Please Enter Morning Diet", Toast.LENGTH_SHORT).show();

        } else if (mAfternoon.length() == 0) {
            Toast.makeText(this, "Please Enter Afternoon Diet", Toast.LENGTH_SHORT).show();

        } else if (mEvening.length() == 0) {
            Toast.makeText(this, "Please Enter Evening Diet", Toast.LENGTH_SHORT).show();

        } else if (mNight.length() == 0) {
            Toast.makeText(this, "Please Enter Dinner Diet", Toast.LENGTH_SHORT).show();

        } else if (mPrecaution.length() == 0) {
            Toast.makeText(this, "Please Enter Precautions", Toast.LENGTH_SHORT).show();

        } else {
            insertData();
        }
    }

    private void insertData() {
        if (Utility.isNetworkAvailable(this)) {
            createProgressDialog(this, "loading...", false);
            String url = ApiCall.HOST_URL + "InsertIntoDietPlan";
            JSONObject requestJSONObject = createRequestJSON(Integer.parseInt(mPatientId), mMorning, mAfternoon, mEvening, mNight, mPrecaution, Integer.parseInt(mDoctorId));
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "response " + response);
                            Toast.makeText(DrDietPlanActivity.this, "Successfully Data Inserted", Toast.LENGTH_LONG).show();
                            hideProgressDialog();
                            clearAllFields();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Error Here " + error);
                            mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong.", Snackbar.LENGTH_INDEFINITE);
                            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    insertData();
                                    mSnkBar.dismiss();
                                }
                            });
                            mSnkBar.show();
                            hideProgressDialog();
                        }
                    });
            VolleySingleton.getInstance(getApplicationContext()).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private JSONObject createRequestJSON(int patientId, String mMorning, String mAfternoon, String mEvening, String mNight, String mPrecautions, int docId) {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("user_id", patientId);
            jsonObject.put("Day", "NA");
            jsonObject.put("Morning", mMorning);
            jsonObject.put("Afternoon", mAfternoon);
            jsonObject.put("Evening", mEvening);
            jsonObject.put("Night", mNight);
            jsonObject.put("Precautions", mPrecautions);
            jsonObject.put("EntryBy_Id", docId);
        } catch (JSONException exp) {
            Log.e(TAG, " Error : " + exp);
        }
        return jsonObject;

    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void clearAllFields() {

        et_morning.setText("");
        et_afternoon.setText("");
        et_evening.setText("");
        et_night.setText("");
        et_precaution.setText("");
    }

}
