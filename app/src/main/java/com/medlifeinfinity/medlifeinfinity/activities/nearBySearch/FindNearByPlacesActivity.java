package com.medlifeinfinity.medlifeinfinity.activities.nearBySearch;

import android.content.Context;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.FragmentOneInNearBy;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.FragmentTwoInNearBy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by student on 4/11/2016.
 */

public  class FindNearByPlacesActivity extends AppCompatActivity {
    private static String TAG=FindNearByPlacesActivity.class.getSimpleName();
    double mCurrentlatInFindNearByPlaces, mCurrentLngInFindNearByPlaces;
    boolean mflagFindNearByPlaces;
    int rangeFindNearByPlaces =0;

    float resultFindNearByPlaces[]=new float[2];

    static ArrayList<String> nearByPlaces=new ArrayList<String>(10);
    static ArrayList<String> nearByLat= new ArrayList<String>(10);
    static ArrayList<String> nearByLng = new ArrayList<String>(10);
    static ArrayList<String> nearByNames = new ArrayList<String>(10);

    double [] distanceArrayFindNearByPlaces;//=new double[1000];

    ArrayList<Double> arrayListOfLatitudes=new ArrayList<>();
    ArrayList<Double> arrayListOfLongitudes=new ArrayList<>();
    ArrayList<String> arrayListOfNames=new ArrayList<>();
    ArrayList<String> arrayListOfAddress=new ArrayList<>();
    Context mContextFindNearByPlaces;


    public FindNearByPlacesActivity(){

    }

    public void findDistance()
    {
        mCurrentlatInFindNearByPlaces = TabTestInNearByActivity.getCurrentLatLng().latitude;
        mCurrentLngInFindNearByPlaces = TabTestInNearByActivity.getCurrentLatLng().longitude;
        Log.i(TAG," Current Lat mCurrentlngFragmentOne "+ mCurrentlatInFindNearByPlaces +" -- "+ mCurrentLngInFindNearByPlaces);
        Log.i(TAG,"Size Of ALLat="+arrayListOfLatitudes.size());
        for(int i=0;i<arrayListOfLatitudes.size();i++)
        {
            Location.distanceBetween(mCurrentlatInFindNearByPlaces, mCurrentLngInFindNearByPlaces, arrayListOfLatitudes.get(i), arrayListOfLongitudes.get(i), resultFindNearByPlaces);
            Log.i(TAG, " Distance " + i + " " + resultFindNearByPlaces[0] / 1000 + "--" + resultFindNearByPlaces[1] / 1000);
            distanceArrayFindNearByPlaces[i]= resultFindNearByPlaces[0]/1000;
        }
        Log.i(TAG, " Distance from current Location " + distanceArrayFindNearByPlaces.toString());
    }

    public void findDistanceBetweenRange(int range) {
        mflagFindNearByPlaces = true;
        nearByPlaces.clear();
        nearByLat.clear();
        nearByLng.clear();
        nearByNames.clear();
        findDistance();

        Log.i(TAG,"size of ALLat =="+arrayListOfLatitudes.size());
        for (int i = 0; i < arrayListOfLatitudes.size(); i++) {
            if (distanceArrayFindNearByPlaces[i] <= range) {
                nearByPlaces.add(arrayListOfAddress.get(i));
                nearByLat.add(String.valueOf(arrayListOfLatitudes.get(i)));
                nearByLng.add(String.valueOf(arrayListOfLongitudes.get(i)));
                nearByNames.add(arrayListOfNames.get(i));
            }
        }
        Log.i(TAG, "Within Range Names \n" + nearByNames.toString());
        Log.i(TAG, "Within Range Latitudes \n" + nearByLat.toString());
        Log.i(TAG, "Within Range Longitudes \n" + nearByLng.toString());
        Log.i(TAG, "Within Range Address \n" + nearByPlaces.toString());

        FragmentOneInNearBy.setData();
        FragmentOneInNearBy.addMarkerForNearByPlaces();
        FragmentTwoInNearBy.prepareListData();


    }

    public static ArrayList<String> getNearByPlaces()
    {
        return nearByPlaces ;
    }

    public static ArrayList<String> getNearByLat()
    {
        return nearByLat ;
    }

    public static ArrayList<String> getNearByLng()
    {
        return nearByLng;
    }

    public static ArrayList<String> getNearByNames()
    {
        return nearByNames;
    }


    public void fetchData(final Context context,int range)
    {
        this.rangeFindNearByPlaces =range;
        arrayListOfLatitudes.clear();
        arrayListOfLongitudes.clear();
        arrayListOfNames.clear();
        arrayListOfAddress.clear();

        this.mContextFindNearByPlaces =context;
        RequestQueue requestQueue = Volley.newRequestQueue(context);


        String url ="http://mani11-001-site1.ctempurl.com/api/WebApi/GetUserDetails";

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, "Response " + response);
                Toast.makeText(mContextFindNearByPlaces, "Response Received Successfully", Toast.LENGTH_SHORT).show();
                parseResponse(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error " + error);
                        Toast.makeText(mContextFindNearByPlaces, "Response Received with Error !!\nTry Again ...", Toast.LENGTH_SHORT).show();

                    }
                });
        requestQueue.add(jsonRequest);

    }

    private void parseResponse(JSONArray response) {
        JSONArray dataArray=response;
        Log.i(TAG, " Response Got " + response);
        try {

            for (int i=0; i < dataArray.length() ; i++){
                JSONObject userJSONObject = dataArray.getJSONObject(i);

                if(userJSONObject.has("Roll_Id")&& !userJSONObject.isNull("Roll_Id")){

                    if(userJSONObject.getInt("Roll_Id")==FindDoctorOrChemistActivity.mRollIdInFindDoctorOrChemist){

                        if ((userJSONObject.has("Latitude") && !userJSONObject.isNull("Latitude"))
                                || (userJSONObject.has("Longitude") && !userJSONObject.isNull("Longitude"))) {
                            // get data JSONArray from response
                            arrayListOfLatitudes.add(Double.parseDouble(userJSONObject.getString("Latitude")));
                            arrayListOfLongitudes.add(Double.parseDouble(userJSONObject.getString("Longitude")));
                            arrayListOfAddress.add(userJSONObject.getString("Address"));
                            arrayListOfNames.add(userJSONObject.getString("Name"));


                        } else {
                            // get message using error key
                        }
                    }
                }

            }

            for(int i=0;i< arrayListOfLatitudes.size();i++)
            {
                Log.i(TAG, arrayListOfNames.get(i) + " has address " + arrayListOfAddress.get(i) + " has mCurrrentlatFragmentOne " + arrayListOfLatitudes.get(i) + " and long " + arrayListOfLongitudes.get(i));
            }
        } catch (Exception e) {
            Log.e(TAG,"Error while parsing "+e);
            e.printStackTrace();
            Toast.makeText(mContextFindNearByPlaces,"Error : "+e.getMessage(),Toast.LENGTH_SHORT).show();
        }
        distanceArrayFindNearByPlaces =new double[arrayListOfLatitudes.size()];
        findDistanceBetweenRange(rangeFindNearByPlaces);
    }
}
