package com.medlifeinfinity.medlifeinfinity.activities.findDoctors;

/**
 * Created by Vaio on 4/23/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.adapters.ProfileListItemAdapter;
import com.medlifeinfinity.medlifeinfinity.adapters.SimpleProfileListItemAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class DoctorListActivity extends AppCompatActivity {

    private String mDoctorSpecialization;
    private String mCityName;
    TextView tv_city_nameAndCategory;
    ListView lv_doctorList;
    private Toolbar mToolbar;
    ProfileListItemAdapter mAdapter;
    public final String TAG = DoctorListActivity.class.getSimpleName();
    ArrayList<User> mUsers = new ArrayList<>();
    String[] mUserIds;
    private ProgressDialog progressDialog;
    private LinearLayout mRootLayout;
    private Snackbar mSnkBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_list);
        mDoctorSpecialization = getIntent().getStringExtra("doctorSpecialization");
        mCityName = getIntent().getStringExtra("cityName");
        createProgressDialog(this, "Loading doctor data...", false);
        setup();
        getDoctorList();

    }

    private void getDoctorList() {
        String url = ApiCall.HOST_URL + "getDoctorByCityAndSpec/" + mCityName + "/" + mDoctorSpecialization;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                mAdapter = new ProfileListItemAdapter(getApplicationContext(), User.DOCTOR_ROLE,DoctorListActivity.class.getSimpleName());
                lv_doctorList.setAdapter(mAdapter);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar = Snackbar.make(mRootLayout, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDoctorList();
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void parseResponse(JSONArray response) throws JSONException {

        if (response.length() > 0) {
            mUserIds = new String[response.length()];
            for (int i = 0; i < response.length(); i++) {
                mUserIds[i] = (response.getJSONObject(i)).getString("user_id");
                getUserObjectFromId(mUserIds[i]);

            }
        }
        else {
            mSnkBar=Snackbar.make(mRootLayout,"No Data Found",Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }

    }

    private void getUserObjectFromId(final String userid) {
        String url = ApiCall.HOST_URL + "GetUserDetailsWithId/" + userid;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    mUsers.add(User.getUserObjectFromJsonObject(response.getJSONObject(0)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.addProfile(mUsers.get(mUsers.size() - 1));
                mAdapter.notifyDataSetChanged();
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar = Snackbar.make(mRootLayout, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getUserObjectFromId(userid);
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void setup() {
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        mToolbar.setTitle(mDoctorSpecialization + " At " + mCityName);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRootLayout = (LinearLayout) findViewById(R.id.root_doctor_list_activity);
        lv_doctorList = (ListView) findViewById(R.id.lv_doctor_list_doctor_list_activity);
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


}
