package com.medlifeinfinity.medlifeinfinity.activities.dietplan;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PtShowDietPlanDetailsActivity extends AppCompatActivity {

    public static final String TAG = PtShowDietPlanDetailsActivity.class.getSimpleName();
    public static final String KEY_MORNING = "Morning";
    public static final String KEY_AFTERNOON = "Afternoon";
    public static final String KEY_EVENING = "Evening";
    public static final String KEY_NIGHT = "Night";
    public static final String KEY_PRECAUTION = "Precautions";
    public static String morning = "";
    public static String afternoon = "";
    public static String evening = "";
    public static String night = "";
    public static String precaution = "";


    private TextView tvv_morning, tvv_afternoon, tvv_evening, tvv_night, tvv_precaution;
    private TextView tv_morning, tv_afternoon, tv_evening, tv_night, tv_precaution;
    private LinearLayout ll_morning, ll_afternoon, ll_evening, ll_night, ll_precaution;

    private ProgressDialog progressDialog;
    private LinearLayout mRootLayout;
    private String mPatientId;
    private String mDoctorId;
    private int flag;
    private Drawable im;
    private Drawable toggle_image_up;
    private Drawable toggle_image_down;
    private Snackbar mSnkBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_diet_plan_details);
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        flag = getIntent().getFlags();
        setup();
        loadData(mDoctorId, mPatientId);
    }

    private void loadData(final String doctorID, final String userId) {
        if (Utility.isNetworkAvailable(this)) {
            String url = ApiCall.HOST_URL + "GetDietPlanWithDoctorId/" + doctorID + "/" + userId;
            this.createProgressDialog(this, "Loding data...", false);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.i(TAG, "Response " + response);
                    try {
                        parseResponse(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong.", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadData(doctorID, userId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(getApplicationContext()).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadData(doctorID, userId);
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private void parseResponse(JSONArray response) throws JSONException {
        if (response.length() == 0) {
            mSnkBar = Snackbar.make(mRootLayout, "No Data found.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        } else {
            JSONObject object = response.getJSONObject(0);
            morning = object.getString(KEY_MORNING);
            afternoon = object.getString(KEY_AFTERNOON);
            evening = object.getString(KEY_EVENING);
            night = object.getString(KEY_NIGHT);
            precaution = object.getString(KEY_PRECAUTION);

            tvv_afternoon.setText(afternoon);
            tvv_morning.setText(morning);
            tvv_evening.setText(evening);
            tvv_night.setText(night);
            tvv_precaution.setText(precaution);
        }
    }

    private void setup() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        toolbar.setTitle("Diet Plan");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mRootLayout = (LinearLayout) findViewById(R.id.root_show_diet_plan_details_activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toggle_image_up = getDrawable(R.drawable.ic_arrow_up);
            toggle_image_down = getDrawable(R.drawable.ic_arrow_down);
        }

        tvv_morning = (TextView) findViewById(R.id.tvv_morning_show_diet_plan_details_activity);
        tvv_afternoon = (TextView) findViewById(R.id.tvv_afternoon_show_diet_plan_details_activity);
        tvv_evening = (TextView) findViewById(R.id.tvv_evening_show_diet_plan_details_activity);
        tvv_night = (TextView) findViewById(R.id.tvv_night_show_diet_plan_details_activity);
        tvv_precaution = (TextView) findViewById(R.id.tvv_precaution_show_diet_plan_details_activity);

        tv_morning = (TextView) findViewById(R.id.tv_morning_show_diet_plan_details_activity);
        tv_afternoon = (TextView) findViewById(R.id.tv_afternoon_show_diet_plan_details_activity);
        tv_evening = (TextView) findViewById(R.id.tv_evening_show_diet_plan_details_activity);
        tv_night = (TextView) findViewById(R.id.tv_night_show_diet_plan_details_activity);
        tv_precaution = (TextView) findViewById(R.id.tv_precaution_show_diet_plan_details_activity);

        ll_morning = (LinearLayout) findViewById(R.id.ll_morning_show_diet_plan_details_activity);
        ll_afternoon = (LinearLayout) findViewById(R.id.ll_afternoon_show_diet_plan_details_activity);
        ll_evening = (LinearLayout) findViewById(R.id.ll_evening_show_diet_plan_details_activity);
        ll_night = (LinearLayout) findViewById(R.id.ll_night_show_diet_plan_details_activity);
        ll_precaution = (LinearLayout) findViewById(R.id.ll_precaution_show_diet_plan_details_activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_morning);
        }
        ll_morning.setVisibility(View.GONE);
        tv_morning.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_afternoon);
        }
        ll_afternoon.setVisibility(View.GONE);
        tv_afternoon.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_evening);
        }
        ll_evening.setVisibility(View.GONE);
        tv_evening.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_night);
        }
        ll_night.setVisibility(View.GONE);
        tv_night.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            im = getDrawable(R.drawable.ic_precaution);
        }
        ll_precaution.setVisibility(View.GONE);
        tv_precaution.setCompoundDrawablesWithIntrinsicBounds(im, null, toggle_image_down, null);


        tv_morning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_morning);
                }
                ll_morning.setVisibility(ll_morning.isShown() ? View.GONE : View.VISIBLE);
                tv_morning.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_morning.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_afternoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_afternoon);
                }
                ll_afternoon.setVisibility(ll_afternoon.isShown() ? View.GONE : View.VISIBLE);
                tv_afternoon.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_afternoon.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_evening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_evening);
                }
                ll_evening.setVisibility(ll_evening.isShown() ? View.GONE : View.VISIBLE);
                tv_evening.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_evening.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_night.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_night);
                }
                ll_night.setVisibility(ll_night.isShown() ? View.GONE : View.VISIBLE);
                tv_night.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_night.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
        tv_precaution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    im = getDrawable(R.drawable.ic_precaution);
                }
                ll_precaution.setVisibility(ll_precaution.isShown() ? View.GONE : View.VISIBLE);
                tv_precaution.setCompoundDrawablesWithIntrinsicBounds(im, null, ll_precaution.isShown() ? toggle_image_up : toggle_image_down, null);

            }
        });
    }


    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


}
