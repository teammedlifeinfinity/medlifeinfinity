package com.medlifeinfinity.medlifeinfinity.activities.history.viewHistory;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;


import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.adapters.ImageSlideAdapter;
import com.medlifeinfinity.medlifeinfinity.customClasses.GetHistory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class OpenImageActivity extends AppCompatActivity {
    ViewPager mViewPager;
    String mImgUrl;
    Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_image);
//        mToolBar = (Toolbar) findViewById(R.id.app_bar);
//        mToolBar.setTitle("View History");
//        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
//        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

        Intent intent = getIntent();
        ArrayList<GetHistory> mArrayList = intent.getParcelableArrayListExtra("img");
        int mPosition = intent.getExtras().getInt("position");
        mImgUrl = mArrayList.get(mPosition).getmImageUrl();
        String[] mImageUrlArray = new String[mArrayList.size()];
        String[] mImageNameArray=new String[mArrayList.size()];
        mViewPager = (ViewPager) findViewById(R.id.view_pager_open_image_activity);

        for (int i = 0; i < mArrayList.size(); i++) {
            mImageUrlArray[i] = mArrayList.get(i).getmImageUrl();
            mImageNameArray[i]=mArrayList.get(i).getmImageName();
            Log.e("Visible images",mImageUrlArray[i]);
        }
     ImageSlideAdapter imageSlideAdapter=new ImageSlideAdapter(getApplicationContext(),mImageUrlArray,mPosition);
        mViewPager.setAdapter(imageSlideAdapter);
        mViewPager.setCurrentItem(mPosition);
        Object[] array = new Object[2];
        array[0]=mImageUrlArray[mPosition];
        array[1]=mImageNameArray[mPosition];
        new SaveImage().execute(array);
    }
    private class SaveImage extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] params) {
                saveImage("http://mani11-001-site1.ctempurl.com/images/" + ((String)params[0]),((String)params[1]));
                Log.e("url", "http://mani11-001-site1.ctempurl.com/images/" + ((String) params[0]));
            return params[0];
        }
    }
    private void saveImage(String imagePath, String imageName) {
        try{
            URL imageUrl=new URL(imagePath);
            HttpURLConnection urlConnection=(HttpURLConnection)imageUrl.openConnection();
            urlConnection.connect();
            File SDCard= Environment.getExternalStorageDirectory();
            File dir=new File(SDCard.getAbsolutePath()+"/MedLife Infinity");
            dir.mkdirs();
            File file=new File(dir,imageName);
            FileOutputStream fileOutput=new FileOutputStream(file);
            InputStream inputStream=urlConnection.getInputStream();
            //totalSize = urlConnection.getContentLength();

            runOnUiThread(new Runnable() {
                public void run() {
              //      mProgressBar.setMax(totalSize);
                }
            });

            //create a buffer...
            byte[] buffer = new byte[1024];
            int bufferLength = 0;

            while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
                fileOutput.write(buffer, 0, bufferLength);

            }
            //close the output stream when complete //
            fileOutput.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}