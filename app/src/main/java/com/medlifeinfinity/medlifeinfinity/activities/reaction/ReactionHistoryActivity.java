package com.medlifeinfinity.medlifeinfinity.activities.reaction;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.fragments.reaction.AddNewReactionDetailFragment;
import com.medlifeinfinity.medlifeinfinity.fragments.reaction.ReactionListViewFragment;

public class ReactionHistoryActivity extends AppCompatActivity {


    private Toolbar mToolBar;
    private FloatingActionButton fab_addDetails;
    private ReactionListViewFragment reactionListFrag;
    private AddNewReactionDetailFragment addNewDetailFrag;
    private User mUserProfile;
    public String mUserId;
    private String mLauncherClass;
    public CoordinatorLayout mRootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reaction_history);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Reaction Details");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        attachReactionHistoryFragment();
        mUserId=getIntent().getStringExtra("userId");
        mLauncherClass=getIntent().getStringExtra("class");
        setup();
    }

    private void setup() {
        mRootLayout= (CoordinatorLayout) findViewById(R.id.root_reaction_history_activity);
        fab_addDetails = (FloatingActionButton) findViewById(R.id.fab_add_reaction_details_reaction_history_activity);
        fab_addDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachNewDetailFragment();
                fab_addDetails.setVisibility(View.GONE);
            }
        });
        if(mLauncherClass.equals(MiniDashboradActivity.class.getSimpleName()))
        {
            fab_addDetails.setVisibility(View.GONE);
        }
    }

    private void attachReactionHistoryFragment() {

        reactionListFrag = new ReactionListViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.container_reaction_history_activity, reactionListFrag, "ReactionListFrag");
        transaction.commit();
    }

    //method to attach the add new detail fragment
    private void attachNewDetailFragment() {
        addNewDetailFrag = new AddNewReactionDetailFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container_reaction_history_activity, addNewDetailFrag, "AddNewDetailFrag");
        transaction.addToBackStack(null);
        transaction.commit();

    }

    //method to be called when back button is pressed, it pops the fragmants from the back satack
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            fab_addDetails.setVisibility(View.VISIBLE);

            return;
        }
        super.onBackPressed();
    }
}
