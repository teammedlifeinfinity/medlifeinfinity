package com.medlifeinfinity.medlifeinfinity.activities.chat;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.Message;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by jamiepatel on 22/09/2015.
 */
public class MessageAdapter extends BaseAdapter {

    Context messageContext;
    List<Message> messageList;

    public MessageAdapter(Context context, List<Message> messages){
        messageList = messages;
        messageContext = context;
    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder holder;
        if (convertView == null){
            LayoutInflater messageInflater = (LayoutInflater) messageContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = messageInflater.inflate(R.layout.custom_chat_row, null);
            holder = new MessageViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.img_thumbnail_custom_chat_row);
            holder.senderView = (TextView) convertView.findViewById(R.id.tv_message_sender_custom_chat_row);
            holder.bodyView = (TextView) convertView.findViewById(R.id.tv_message_body_custom_chat_row);
            holder.Time=(TextView)convertView.findViewById(R.id.tv_time_custom_chat_row);


            convertView.setTag(holder);
        } else {
            holder = (MessageViewHolder) convertView.getTag();
        }

        Message message = (Message) getItem(position);

        holder.bodyView.setText(message.text);
        holder.senderView.setText(message.name);
        holder.Time.setText(getdata(message.time));

//        Picasso.with(messageContext).
//                load("https://twitter.com/"+message.name+"/profile_image?size=original").
//                placeholder(R.mipmap.ic_launcher).
//                into(holder.thumbnailImageView);
        return convertView;
    }

    private String getdata(long time)
    { SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return (formatter.format(calendar.getTime()));
    }

    public void add(Message message){
        messageList.add(message);
        notifyDataSetChanged();
    }
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    private static class MessageViewHolder {
        public ImageView thumbnailImageView;
        public TextView senderView;
        public TextView bodyView;
        public TextView Time;
    }
}