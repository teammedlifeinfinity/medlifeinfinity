package com.medlifeinfinity.medlifeinfinity.activities;


import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.Validate;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity {
    private static final String TAG = ChangePasswordActivity.class.getSimpleName();
    Button mBtnSendPasswordChangePassword;
    private LinearLayout mLinearLayoutUpdatePassword;
    private EditText mEditTextNewPassword;
    private EditText mEditTextCurrentPassword;
    private Snackbar mSnackBarInfo;
    private TextInputLayout tilTextNewPassword;
    private TextInputLayout tilTextCurrentPassword;
    private Snackbar snkBar;
    private String mPassword;
    private User mUserInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        mUserInstance = User.getInstance();
        generateViews();
        eventListner();
    }

    private void eventListner() {
        mBtnSendPasswordChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isNetworkAvailable(getApplicationContext())) {
                    if (validateFields()) {
                        try {
                            UpdatePasswordApi(mUserInstance.getmEmail(), mEditTextNewPassword.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Internet is not connected", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void UpdatePasswordApi(String emailId, String newPassword) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("Email", emailId);
        object.put("Password", newPassword);

        String url = ApiCall.HOST_URL + "UpdatePassword";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                snkBar = Snackbar.make(mLinearLayoutUpdatePassword, "No Internet Connection", Snackbar.LENGTH_INDEFINITE);
                snkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            UpdatePasswordApi(mUserInstance.getmEmail(), mEditTextNewPassword.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        snkBar.dismiss();
                    }
                });
                snkBar.show();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void parseResponse(JSONObject response) throws JSONException {
        String status = response.getString("status");
        if (status.equals("Record Successfully Updated")) {
            Toast.makeText(ChangePasswordActivity.this, "Your Password has Successfully Updated", Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            snkBar = Snackbar.make(mLinearLayoutUpdatePassword, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
            snkBar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snkBar.dismiss();
                }
            });
            snkBar.show();
        }
    }

    private void generateViews() {
        mBtnSendPasswordChangePassword = (Button) findViewById(R.id.btn_update_password_update_password);
        mEditTextCurrentPassword = (EditText) findViewById(R.id.et_user_current_password_change_password_layout);
        mEditTextNewPassword = (EditText) findViewById(R.id.et_user_new_password_change_password_layout);
        mLinearLayoutUpdatePassword = (LinearLayout) findViewById(R.id.linear_layout_change_password_interface);
    }

    private boolean validateFields() {
        if (!(Validate.isValid(mEditTextNewPassword.getText().toString(), Validate.PASSWORD_PATTERN))) {
            mEditTextNewPassword.setError("Password should contain a uppercase ,a lower case, a numeric and ,a symbol.");
            return false;
        }
        if (mEditTextCurrentPassword.equals(mUserInstance.getmPassword())) {
            mEditTextCurrentPassword.setError("Your Current Password doesn't Match");
        }
        return true;
    }
}
