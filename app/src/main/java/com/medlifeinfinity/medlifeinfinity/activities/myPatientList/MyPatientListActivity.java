package com.medlifeinfinity.medlifeinfinity.activities.myPatientList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.SimpleProfileListItemAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MyPatientListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView lv_patientList;
    private Toolbar mToolbar;
    SimpleProfileListItemAdapter mAdapter;
    public final String TAG = MyPatientListActivity.class.getSimpleName();
    ArrayList<User> mUsers = new ArrayList<>();
    String[] mUserIds;
    private ProgressDialog progressDialog;
    private LinearLayout rootLayout;
    Snackbar mSnkBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_patient_list);
        createProgressDialog(this, "Loading patients data...", false);
        setup();
        if (Utility.isNetworkAvailable(this)) {
            getPatientList();
        } else {
            mSnkBar = Snackbar.make(rootLayout, "check for internet connection", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getPatientList();
                }
            });
            mSnkBar.show();
        }
    }

    private void getPatientList() {
        String url = ApiCall.HOST_URL + "GetPatientFromRelationship/" + User.getInstance().getmUserId();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter = new SimpleProfileListItemAdapter(getApplicationContext());
                lv_patientList.setAdapter(mAdapter);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar = Snackbar.make(rootLayout, "check for internet connection", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPatientList();
                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void parseResponse(JSONArray response) throws JSONException {

        mUserIds = new String[response.length()];
        if (mUserIds.length != 0) {
            for (int i = 0; i < response.length(); i++) {
                mUserIds[i] = (response.getJSONObject(i)).getString("Patient_Id");
                getUserObjectFromId(mUserIds[i]);
            }
        } else {
            mSnkBar = Snackbar.make(rootLayout, "No data found", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }

    }

    private void getUserObjectFromId(String userid) {
        String url = ApiCall.HOST_URL + "GetUserDetailsWithId/" + userid;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    mUsers.add(User.getUserObjectFromJsonObject(response.getJSONObject(0)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mAdapter.addProfile(mUsers.get(mUsers.size() - 1));
                mAdapter.notifyDataSetChanged();
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void setup() {
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        mToolbar.setTitle("My Patients");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lv_patientList = (ListView) findViewById(R.id.lv_patient_list_my_patients_list_activity);
        rootLayout = (LinearLayout) findViewById(R.id.root_my_patient_list_activity);

        lv_patientList.setOnItemClickListener(this);

    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        long viewId = view.getId();
        if (viewId == R.id.ib_overflow_menu_card_view_simple_profile) {
            Log.i(TAG, " over flow menu Position " + position);
            ImageButton ib_overflow = (ImageButton) view;
            PopupMenu popupMenu = new PopupMenu(getApplicationContext(), ib_overflow);
            popupMenu.inflate(R.menu.menu_remove);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.menu_remove) {
                        Log.i(TAG, "Drop");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MyPatientListActivity.this);
                        builder.setTitle("Warning");
                        builder.setMessage("Are you Sure");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                try {
                                    deleteRelation(mAdapter.mUserProfilesId.get(position).getmUserId(), User.getInstance().getmUserId());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    return true;
                }
            });
            popupMenu.show();

        }
        if (viewId == R.id.card_view_simple_profile) {
            String patientId = mAdapter.mUserProfilesId.get(position).getmUserId();
            String name = mAdapter.mUserProfilesId.get(position).getmName();
//                Toast.makeText(getApplicationContext(), patientId + " Clicked position " + position, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MyPatientListActivity.this, MiniDashboradActivity.class);
            intent.putExtra("type", "doctor");
            intent.putExtra("patientId", patientId);
            intent.putExtra("name", name);
            startActivity(intent);
        }
    }

    private void deleteRelation(final String patientId, final String doctorId) throws JSONException {
        if (Utility.isNetworkAvailable(this)) {
            JSONObject object = new JSONObject();
            object.put("Patient_Id", patientId);
            object.put("Doctor_id", doctorId);
            String url = ApiCall.HOST_URL + "DeleteFromRelationship";
            createProgressDialog(this,"Deleting Relation...",false);
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i(TAG, response + "");
                    String result="";
                    try {
                        result = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(result.equals("deleted")){
                        Toast.makeText(MyPatientListActivity.this,"Relation Deleted",Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                        getPatientList();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG,error.toString());
                    mSnkBar = Snackbar.make(rootLayout, "Something Went Wrong.", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                deleteRelation(patientId,doctorId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);

        } else

        {
            mSnkBar = Snackbar.make(rootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }
}


