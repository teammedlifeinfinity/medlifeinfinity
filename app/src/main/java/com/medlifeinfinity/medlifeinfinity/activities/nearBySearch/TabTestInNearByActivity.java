package com.medlifeinfinity.medlifeinfinity.activities.nearBySearch;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.FragmentOneInNearBy;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.FragmentTwoInNearBy;

import java.util.ArrayList;
import java.util.List;

public class TabTestInNearByActivity extends AppCompatActivity {

    private static final String TAG =TabTestInNearByActivity.class.getSimpleName() ;
    //    private Toolbar toolbar;
    private TabLayout mTabLayoutTabTestInNearBy;
    private ViewPager mViewPagerTabTestInNearBy;
    private int[] tabIcons = {
            R.drawable.ic_map,
            R.drawable.ic_list,
    };
    private FragmentOneInNearBy fragmentOneInNearBy;

    static ArrayList<String> mCurrentPlaceLatLngTabTestInNearBy =new ArrayList<>();
    private static String currentPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_test_in_near_by);

        mViewPagerTabTestInNearBy = (ViewPager) findViewById(R.id.viewpager_in_tab_test_in_nearby);
        setupViewPager(mViewPagerTabTestInNearBy);

        mTabLayoutTabTestInNearBy = (TabLayout) findViewById(R.id.tabs_in_tab_test_in_nearby);
        mTabLayoutTabTestInNearBy.setupWithViewPager(mViewPagerTabTestInNearBy);
        setupTabIcons();

        Bundle bundle1=getIntent().getBundleExtra("bundle");
        mCurrentPlaceLatLngTabTestInNearBy =bundle1.getStringArrayList("mCurrentPlaceLatLngTabTestInNearBy");
        currentPlace=bundle1.getString("currentPlace");
        Log.i("currentPlaceLatLngInT", mCurrentPlaceLatLngTabTestInNearBy.toString());

    }
    private void setupTabIcons() {
        mTabLayoutTabTestInNearBy.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayoutTabTestInNearBy.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fragmentOneInNearBy =new FragmentOneInNearBy();
        adapter.addFragment(new FragmentOneInNearBy(), "MAP");
        adapter.addFragment(new FragmentTwoInNearBy(), "LIST");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public static LatLng getCurrentLatLng()
    {
        LatLng latLng=new LatLng(Double.parseDouble(mCurrentPlaceLatLngTabTestInNearBy.get(0)),Double.parseDouble(mCurrentPlaceLatLngTabTestInNearBy.get(1)));
        Log.i("currentPlacelatLngInST", mCurrentPlaceLatLngTabTestInNearBy.toString());
        return latLng;
    }
    public static String getCurrentPlace()
    {
        return currentPlace;
    }

}
