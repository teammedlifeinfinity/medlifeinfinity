package com.medlifeinfinity.medlifeinfinity.activities.qrcode;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class QRCodeScannerActivity extends AppCompatActivity implements OnQRCodeReadListener {

    private static final String TAG =QRCodeScannerActivity.class.getSimpleName();

    private QRCodeReaderView mydecoderview;
    private ImageView line_image;
    private Toolbar mToolbar;
    private ProgressDialog progressDialog;
    User mUserProfile;
    private String mDoctorId;
    private String mPatientId;
    private LinearLayout rootLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode_scanner);
        mToolbar=(Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDoctorId=User.getInstance().getmUserId();
        mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderView_activity_scanner);
        mydecoderview.setOnQRCodeReadListener(this);
        line_image = (ImageView) findViewById(R.id.image_activity_scanner);
        rootLayout= (LinearLayout) findViewById(R.id.root_qrcode_scanner_activity);
        TranslateAnimation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.9f);
        mAnimation.setDuration(2000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        line_image.setAnimation(mAnimation);
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        mydecoderview.getCameraManager().stopPreview();
        Log.e("Text", text);
        line_image.clearAnimation();
        mPatientId=text.substring(7);
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Medical Id");
        alert.setMessage("Do You Want To Continue");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if (Utility.isNetworkAvailable(QRCodeScannerActivity.this)) {
                        apicall();
                    }else{
                        Toast.makeText(QRCodeScannerActivity.this,"Internet is not connected",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        alert.show();
    }

    private void apicall() throws JSONException {
        String url= ApiCall.HOST_URL+"SaveToRelationship";
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("Patient_Id",mPatientId);
        jsonObject.put("Doctor_id",mDoctorId);
        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, url,jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG,response.toString());
                String temp=null;
                try {
                    temp=response.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(temp.equals("Record Successfully Saved"))
                {
                    Toast.makeText(QRCodeScannerActivity.this, "Patient Added successfully", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,error.toString());
                final Snackbar snkBar= Snackbar.make(rootLayout,"check for internet connection",Snackbar.LENGTH_INDEFINITE);
                snkBar.setAction("Try again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            apicall();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                snkBar.show();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    @Override
    public void cameraNotFound() {

    }

    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
