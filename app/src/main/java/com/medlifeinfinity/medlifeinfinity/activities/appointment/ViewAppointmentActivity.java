package com.medlifeinfinity.medlifeinfinity.activities.appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.activities.findDoctors.DoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.profile.UserProfileActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.AppointmentViewerAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

public class ViewAppointmentActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemClickListener {

    private static final String TAG = ViewAppointmentActivity.class.getCanonicalName();
    String mSelectedDate = "";
    String mDoctorId;
    private EditText et_date;
    private Button btn_increase, btn_decrease;
    private ListView lv_slots_preview;
    private User mUserProfile;
    private int mDay;
    private int mMonth, mYear;
    private String currentDate;
    private Toolbar mToolBar;
    private String mLauncherClass;
    private FloatingActionButton fab_addSlots;
    private int flag;
    private ProgressDialog progressDialog;
    private CoordinatorLayout mRootLayout;
    private Snackbar mSnkBar;
    AppointmentViewerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appointment);

        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("View Appointments");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mDoctorId = getIntent().getStringExtra("doctorId");
        mLauncherClass = getIntent().getStringExtra("class");
        flag = getIntent().getFlags();
        setup();
        if (Utility.isNetworkAvailable(this)) {
            getAppointment();
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getAppointment();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private void setup() {
        mRootLayout = (CoordinatorLayout) findViewById(R.id.root_view_appointment_activity);
        fab_addSlots = (FloatingActionButton) findViewById(R.id.fab_add_slots_view_appointment_activity);

        et_date = (EditText) findViewById(R.id.et_date_view_appointment_activity);
        lv_slots_preview = (ListView) findViewById(R.id.lv_slots_preview_view_appointment_activity);

        lv_slots_preview.setOnItemClickListener(this);

//        lv_slots_preview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.i(TAG,"item clicked "+position);
//                final int pos = position;
//                if (!(flag == Intent.FLAG_ACTIVITY_NEW_TASK)) {
//                    if (mAdapter.getmAppointmentList().get(position).getmAppointmentStatus()) {
//                        Toast.makeText(ViewAppointmentActivity.this, "Seleted Slot is already Booked", Toast.LENGTH_SHORT).show();
//                    } else {
//                        if (Utility.isNetworkAvailable(ViewAppointmentActivity.this)) {
//                            bookSlot(mAdapter.getmAppointmentList().get(pos).getmSlotId(), User.getInstance().getmUserId());
//                        } else {
//                            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
//                            mSnkBar.setAction("Try Again", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    bookSlot(mAdapter.getmAppointmentList().get(pos).getmSlotId(), User.getInstance().getmUserId());
//                                    mSnkBar.dismiss();
//                                }
//                            });
//                            mSnkBar.show();
//                        }
//                    }
//                }
//            }
//        });

        if (flag == Intent.FLAG_ACTIVITY_NEW_TASK) {
            fab_addSlots.setVisibility(View.GONE);
        }

        fab_addSlots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewAppointmentActivity.this, DoctorSlotMakerActivity.class);
                intent.putExtra("select date", mSelectedDate);
                startActivity(intent);
            }
        });

        parseCurrentDate(DateTime.now(TimeZone.getDefault()));

        Log.i(TAG, mSelectedDate + "");
        et_date.setText(getDate(mYear, mMonth, mDay));

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                Calendar tempCalendar = Calendar.getInstance();
                Calendar tempCalendar2 = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(ViewAppointmentActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Select Date");
                dpd.vibrate(true);
                tempCalendar.add(Calendar.DAY_OF_WEEK, 0);
                dpd.setMinDate(tempCalendar);
                tempCalendar2.add(Calendar.MONTH, 1);
                dpd.setMaxDate(tempCalendar2);
                dpd.dismissOnPause(true);
                dpd.show(getFragmentManager(), "DatePickerDialog");
            }
        });


    }

    private void getAppointment() {
        Log.i(TAG, mDoctorId + " " + mSelectedDate);
        String url = ApiCall.HOST_URL + "SelectFromAppointment/" + mDoctorId + "/" + mSelectedDate;
        createProgressDialog(this, "Loading...", false);
        JsonArrayRequest objectRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                parseResponse(response);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSnkBar.dismiss();
                        getAppointment();

                    }
                });
                mSnkBar.show();
                hideProgressDialog();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue().add(objectRequest);
    }

    private void bookSlot(final String slotId, final String userId) {
        JSONObject object = new JSONObject();
        try {
            object.put("Slot_Id", slotId);
            object.put("BookBy_Id", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG, object.toString());

        String url = ApiCall.HOST_URL + "UpdateAppointment";
        createProgressDialog(this, "Booking Time Slot....", false);
        if (Utility.isNetworkAvailable(this)) {
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String result = "";
                    Log.i(TAG, response.toString());
                    try {
                        result = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (result.equals("Updated")) {
                        Toast.makeText(ViewAppointmentActivity.this, "Appointment booked", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                        getAppointment();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            bookSlot(slotId, userId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    bookSlot(slotId, userId);
                }
            });
            mSnkBar.show();
        }

    }

    private void parseCurrentDate(DateTime currentDate) {
        mYear = currentDate.getYear();
        mSelectedDate = mSelectedDate + mYear;
        String temp = "";
        mMonth = currentDate.getMonth();
        if (mMonth < 10) {
            temp = temp + "0" + mMonth;
        } else {
            temp = temp + mMonth;
        }
        mSelectedDate = mSelectedDate + temp;
        temp = "";
        mDay = currentDate.getDay();
        if (mDay < 10) {
            temp = temp + "0" + mDay;
        } else {
            temp = temp + mDay;
        }
        mSelectedDate = mSelectedDate + temp;
        Log.i(TAG, mSelectedDate);
    }

    private String getDate(int yy, int mm, int dd) {
        return dd + "-" + mm + "-" + yy;
    }

    private void parseResponse(JSONArray response) {
        mAdapter = new AppointmentViewerAdapter(getApplicationContext(), response, mLauncherClass);
        lv_slots_preview.setAdapter(mAdapter);
        if (response.length() == 0) {
            mSnkBar = Snackbar.make(mRootLayout, "No data found", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        et_date.setText(getDate(year, ++monthOfYear, dayOfMonth));
        mSelectedDate = "";
        mSelectedDate = mSelectedDate + year;
        String temp = "";
        if (monthOfYear < 10) {
            temp = temp + "0" + monthOfYear;
        } else {
            temp = temp + monthOfYear;
        }
        mSelectedDate = mSelectedDate + temp;
        temp = "";
        if (dayOfMonth < 10) {
            temp = temp + "0" + dayOfMonth;
        } else {
            temp = temp + dayOfMonth;
        }
        mSelectedDate = mSelectedDate + temp;
        Log.i(TAG, mSelectedDate + "");
        if (Utility.isNetworkAvailable(this)) {
            getAppointment();
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getAppointment();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }


    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
        long viewId = view.getId();
        if (viewId == R.id.card_view_appointment_doctor_list_item) {
            if (mLauncherClass.equals(DoctorListActivity.class.getSimpleName())) {
                Log.i(TAG, "Position " + position);
                if (mAdapter.getmAppointmentList().get(position).getmAppointmentStatus()) {
                    Toast.makeText(ViewAppointmentActivity.this, "Slot is already booked", Toast.LENGTH_SHORT).show();
                } else {
                    bookSlot(mAdapter.getmAppointmentList().get(position).getmSlotId(), User.getInstance().getmUserId());
                }
            }
        }

        if (viewId == R.id.ib_overflow_menu_appointment_doctor_list_item_card_view) {
            Log.i(TAG, " over flow menu Position " + position);
            ImageButton ib_overflow = (ImageButton) view;
            if (mLauncherClass.equals(DashboardActivity.class.getSimpleName())) {
                PopupMenu popupMenu = new PopupMenu(getApplicationContext(), ib_overflow);
                popupMenu.inflate(R.menu.menu_doctor_appointment_item_overflow);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu_drop_slot) {
                            Log.i(TAG, "Drop");
                            dropSlot(mAdapter.getmAppointmentList().get(position).getmSlotId());

                        }
                        if (item.getItemId() == R.id.menu_view_patient) {
                            if (mAdapter.getmAppointmentList().get(position).getmAppointmentStatus()) {
                                Intent intent = new Intent(ViewAppointmentActivity.this, UserProfileActivity.class);
                                intent.putExtra("userId", mAdapter.getmAppointmentList().get(position).getmBookedBy());
                                intent.putExtra("class",DashboardActivity.class.getSimpleName());
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(ViewAppointmentActivity.this,"Slot is not booked yet",Toast.LENGTH_SHORT).show();
                            }

                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        }
    }

    private void dropSlot(final String slotId) {
        JSONObject object = new JSONObject();
        try {
            object.put("Slot_Id", slotId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG, object.toString());

        String url = ApiCall.HOST_URL + "DeleteFromAppointment";
        createProgressDialog(this, "Deleting Time Slot....", false);
        if (Utility.isNetworkAvailable(this)) {
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String result = "";
                    Log.i(TAG, response.toString());
                    try {
                        result = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (result.equals("Deleted")) {
                        Toast.makeText(ViewAppointmentActivity.this, "Slot Deleted", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                        getAppointment();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dropSlot(slotId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    dropSlot(slotId);
                }
            });
            mSnkBar.show();
        }

    }
}
