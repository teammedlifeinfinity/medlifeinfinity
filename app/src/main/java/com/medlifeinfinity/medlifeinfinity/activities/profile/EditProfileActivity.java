package com.medlifeinfinity.medlifeinfinity.activities.profile;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;

import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hirondelle.date4j.DateTime;

/**
 * Created by Vishal Chugh on 016 , 16/May/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private static final int CAMERA_REQUEST = 101;
    private static final int GALLERY_REQUEST = 102;
    private static final String TAG = EditProfileActivity.class.getCanonicalName();
    private Toolbar toolbar;
    private EditText etUserNameEditProfile, etUserEmailIdEditProfile, etUserContactEditProfile;
    private EditText etUserAddressLine1EditProfile, etUserAddressLine2EditProfile,  etUserAddressCityEditProfile, etUserAddressStateEditProfile;
    private EditText etUserDoctorSpecializationEditProfile, etUserDoctorQualificationEditProfile;
    private EditText etUserBloodGroupEditProfile, etUserDobEditProfile;
    private RelativeLayout relativeLayoutEditUserProfilePicture;
    private String EncodedImageforUpdation;
    private TextInputLayout tilUserNameEditProfile, tilUserEmailIdEditProfile, tilUserContactEditProfile;
    private TextInputLayout tilUserAddressLine1EditProfile, tilUserAddressLine2EditProfile, tilUserAddressCityEditProfile, tilUserAddressStateEditProfile;
    private TextInputLayout tilUserBloodGroupEditProfile, tilUserDobEditProfile;
    private TextInputLayout tilUserDoctorSpecializationEditProfile, tilUserDoctorQualificationEditProfile;
    private AlertDialog.Builder picturePickerDialog;
    private AlertDialog OptionDialog;
    private ImageView ivUserProfilePictureEditProfile;
    private AlertDialog.Builder bloodGroupPickerDialog;
    private int flagChange;
    private AlertDialog.Builder exitCheck;
    private User mUserInstance;
    private ProgressDialog progressDialog;
    private RadioGroup rg_gender;
    private RadioButton rb_male, rb_female;
    private User mUserProfile;
    private CardView cvUserDoctorDetailsEditProfile;
    private Bitmap tempBitmap;
    private StringBuffer mCurrentEncodedImage;
    private int tempAge;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.saveProfileMenuOption: {
                CheckForChanges();
                Toast.makeText(getApplicationContext(), "Save Profile Menu CLicked", Toast.LENGTH_SHORT).show();
                Log.i("LPU", getUpdatedUserDetails().toString());
                setChangedData();

                // TODO: Validate Fields and save the data in database through api
                flagChange = 0;
//                startActivity(new Intent(getApplicationContext(), EditProfileActivity.class));
                return true;
            }
            case android.R.id.home: {
                Toast.makeText(EditProfileActivity.this, "Navigation Up  Clicked", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void CheckForChanges() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        toolbar = (Toolbar) findViewById(R.id.appBar_edit_user_profile);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        generateView();
        eventListeners();
        setUpProfile();
    }

    private void generateView() {
        ivUserProfilePictureEditProfile = (ImageView) findViewById(R.id.iv_user_profile_picture_edit_profile_layout);
        cvUserDoctorDetailsEditProfile = (CardView) findViewById(R.id.cv_user_doctor_details_edit_user_profile_layout);
        etUserNameEditProfile = (EditText) findViewById(R.id.et_user_name_edit_user_profile_layout);
        etUserEmailIdEditProfile = (EditText) findViewById(R.id.et_user_email_edit_user_profile_layout);
        etUserContactEditProfile = (EditText) findViewById(R.id.et_user_contact_number_edit_user_profile_layout);
        etUserAddressLine1EditProfile = (EditText) findViewById(R.id.et_user_address_line1_edit_user_profile_layout);
        etUserAddressLine2EditProfile = (EditText) findViewById(R.id.et_user_address_line2_edit_user_profile_layout);
        etUserAddressCityEditProfile = (EditText) findViewById(R.id.et_user_address_city_edit_user_profile_layout);
        etUserAddressStateEditProfile = (EditText) findViewById(R.id.et_user_address_state_edit_user_profile_layout);
        etUserBloodGroupEditProfile = (EditText) findViewById(R.id.et_user_blood_group_edit_user_profile_layout);
        etUserDoctorSpecializationEditProfile = (EditText) findViewById(R.id.et_user_doctor_specialization_user_profile_main_layout);
        etUserDoctorQualificationEditProfile = (EditText) findViewById(R.id.et_user_doctor_qualification_user_profile_main_layout);
        rb_male = (RadioButton) findViewById(R.id.rb_user_male_gender_edit_user_profile_layout);
        rb_female = (RadioButton) findViewById(R.id.rb_user_female_gender_edit_user_profile_layout);
        etUserBloodGroupEditProfile.setInputType(InputType.TYPE_NULL);
        etUserDobEditProfile = (EditText) findViewById(R.id.et_user_dob_edit_user_profile_layout);
        etUserDobEditProfile.setInputType(InputType.TYPE_NULL);

        relativeLayoutEditUserProfilePicture = (RelativeLayout) findViewById(R.id.r_layout_edit_user_profile_picture);
        tilUserNameEditProfile = (TextInputLayout) findViewById(R.id.til_user_name_edit_user_profile_layout);
        tilUserEmailIdEditProfile = (TextInputLayout) findViewById(R.id.til_user_email_edit_user_profile_layout);
        tilUserContactEditProfile = (TextInputLayout) findViewById(R.id.til_user_contact_number_edit_user_profile_layout);
        tilUserAddressLine1EditProfile = (TextInputLayout) findViewById(R.id.til_user_address_line1_edit_user_profile_layout);
        tilUserAddressLine2EditProfile = (TextInputLayout) findViewById(R.id.til_user_address_line2_edit_user_profile_layout);
        tilUserAddressCityEditProfile = (TextInputLayout) findViewById(R.id.til_user_address_city_edit_user_profile_layout);
        tilUserAddressStateEditProfile = (TextInputLayout) findViewById(R.id.til_user_address_state_edit_user_profile_layout);
        tilUserBloodGroupEditProfile = (TextInputLayout) findViewById(R.id.til_user_blood_group_edit_user_profile_layout);
        tilUserDobEditProfile = (TextInputLayout) findViewById(R.id.til_user_dob_edit_user_profile_layout);
        tilUserDoctorSpecializationEditProfile = (TextInputLayout) findViewById(R.id.til_user_doctor_specialization_user_profile_main_layout);
        tilUserDoctorQualificationEditProfile = (TextInputLayout) findViewById(R.id.til_user_doctor_qualification_user_profile_main_layout);
    }

    private void setUpProfile() {
        mUserInstance = User.getInstance();
        switch (mUserInstance.getmRole()) {

            case User.DOCTOR_ROLE: //Doctor Profile
            {
                Toast.makeText(EditProfileActivity.this, "Doctor Profile" + mUserInstance.getmRole(), Toast.LENGTH_SHORT).show();
                cvUserDoctorDetailsEditProfile.setVisibility(View.VISIBLE);
                if (checkEmptyViewFields(mUserInstance.getmSpecialization())) {
                    etUserDoctorSpecializationEditProfile.setText("");
                } else {
                    etUserDoctorSpecializationEditProfile.setText(mUserInstance.getmSpecialization());
                }

                if (checkEmptyViewFields(mUserInstance.getmQualification())) {
                    etUserDoctorQualificationEditProfile.setText("");
                } else {
                    etUserDoctorQualificationEditProfile.setText(mUserInstance.getmQualification());
                }
                break;
            }
            case User.PATIENT_ROLE: //Patient Profile
            {
                Toast.makeText(EditProfileActivity.this, "Patient Profile" + mUserInstance.getmRole(), Toast.LENGTH_SHORT).show();
                cvUserDoctorDetailsEditProfile.setVisibility(View.GONE);
                break;
            }
            case User.CHEMIST_ROLE: //Chemist Profile
            {
                Toast.makeText(EditProfileActivity.this, "Chemist Profile" + mUserInstance.getmRole(), Toast.LENGTH_SHORT).show();
                cvUserDoctorDetailsEditProfile.setVisibility(View.GONE);
                // TODO: 021 , 21/Apr/2016 Chemist Layout to be added in future and set its visibility to visible here.and gone elsewhere.
                break;
            }
        }
        if (checkEmptyViewFields(mUserInstance.getmName())) {
            etUserNameEditProfile.setText("");
        } else {
            etUserNameEditProfile.setText(mUserInstance.getmName());
        }

        if (checkEmptyViewFields(mUserInstance.getmEmail())) {
            etUserEmailIdEditProfile.setText("");
        } else {
            etUserEmailIdEditProfile.setText(mUserInstance.getmEmail());
        }

        imageStringRequestAndSetImageinEditProfile(mUserInstance.getmProfliePhoto());
//            mUserInstance.getmProfilePhoto();
//            ivUserProfilePictureEditProfile.setImageDrawable();

        if (checkEmptyViewFields(mUserInstance.getmBloodGroup())) {
            etUserBloodGroupEditProfile.setText("");
        } else {
            etUserBloodGroupEditProfile.setText(mUserInstance.getmBloodGroup());
        }
        if (checkEmptyViewFields(mUserInstance.getmContactNumber())) {
            etUserContactEditProfile.setText("");
        } else {
            etUserContactEditProfile.setText(mUserInstance.getmContactNumber());
        }
        if (checkEmptyViewFields(mUserInstance.getmGender())) {

        } else {
            if (mUserInstance.getmGender().equals("Male")) {
                rb_male.setChecked(true);
            }
            if (mUserInstance.getmGender().equals("Female")) {
                rb_female.setChecked(true);
            }
        }
        if (checkEmptyViewFields(mUserInstance.getmCity())) {
            etUserAddressCityEditProfile.setText("");
        } else {
            etUserAddressCityEditProfile.setText(mUserInstance.getmCity());
        }
        if (checkEmptyViewFields(mUserInstance.getmDob())) {
            etUserDobEditProfile.setText("");
        } else {
            etUserDobEditProfile.setText(mUserInstance.getmDob());
        }
        if (checkEmptyViewFields(mUserInstance.getmState())) {
            etUserAddressStateEditProfile.setText("");
        } else {
            etUserAddressStateEditProfile.setText(mUserInstance.getmState());
        }


        String fullAddress = mUserInstance.getmAddress().toString();
        if (checkEmptyViewFields(fullAddress)) {
            etUserAddressLine1EditProfile.setText("");
            etUserAddressLine2EditProfile.setText("");

        } else {

            if (fullAddress.contains(",")) {
                String addressLine1 = fullAddress.substring(0, fullAddress.lastIndexOf(","));
                String addressLine2 = fullAddress.substring(fullAddress.lastIndexOf(",") + 1, fullAddress.length());
                etUserAddressLine1EditProfile.setText(addressLine1);
                etUserAddressLine2EditProfile.setText(addressLine2);
            } else {
                etUserAddressLine1EditProfile.setText(fullAddress);
            }
        }
    }

    private boolean checkEmptyViewFields(String str) {
        if (str.equals("NA")) {
            return true;
        } else {
            return false;
        }
    }

//    private File createImageFile() throws IOException {
//        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = mUserInstance.getmName() + "_ProfilePic_" + timeStamp;
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentEncodedImage = "file:" + image.getAbsolutePath();
//        return image;
//    }

    //    private void galleryAddPic() {
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        File f = new File(mCurrentEncodedImage);
//        Uri contentUri = Uri.fromFile(f);
//        mediaScanIntent.setData(contentUri);
//        this.sendBroadcast(mediaScanIntent);
//    }
//    private File createImageFile() throws IOException {
//        // Create an image file name
////        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//        String imageFileName = "medlyf" + "_";
//        File storageDir = Environment.getExternalStoragePublicDirectory(
//                Environment.DIRECTORY_PICTURES);
//        File image = File.createTempFile(
//                imageFileName,  /* prefix */
//                ".jpg",         /* suffix */
//                storageDir      /* directory */
//        );
//
//        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentEncodedImage = "file:" + image.getAbsolutePath();
//        Log.i("TAG", "Current path is " + mCurrentEncodedImage);
//        return image;
//    }

    private void eventListeners() {
        relativeLayoutEditUserProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picturePickerDialog = new AlertDialog.Builder(EditProfileActivity.this);
                picturePickerDialog.setTitle("SELECT PHOTO");
                View viewDialog = getLayoutInflater().inflate(R.layout.custom_dialog_select_photo, null);
                picturePickerDialog.setView(viewDialog);
                viewDialog.findViewById(R.id.lLayoutCameraClick).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "Camera Selected");
                        OptionDialog.dismiss();
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

//                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, "new-user-profile-picture.png");
//                        // Ensure that there's a camera activity to handle the intent
//                        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
//                            // Create the File where the photo should go
//                            File photoFile = null;
//                            try {
//                                photoFile = createImageFile();
//                            } catch (IOException ex) {
//                                // Error occurred while creating the File
//                            }
//                            // Continue only if the File was successfully created
//                            if (photoFile != null) {
//                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                                        Uri.fromFile(photoFile));
//
//                            }
//                        }

                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                });
                viewDialog.findViewById(R.id.lLayoutGalleryClick).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OptionDialog.dismiss();
                        Log.i(TAG, "Gallery Selected");

                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, GALLERY_REQUEST);
                    }
                });
                OptionDialog = picturePickerDialog.create();
                OptionDialog.show();

            }
        });
        etUserBloodGroupEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] bloodGroupitems = {"O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-",};
                bloodGroupPickerDialog = new AlertDialog.Builder(EditProfileActivity.this);
                bloodGroupPickerDialog.setTitle("SELECT YOUR BLOOD GROUP");
                bloodGroupPickerDialog.setItems(bloodGroupitems, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        OptionDialog.dismiss();
                        String seletectedBloodGroup = bloodGroupitems[position].toString();
                        Log.d(TAG, "Selected Blood Group is " + seletectedBloodGroup);
                        if (!seletectedBloodGroup.isEmpty()) {
                            etUserBloodGroupEditProfile.setText(seletectedBloodGroup);
                            flagChange = 1;
                        }
                    }
                });
                OptionDialog = bloodGroupPickerDialog.create();
                OptionDialog.show();
            }
        });
        etUserDobEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar now = Calendar.getInstance();
                Calendar tempCalendar2 = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(EditProfileActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Select your Birth date");
                dpd.vibrate(true);
                tempCalendar2.add(Calendar.DAY_OF_WEEK, 0);
                dpd.setMaxDate(tempCalendar2);
                dpd.dismissOnPause(true);
                dpd.show(getFragmentManager(), "DatePickerDialog");
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ivUserProfilePictureEditProfile.setImageBitmap(photo);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "medlyf" + "_" + timeStamp;
            //File file = new File(Environment.getExternalStorageDirectory() + File.separator + "Pro.jpg");
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File fileImage;
            try {
                fileImage = File.createTempFile(imageFileName,  /* prefix */
                        ".jpg",         /* suffix */
                        storageDir      /* directory */
                );
                fileImage.createNewFile();
                FileOutputStream fo = new FileOutputStream(fileImage);
                fo.write(bytes.toByteArray());
                Log.i("TAG", "FILE LOC IS :" + fileImage.getAbsolutePath().toString());
                fo.close();
                tempBitmap = photo;
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File(fileImage.getPath());
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);

            } catch (IOException e) {
                e.printStackTrace();
            }

            flagChange = 1;
        }
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePath[0]);
            String picturePath = c.getString(columnIndex);
            c.close();
            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
            Log.w("path : ", picturePath);
            ivUserProfilePictureEditProfile.setImageBitmap(thumbnail);
            tempBitmap = thumbnail;
            flagChange = 1;
        }


    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String selectedDob = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        Log.d(TAG, "Selected date : " + selectedDob);
        Calendar now = Calendar.getInstance();
        int currenyYear = now.get(Calendar.YEAR);
        tempAge= currenyYear-year;
        etUserDobEditProfile.setText(tempAge);
        flagChange = 1;
    }

    @Override
    public void onBackPressed() {
        Log.i(TAG, "" + flagChange);
        if (flagChange == 1) {
            exitCheck = new AlertDialog.Builder(EditProfileActivity.this);
            exitCheck.setMessage("Some Data has changed. do you want to save it.");
            exitCheck.setTitle("Warning");
            exitCheck.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(EditProfileActivity.this, "data Saved", Toast.LENGTH_SHORT).show();
                    // TODO: 020 , 20/Apr/2016  write code to save the profile data
                    setChangedData();
                    flagChange = 0;
                    onBackPressed();
                }
            });
            exitCheck.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(EditProfileActivity.this, "data Not Saved", Toast.LENGTH_SHORT).show();
                    flagChange = 0;
                    onBackPressed();
                }
            });
            exitCheck.show();
        } else {
            super.onBackPressed();
        }
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void setChangedData() {
        mUserInstance = User.getInstance();
        if (checkEmptyFields(etUserBloodGroupEditProfile)) {
            mUserInstance.setmBloodGroup("NA");
        } else {
            mUserInstance.setmBloodGroup(etUserBloodGroupEditProfile.getText().toString());
        }
        if (checkEmptyFields(etUserDobEditProfile)) {
            mUserInstance.setmDob("NA");
        } else {

            mUserInstance.setmDob(etUserDobEditProfile.getText().toString());
        }
        if (checkEmptyFields(etUserContactEditProfile)) {
            mUserInstance.setmContactNumber("NA");
        } else {
            mUserInstance.setmContactNumber(etUserContactEditProfile.getText().toString());
        }
        if (checkEmptyFields(etUserDoctorQualificationEditProfile)) {
            mUserInstance.setmQualification("NA");
        } else {
            mUserInstance.setmQualification(etUserDoctorQualificationEditProfile.getText().toString());
        }
        if (checkEmptyFields(etUserAddressCityEditProfile)) {
            mUserInstance.setmCity("NA");
        } else {
            mUserInstance.setmCity(etUserAddressCityEditProfile.getText().toString());
        }
        if (rb_male.isChecked()) {
            mUserInstance.setmGender("Male");
        } else if (rb_female.isChecked()) {
            mUserInstance.setmGender("Female");
        } else {
            mUserInstance.setmGender("NA");
        }

        if (checkEmptyFields(etUserAddressStateEditProfile)) {
            mUserInstance.setmState("NA");
        } else {
            mUserInstance.setmState(etUserAddressStateEditProfile.getText().toString());
        }
        if (checkEmptyFields(etUserAddressLine1EditProfile) && checkEmptyFields(etUserAddressLine2EditProfile)) {
            mUserInstance.setmAddress("NA");
        } else {
            if (checkEmptyFields(etUserAddressLine1EditProfile)) {
                mUserInstance.setmAddress(etUserAddressLine2EditProfile.getText().toString());
            } else {
                if (checkEmptyFields(etUserAddressLine2EditProfile)) {
                    mUserInstance.setmAddress(etUserAddressLine1EditProfile.getText().toString());
                } else {
                    mUserInstance.setmAddress(etUserAddressLine1EditProfile.getText().toString() + ", " + etUserAddressLine2EditProfile.getText().toString());
                }

            }
        }
        BitmapDrawable bmdr = (BitmapDrawable) ivUserProfilePictureEditProfile.getDrawable();
        Bitmap bm = bmdr.getBitmap();
        mCurrentEncodedImage= new StringBuffer(ImageEncodeToBase64(bm));
        Log.w("IMage Base Value : ", mCurrentEncodedImage.toString());
        updateUserDetailsWithApi(getUpdatedUserDetails());
    }

    private void getmUserProfile(final String userId) {
        createProgressDialog(this,"updating...",false);
        String url = ApiCall.HOST_URL.toString() + "GetUserDetailswithId/" + userId;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                try {
                    User.getUserFromJsonArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mUserProfile = User.getInstance();
                User.storeInPreference(response.toString(), getApplicationContext());
                Log.i(TAG, mUserProfile.toString());
                hideProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void updateUserDetailsWithApi(JSONObject userDetails) {
        String url = ApiCall.HOST_URL.toString() + "UploadImage/";
        createProgressDialog(this, "Updating Details", false);
        JsonObjectRequest updateUserDetailsRequest = new JsonObjectRequest(Request.Method.POST, url, userDetails, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, response.toString());
                try {
                    if (response.getString("status") != null) {
                        Log.i("Status", response.getString("status1"));
                        getmUserProfile(mUserInstance.getmUserId());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,error.toString());
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        JSONObject obj = new JSONObject(res);
                        Log.w("Error Volley", obj.toString());
                    } catch (UnsupportedEncodingException e1) {
                        // Couldn't properly decode data to string
                        Log.w("Error Volley", e1.toString());
                        e1.printStackTrace();
                    } catch (JSONException e2) {
                        // returned data is not JSONObject?
                        Log.w("Error Volley", e2.toString());
                        e2.printStackTrace();
                    }
                }
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(getApplicationContext()).getRequestQueue().add(updateUserDetailsRequest);
    }

    private String ImageEncodeToBase64(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] image = stream.toByteArray();
        Log.i("Image Byte Array", image.toString());
        String encodedImage = Base64.encodeToString(image, Base64.DEFAULT);
        Log.i("Image Encoded in class:", encodedImage);
        return encodedImage;
    }


    private JSONObject getUpdatedUserDetails() {
        JSONObject userUpdatedInfo = new JSONObject();
        try {
            userUpdatedInfo.put("user_id", mUserInstance.getmUserId());
            userUpdatedInfo.put("Name", mUserInstance.getmName());
            userUpdatedInfo.put("Contact_No", mUserInstance.getmContactNumber());
            userUpdatedInfo.put("Gender", mUserInstance.getmGender());
            userUpdatedInfo.put("Age", mUserInstance.getmDob());
            userUpdatedInfo.put("Blood_Group", mUserInstance.getmBloodGroup());
            userUpdatedInfo.put("Address", mUserInstance.getmAddress());
            userUpdatedInfo.put("City", mUserInstance.getmCity());
            userUpdatedInfo.put("State", mUserInstance.getmState());
            userUpdatedInfo.put("Country", mUserInstance.getmCountry());
            userUpdatedInfo.put("Latitude", mUserInstance.getmLatitude());
            userUpdatedInfo.put("Longitude", mUserInstance.getmLongitude());
            userUpdatedInfo.put("Specialization", mUserInstance.getmSpecialization());
            userUpdatedInfo.put("Qualification", mUserInstance.getmQualification());
            userUpdatedInfo.put("About_Doctor", "null");
            userUpdatedInfo.put("Image", mCurrentEncodedImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, userUpdatedInfo.toString());
        return userUpdatedInfo;
    }

    private void imageStringRequestAndSetImageinEditProfile(String profilePictureUrl) {
        String imageURL = ApiCall.IMAGE_URL.toString() + profilePictureUrl;

        ImageLoader mImageLoader = VolleySingleton.getInstance(this).getImageLoader();
        mImageLoader.get(imageURL, ImageLoader.getImageListener(ivUserProfilePictureEditProfile, R.drawable.ic_profile_avatar, R.drawable.ic_warning));


//        VolleySingleton.getInstance(getApplicationContext()).getRequestQueue().add(stringRequest);

//        return ImageResponse;
    }

    private boolean checkEmptyFields(EditText editTextCameForCheck) {

        if (editTextCameForCheck.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

}
