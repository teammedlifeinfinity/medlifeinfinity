package com.medlifeinfinity.medlifeinfinity.activities.pillReminder;


import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mummyding.colorpickerdialog.ColorPickerDialog;
import com.github.mummyding.colorpickerdialog.OnColorChangedListener;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.ComplexPreferencesForPillReminder;
import com.medlifeinfinity.medlifeinfinity.customClasses.PillsReminderInformation;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import java.util.Calendar;

/**
 * Created by Vishal Chugh on 011 , 11/Mar/2016.
 */
public class AddPillReminderActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private static final int DEFAULT_INT = 1;
    private final String PillsReminderListFileName = "MyPillsReminderList";
    String hourString, minuteString;
    private EditText editTextMedicineName, editTextStartDate, editTextStartTime, editTextQuantityMedicine, editTextMedicineColor, editTextInterval;
    private AlertDialog.Builder quantityPickerBuilder, intervalPickerBuilder;
    private AlertDialog OptionDialog;
    private Drawable img = null;
    private String[] strs;
    private Drawable img2 = null;
    private int mSelectedColor;
    private TextInputLayout til_med_name, til_med_color, til_med_start_date, til_med_start_time, til_med_interval, til_med_quantity;
    private boolean mFlag;
    private PillsReminderInformation pillsReminderInformation;
    private String mTempMedName, mTempMedDosage, mTempMedStartDate, mTempMedStartTime, mTempMedInterval;
    private Integer mTempMedColor;
    private int c = 0;
    private SharedPreferences sharedPreferences;
    private ComplexPreferencesForPillReminder complexPreferences;
    private int tempYear, tempMonth, tempDay, tempMinute, tempHour;
    private PendingIntent pendingIntent;
    // colors you want to add,colors must not be null!!!
    private int[] colors = new int[]{Color.YELLOW, Color.BLACK, Color.BLUE, Color.GRAY,
            Color.GREEN, Color.CYAN, Color.RED, Color.DKGRAY, Color.LTGRAY, Color.MAGENTA,
            Color.rgb(100, 22, 33), Color.rgb(82, 182, 2), Color.rgb(122, 32, 12), Color.rgb(82, 12, 2),
            Color.rgb(89, 23, 200), Color.rgb(13, 222, 23), Color.rgb(222, 22, 2), Color.rgb(2, 22, 222)};
    private Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pill_reminder);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        mToolBar.setTitle("Add Pill Reminder");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(mToolBar);
        findView();
//        Intent alarmIntent = new Intent(getApplicationContext(), PillReminderAlarmReciever.class);
//        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, 0);
    }

//    @Override
//    public void onBackPressed() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            img.setTint(Color.BLACK);
//        }
//        super.onBackPressed();
//    }

    private void findView() {
        mSelectedColor = Color.BLACK;

        strs = getResources().getStringArray(R.array.interval);
        til_med_name = (TextInputLayout) findViewById(R.id.til_medicine_name);
        til_med_color = (TextInputLayout) findViewById(R.id.til_medicine_color);
        til_med_interval = (TextInputLayout) findViewById(R.id.til_interval);
        til_med_quantity = (TextInputLayout) findViewById(R.id.til_medicine_quantity);
        til_med_start_date = (TextInputLayout) findViewById(R.id.til_start_date);
        til_med_start_time = (TextInputLayout) findViewById(R.id.til_start_time);

        editTextMedicineName = (EditText) findViewById(R.id.et_medicine_name);
        editTextMedicineName.setCursorVisible(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            img = getDrawable(R.mipmap.ic_medicine).getConstantState().newDrawable();
            img2 = getDrawable(R.mipmap.ic_color_chart);
        }

        editTextStartDate = (EditText) findViewById(R.id.et_start_date);
        editTextStartDate.setInputType(InputType.TYPE_NULL);

        editTextStartTime = (EditText) findViewById(R.id.et_start_time);
        editTextStartTime.setInputType(InputType.TYPE_NULL);

        editTextMedicineColor = (EditText) findViewById(R.id.et_medicine_color);
        editTextMedicineColor.setInputType(InputType.TYPE_NULL);

        editTextQuantityMedicine = (EditText) findViewById(R.id.et_medicine_quantity);
        editTextQuantityMedicine.setInputType(InputType.TYPE_NULL);

        editTextInterval = (EditText) findViewById(R.id.et_interval);
        editTextInterval.setInputType(InputType.TYPE_NULL);

        editTextStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                Calendar tempCalendar = Calendar.getInstance();
                Calendar tempCalendar2 = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(AddPillReminderActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Select Start Date");
                dpd.vibrate(true);
                tempCalendar.add(Calendar.DAY_OF_WEEK, 0);
                dpd.setMinDate(tempCalendar);
                tempCalendar2.add(Calendar.MONTH, 3);
                dpd.setMaxDate(tempCalendar2);
                dpd.dismissOnPause(true);
                dpd.show(getFragmentManager(), "DatePickerDialog");
            }
        });

        editTextStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(AddPillReminderActivity.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                tpd.setTitle("Select Start Time");
                tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), 0, 0);
                tpd.vibrate(true);
                tpd.dismissOnPause(true);
                tpd.show(getFragmentManager(), "TimePickerDialog");
            }
        });

        editTextQuantityMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final CharSequence[] items = {"One", "Two", "Three", "Four"};

                quantityPickerBuilder = new AlertDialog.Builder(AddPillReminderActivity.this);
                View view = getLayoutInflater().inflate(R.layout.custom_layout_title_header, null, false);
                TextView tv = (TextView) view.findViewById(R.id.tv_header_dialog);
                tv.setText("Medicine Quantity");

                quantityPickerBuilder.setCustomTitle(view);

                quantityPickerBuilder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        int loc = position + 1;
                        OptionDialog.dismiss();
                        editTextQuantityMedicine.setText(loc + " unit");
                        Log.d("TAG", "Position is " + loc);
                    }
                });
                OptionDialog = quantityPickerBuilder.create();
                OptionDialog.setCancelable(false);
                OptionDialog.setCanceledOnTouchOutside(false);
                OptionDialog.show();
            }
        });

        editTextMedicineColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ColorPickerDialog cpd = new ColorPickerDialog(AddPillReminderActivity.this, colors).
                        setTitle("Choose the Color of Medicine")
                        .setCheckedColor(Color.BLACK)
                        .setOnColorChangedListener(new OnColorChangedListener() {
                            @Override
                            public void onColorChanged(int color) {
                                mSelectedColor = color;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    img.setTint(color);
                                    editTextMedicineColor.setCompoundDrawablesWithIntrinsicBounds(img2, null, img, null);
                                }
                            }
                        })
                        .build();
                cpd.show();
//                ColorPickerDialog cpd = ColorPickerDialog.newInstance("Choose the color of medicine", new int[]{Color.BLACK, Color.RED, Color.GREEN, Color.MAGENTA, Color.WHITE, Color.YELLOW, Color.GRAY, Color.CYAN}, mSelectedColor, 3, ColorPickerDialog.SIZE_LARGE);
//                cpd.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
//                    @Override
//                    public void onColorSelected(int color) {
//
//                        mSelectedColor = color;
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            img.setTint(color);
//                            editTextMedicineColor.setCompoundDrawablesWithIntrinsicBounds(img2, null, img, null);
//
//                        }
//                    }
//                });
//                cpd.show(getFragmentManager(), "ColorPicker");

            }
        });
        editTextInterval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final CharSequence[] items = {"15 Minutes", "30 Minutes", "45 Minutes", "1 Hour", "2 Hours", "3 Hours", "4 Hours", "6 Hours", "8 Hours", "10 Hours", "12 Hours", "1 Day", "2 Days", "1 Week", "15 Days", "1 Month"};

                View view = getLayoutInflater().inflate(R.layout.custom_layout_title_header, null, false);
                TextView tv = (TextView) view.findViewById(R.id.tv_header_dialog);
                tv.setText("Interval Between\n    Notifications");
                intervalPickerBuilder = new AlertDialog.Builder(AddPillReminderActivity.this);
                intervalPickerBuilder.setCustomTitle(view);
                View mView = getLayoutInflater().inflate(R.layout.interval_choose_layout, null, false);
                ListView lv = (ListView) mView.findViewById(R.id.lv_interval);
                ListAdapter listAdapter = new ArrayAdapter(AddPillReminderActivity.this, R.layout.custom_listview_centered_text_layout, items);
                lv.setAdapter(listAdapter);
                intervalPickerBuilder.setView(mView);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int loc = position + 1;
                        Log.d("TAG", "Position clicked " + loc);

                        editTextInterval.setText(strs[position] + " hours");
                        OptionDialog.dismiss();
                    }
                });
                OptionDialog = intervalPickerBuilder.create();
                OptionDialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("DatePickerDialog");
        TimePickerDialog tpd = (TimePickerDialog) getFragmentManager().findFragmentByTag("TimePickerDialog");
//        ColorPickerDialog cpd = (ColorPickerDialog) getFragmentManager().findFragmentByTag("ColorPicker");
        if (dpd != null) {
            dpd.setOnDateSetListener(this);
        }
        if (tpd != null) {
            tpd.setOnTimeSetListener(this);
        }
//        if (cpd != null) {
//            cpd.setOnColorSelectedListener((ColorPickerSwatch.OnColorSelectedListener) this);
//            cpd.setOnColorChangedListener()
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.tick_confirm: {
                boolean result = errorchecking();
                if (result) {
                    LoadPillReminderData();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void LoadPillReminderData() {
        pillsReminderInformation = new PillsReminderInformation();
        pillsReminderInformation.setmMedicineName(mTempMedName);
        pillsReminderInformation.setmMedicineDosage(mTempMedDosage);
        pillsReminderInformation.setmMedicineColor(mTempMedColor);
        pillsReminderInformation.setmMedicineStartDateTime(mTempMedStartDate.toString() + " -  " + mTempMedStartTime.toString());
        pillsReminderInformation.setmMedicineInterval(mTempMedInterval);

        complexPreferences = ComplexPreferencesForPillReminder.getComplexPreferences(this, PillsReminderListFileName, MODE_PRIVATE);
        sharedPreferences = getSharedPreferences(PillsReminderListFileName, MODE_PRIVATE);
        c = sharedPreferences.getAll().size();
        c++;
        complexPreferences.putObject("KEY" + c, pillsReminderInformation);
        complexPreferences.commit();

        setResult(RESULT_OK);
        setAlarm(pillsReminderInformation);
        onBackPressed();

    }

    private void setAlarm(PillsReminderInformation reminderInformation) {
        String interval = reminderInformation.getmMedicineInterval();
        String temp = interval.substring(0, interval.indexOf(" ")).trim();
        Log.i("TAG", "Info :" + reminderInformation.toString());
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        int inter = Integer.parseInt(temp);
        Log.i("TAG", "interval=" + inter);
        inter = inter * 1000 * 60;
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.set(Calendar.HOUR, tempHour);
        cal.set(Calendar.MINUTE, tempMinute);
        Log.i("TAG", cal.toString());
        Toast.makeText(this, "Alarm Set for " + cal.toString(), Toast.LENGTH_SHORT).show();
        manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, cal.getTimeInMillis(), inter, pendingIntent);
    }

    private boolean errorchecking() {
        if (editTextMedicineName.getText().toString().isEmpty()) {
            til_med_name.setErrorEnabled(true);
            til_med_name.setError("Please enter the name of medicine");
            mFlag = false;
        } else {
            mTempMedName = editTextMedicineName.getText().toString();
            til_med_name.setErrorEnabled(false);
            mFlag = true;
        }
        if (editTextStartDate.getText().toString().isEmpty()) {
            til_med_start_date.setErrorEnabled(true);
            til_med_start_date.setError("Invalid Date.\nplease select a valid date.");
            mFlag = false;
        } else {
            til_med_start_date.setErrorEnabled(false);
            mFlag = true;
            mTempMedStartDate = editTextStartDate.getText().toString();
        }
        if (editTextStartTime.getText().toString().isEmpty()) {
            til_med_start_time.setErrorEnabled(true);
            til_med_start_time.setError("Invalid Time.\nplease select a valid Start Time");
            mFlag = false;
        } else {
            til_med_start_time.setErrorEnabled(false);
            mFlag = true;
            mTempMedStartTime = editTextStartTime.getText().toString();
        }
        if (editTextQuantityMedicine.getText().toString().isEmpty()) {
            til_med_quantity.setErrorEnabled(true);
            til_med_quantity.setError("Please select medicine quantity");
            mFlag = false;
        } else {
            til_med_quantity.setErrorEnabled(false);
            mFlag = true;
            mTempMedDosage = editTextQuantityMedicine.getText().toString();
        }
        if (editTextInterval.getText().toString().isEmpty()) {
            til_med_interval.setErrorEnabled(true);
            til_med_interval.setError("Please Select Interval");
            mFlag = false;
        } else {
            til_med_interval.setErrorEnabled(false);
            mFlag = true;
            mTempMedInterval = editTextInterval.getText().toString();
        }

        if (mFlag) {
            mTempMedColor = mSelectedColor;
            return true;
        } else
            return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_pill, menu);
        return true;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        tempYear = year;
        tempMonth = monthOfYear;
        tempDay = dayOfMonth;

        String selectedStartDate = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        Log.d("TAG", "Selected date : " + selectedStartDate);
        editTextStartDate.setText(selectedStartDate);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        Boolean flagAmPm = true; //Default is morning A.M.


        if (hourOfDay > 12) {
            hourOfDay = hourOfDay - 12;
            flagAmPm = false; // changed to p.m.

        }
        hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        minuteString = minute < 10 ? "0" + minute : "" + minute;

        if (hourOfDay > 12) {
            tempHour = Integer.parseInt(hourString) + 12;
        } else {
            tempHour = Integer.parseInt(hourString);
        }
        tempMinute = Integer.parseInt(minuteString);
        String selectedStartTime = hourString + " : " + minuteString + "" + (flagAmPm ? " a.m." : " p.m.");
        editTextStartTime.setText(selectedStartTime);
    }
}