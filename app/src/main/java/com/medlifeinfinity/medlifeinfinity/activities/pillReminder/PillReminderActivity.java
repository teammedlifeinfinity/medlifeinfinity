package com.medlifeinfinity.medlifeinfinity.activities.pillReminder;


import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.adapters.PillReminderListViewAdapter;
import com.medlifeinfinity.medlifeinfinity.customClasses.ComplexPreferencesForPillReminder;
import com.medlifeinfinity.medlifeinfinity.customClasses.PillsReminderInformation;

public class PillReminderActivity extends AppCompatActivity {

    private static final int GET_REMINDER_DETAILS = 1;
    private static final String DEFAULT = "N/A";
    private static final int DEFAULT_INT = 1;
    private final String PillsReminderListFileName = "MyPillsReminderList";
    private int count = 0;
    private FloatingActionButton fabAddPill;
    private ListView lv_pill_reminder_list;
    private ArrayList<PillsReminderInformation> mPillReminderList;
    private PillReminderListViewAdapter mPillReminderListAdapter;
    private PillsReminderInformation pillsReminderInformation;
    private boolean mTemp;
    private SharedPreferences sharedPreferences;
    private boolean previouslyStarted;
    private ComplexPreferencesForPillReminder complexPreferences;
    private File fileName;
    private String tMedName, tMedDosage, tMedStartDateTime, tMedInterval;
    private int tMedColor;
    private Map<String, ?> stringMap;
    private String pref_previously_started = "FILE";
    private PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pill_reminder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pill Reminder");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        generateView();

    }

    private void generateView() {
        fabAddPill = (FloatingActionButton) findViewById(R.id.fab_add_reminder);
        fabAddPill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), AddPillReminderActivity.class), GET_REMINDER_DETAILS);
            }
        });
        mPillReminderList = new ArrayList<>();
        pillsReminderInformation = new PillsReminderInformation();
        mPillReminderListAdapter = new PillReminderListViewAdapter(this, mPillReminderList);
        lv_pill_reminder_list = (ListView) findViewById(R.id.lv_pill_reminder_list);
        lv_pill_reminder_list.setDividerHeight(0);
        complexPreferences = ComplexPreferencesForPillReminder.getComplexPreferences(this, PillsReminderListFileName, MODE_PRIVATE);
        sharedPreferences = getSharedPreferences(PillsReminderListFileName, MODE_PRIVATE);
        checkForFirstTime();
    }

    private void checkForFirstTime() {
        complexPreferences = ComplexPreferencesForPillReminder.getComplexPreferences(this, PillsReminderListFileName, MODE_PRIVATE);
        sharedPreferences = getSharedPreferences(PillsReminderListFileName, MODE_PRIVATE);
        try {
            logMessage("checked for first time run ");
            if (sharedPreferences.getAll().size() == 0) {
                logMessage("File Exist but Data Not available");
                mTemp = false;
            } else {
                logMessage("File Exist and Data also available");
                mTemp = true;
            }
            previouslyStarted = sharedPreferences.getBoolean(pref_previously_started, mTemp);
            if (!previouslyStarted) {
                logMessage("prefs not stored");
                fileName = getDatabasePath(PillsReminderListFileName + ".xml");
                Boolean m = fileName.delete();
                logMessage(m.toString());
            } else if (previouslyStarted) {
                logMessage("prefs Data stored");
                loadData(complexPreferences);
            }
        } catch (Exception ex) {
            logMessage("In Catch Block EXCEPTION :  " + ex);
        }
    }

    private void loadData(ComplexPreferencesForPillReminder preferencesSaved) {


        stringMap = sharedPreferences.getAll();

        count = stringMap.size();
        logMessage("Total Objects Stored ==>  " + count);
        mPillReminderList.clear();
        for (Map.Entry<String, ?> entry : stringMap.entrySet()) {
            //logMessage(entry.getKey() + ": " + entry.getValue().toString());

            PillsReminderInformation pop = complexPreferences.getObject(entry.getKey(), pillsReminderInformation.getClass());

            tMedName = pop.getmMedicineName();
            tMedDosage = pop.getmMedicineDosage();
            tMedStartDateTime = pop.getmMedicineNextTime();
            tMedColor = pop.getmMedicineColor();
            tMedInterval = pop.getmMedicineInterval();
            pillsReminderInformation.setAll(tMedName, tMedStartDateTime, tMedDosage, tMedInterval, tMedColor);
            //logMessage("" + entry.getKey());
            //logMessage(pop.toString());
            mPillReminderList.add(pop);
        }
        logMessage("Size of List ==>  " + mPillReminderList.size());
        mPillReminderListAdapter = new PillReminderListViewAdapter(this, mPillReminderList);
        lv_pill_reminder_list.setAdapter(mPillReminderListAdapter);
        mPillReminderListAdapter.notifyDataSetChanged();

    }

    private void logMessage(String str) {
        Log.d("TAG", str);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        logMessage("onActivityResult running");
        if (requestCode == GET_REMINDER_DETAILS && resultCode == RESULT_OK) {
            checkForFirstTime();
        }
    }
}
