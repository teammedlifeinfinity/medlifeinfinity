package com.medlifeinfinity.medlifeinfinity.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.Validate;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPasswordActivity extends AppCompatActivity {

    private static final String TAG = ForgetPasswordActivity.class.getSimpleName();
    Button mBtnSendPasswordForgotPassword;
    TextView mTvLabelLoginForgotPassword;
    private LinearLayout mLinearLayoutForgotPassword;
    private EditText meditTextForgotPassword;
    private Snackbar mSnackBarInfo;
    private TextInputLayout til_textForgotPassword;
    private Snackbar snkBar;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        setContentView(R.layout.activity_forget_password);
        generateViews();
        eventHandlers();

    }

    private void eventHandlers() {
        mBtnSendPasswordForgotPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String emailId = meditTextForgotPassword.getText().toString();
                if (Validate.isValid(emailId,Validate.EMAIL_PATTERN)) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    try {

                        if (connectivityManager != null && (
                                (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                                        (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {

                            //You are connected, do something online.
                            getPassword(emailId);


                        } else if (connectivityManager != null && (
                                (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) ||
                                        (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED))) {

                            //Not connected.
                            mSnackBarInfo.show();

                        }
//                Toast.makeText(EmergencyModuleMainActivity.this, "Send SOS Button pressed But it's working is disabled by admin<VishalChugh>", Toast.LENGTH_SHORT).show();

                    } catch (Exception ex) {
                        Log.i("TAG", ex.getStackTrace().toString());
                    }
                }else {
                    snkBar= Snackbar.make(mLinearLayoutForgotPassword,"please provide a valid email address",Snackbar.LENGTH_INDEFINITE);
                    snkBar.setAction("Dismiss", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snkBar.dismiss();
                        }
                    });
                    snkBar.show();
                }
            }

        });

        mTvLabelLoginForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getPassword(final String emailId) throws JSONException {
        JSONObject object=new JSONObject();
        object.put("Email",emailId);

        String url= ApiCall.HOST_URL+"ForgetPassword";

        JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG,response.toString());
                try {
                    parseResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG,error.toString());
                snkBar=Snackbar.make(mLinearLayoutForgotPassword,"Network is down",Snackbar.LENGTH_INDEFINITE);
                snkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            getPassword(meditTextForgotPassword.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        snkBar.dismiss();
                    }
                });
                snkBar.show();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(request);
    }

    private void parseResponse(JSONObject response) throws JSONException {
        String status=response.getString("status");
        mPassword=response.getString("status2");
        Log.i(TAG,mPassword);
        if(status.equals("Find Successfully")&&!mPassword.equals(""))
        {

            sendEmail(meditTextForgotPassword.getText().toString(),mPassword);

        }
        else {
            snkBar=Snackbar.make(mLinearLayoutForgotPassword,"you are not registered with us",Snackbar.LENGTH_INDEFINITE);
            snkBar.setAction("Dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snkBar.dismiss();
                }
            });
            snkBar.show();
        }
    }

    private void sendEmail(String str,String mPassword) {
        Session session = createSessionObject();

        try {
            String email = str;
            String subject = "Recovery Password";
            String messageBody = "Your Password is "+mPassword;
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {

            e.printStackTrace();
        } catch (MessagingException e) {

            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }
    }

    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("info.medlifeinfinity@gmail.com", "MedLife Infinity"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private void generateViews() {
        til_textForgotPassword= (TextInputLayout) findViewById(R.id.til_user_email_forgot_password);
        mLinearLayoutForgotPassword = (LinearLayout) findViewById(R.id.linear_layout_forgot_password_interface);
        mBtnSendPasswordForgotPassword = (Button) findViewById(R.id.btn_send_password_forgot_password);
        mTvLabelLoginForgotPassword = (TextView) findViewById(R.id.tv_label_login_forgot_password);
        meditTextForgotPassword = (EditText) findViewById(R.id.et_user_email_forgot_password_layout);
        mSnackBarInfo = Snackbar.make(mLinearLayoutForgotPassword, "Internet Not Available.\nConnect to Internet and try again", Snackbar.LENGTH_SHORT);
    }

    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        // TODO: 4/27/2016 put the account password from which you wana send email
        final String password = "medlife@INFOTECH";
        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("info.medlifeinfinity@gmail.com", password);
            }
        });
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ForgetPasswordActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Check your Email Account For Password", Toast.LENGTH_SHORT).show();
            meditTextForgotPassword.setText("");
            onBackPressed();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Something went wrong Try Again", Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }
}
