package com.medlifeinfinity.medlifeinfinity.activities.rms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RMSPostPtActivity extends AppCompatActivity {

    public static final String TAG = RMSPostPtActivity.class.getSimpleName();
    Spinner spn_MessageType;
    Button btn_submit, btn_cancel;
    EditText et_Description;
    private ProgressDialog progressDialog;
    private Toolbar mToolBar;
    private User mUserProfile;
    private String mPatientId;
    private String mDoctorId;
    private LinearLayout mRootLayout;
    private Snackbar mSnkBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rms_post_pt);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Add RMS");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        mUserProfile = User.getInstance();
        setup();

    }

    private void setup() {
        spn_MessageType = (Spinner) findViewById(R.id.spn_message_type_rmspost_pt_activity);
        btn_submit = (Button) findViewById(R.id.btn_submit_rmspost_pt_activity);
        btn_cancel = (Button) findViewById(R.id.btn_cancel_rmspost_pt_activity);
        et_Description = (EditText) findViewById(R.id.et_description_rmspost_pt_activity);
        mRootLayout = (LinearLayout) findViewById(R.id.root_rms_post_pt_activity);

        List<String> list = new ArrayList<>();
        list.add("Select Option");
        list.add("Complaint");
        list.add("Compliment");
        list.add("Enquiry");
        list.add("Feedback");
        list.add("Request");
        list.add("Suggestion");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(RMSPostPtActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_MessageType.setAdapter(arrayAdapter);


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                postRMSPost();
            }
        });
    }

    private void postRMSPost() {

        String V_Description = et_Description.getText().toString();
        String V_MessageType = spn_MessageType.getSelectedItem().toString();


        //Validation
        if (V_MessageType.equals("") || V_MessageType.equalsIgnoreCase("Select Option")) {

            AlertDialog.Builder builder = new AlertDialog.Builder(RMSPostPtActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("PLEASE SELECT MESSAGE TYPE");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    spn_MessageType.setFocusable(true);
                    spn_MessageType.setFocusableInTouchMode(true);
                    spn_MessageType.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        } else if (V_Description.length() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(RMSPostPtActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("PLEASE ADD DESCRIPTION");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    et_Description.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        } else if (V_Description.length() < 7) {
            AlertDialog.Builder builder = new AlertDialog.Builder(RMSPostPtActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("DESCRIPTION MUST BE MORE THE 7 CHARACTER");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    et_Description.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();

        } else {

            JSONObject requestJSONObject = createRequestJSON(V_MessageType, mDoctorId, "NA", V_Description, mPatientId);
            createProgressDialog(this, "Loading ... ", false);

            String url = ApiCall.HOST_URL + "InsertFeedbackIntoRMS";

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "response " + response.toString());
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "Successfully Data Inserted", Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {//not response the show the error
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Error here " + error.toString());
                            hideProgressDialog();
                            mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    postRMSPost();
                                    mSnkBar.dismiss();
                                }
                            });
                            mSnkBar.show();
                        }
                    });
            VolleySingleton.getInstance(getApplicationContext()).getRequestQueue().add(request);
            cancel();
        }
    }

    private JSONObject createRequestJSON(String vMessageType, String doctorId, String vName, String vDescription, String vRmsBy) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Message_Type", vMessageType);
            jsonObject.put("SendTo_Id", doctorId);
            jsonObject.put("SendTo_Name", vName);
            jsonObject.put("Description", vDescription);
            jsonObject.put("Entry_By", vRmsBy);
        } catch (JSONException exp) {
            Log.e(TAG, " Error : " + exp);
        }
        return jsonObject;
    }

    private void cancel() {
        et_Description.setText("");
        spn_MessageType.setSelection(0);
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
