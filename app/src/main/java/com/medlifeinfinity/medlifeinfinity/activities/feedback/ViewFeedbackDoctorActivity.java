package com.medlifeinfinity.medlifeinfinity.activities.feedback;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class ViewFeedbackDoctorActivity extends AppCompatActivity {


    public static final String TAG = ViewFeedbackDoctorActivity.class.getSimpleName();

    public static String Title="";
    public static String Rating;
    public static String Comment;

    public static final String KEY_RATING = "Rating";
    public static final String KEY_TITLE = "Behaviour";
    public static final String KEY_COMMENT = "Comment";

    private ProgressDialog progressDialog;
    private Toolbar mToolBar;
    User mUserProfile;
    private String mDoctorId;
    private String mPatientId;
    private int flag;
    TextView tv_Title, tv_Rating, tv_Comment;
    ImageView iv_FirstLetter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_feedback_doctor);
        mUserProfile = User.getInstance();
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        flag = getIntent().getFlags();
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Feedback");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        sendRequest();
        setup();
    }

    private void setup() {
        tv_Rating = (TextView)findViewById(R.id.tv_ratings_listItem_custom_lv_for_doctor_display);
        tv_Title = (TextView)findViewById(R.id.tv_title_listItem);
        tv_Comment = (TextView) findViewById(R.id.tv_Comment_listItem_custom_lv_for_doctor_display);
        iv_FirstLetter = (ImageView)findViewById(R.id.iv_DoctorFeedbackViewFirstName_custom_lv_for_doctor_display);
    }

    private void sendRequest() {
        String url = ApiCall.HOST_URL + "GetDoctorFeedbback/" + mDoctorId+"/"+mPatientId;
        createProgressDialog(this, "Loading..", false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Response " + response);
//                Toast.makeText(ViewFeedbackDoctorActivity.this, "Response Received Successfully", Toast.LENGTH_SHORT).show();
                parseResponse(response);
                hideProgressDialog();
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ViewFeedbackDoctorActivity.this, "Error Please check Your Internet Connection" + error.getMessage(), Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        });
        VolleySingleton.getInstance(this).getRequestQueue().add(stringRequest);
    }

    private void parseResponse(String json) {
        JSONArray users = null;
        try {
            users = new JSONArray(json);
            JSONObject jo = users.getJSONObject(users.length()-1);
            Rating = jo.getString(KEY_RATING);
            Title = jo.getString(KEY_TITLE);
            Comment = jo.getString(KEY_COMMENT);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        tv_Rating.setText(Rating);
        tv_Title.setText(Title);
        tv_Comment.setText(Comment);
        String letter;
        if(!Title.equals(""))
        {
            letter = String.valueOf(Title.charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(new Random().nextInt(100));
            TextDrawable Drawable = TextDrawable.builder().buildRound(letter, color);
            iv_FirstLetter.setImageDrawable(Drawable);
        }
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
