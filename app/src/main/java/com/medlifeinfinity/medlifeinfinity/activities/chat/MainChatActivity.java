package com.medlifeinfinity.medlifeinfinity.activities.chat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.medlifeinfinity.medlifeinfinity.customClasses.Message;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.pusher.client.Pusher;
import com.google.gson.Gson;
import com.medlifeinfinity.medlifeinfinity.R;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class MainChatActivity extends AppCompatActivity implements  View.OnKeyListener {

    final String MESSAGES_ENDPOINT = "http://pusher-chat-demo.herokuapp.com";

    MessageAdapter messageAdapter;
    EditText messageInput;
    Button sendButton;
    String username;
    User mUserProfile;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_chat);
        mToolbar= (Toolbar) findViewById(R.id.app_bar);
        mToolbar.setTitle("Chat");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mUserProfile=User.getInstance();
        username =mUserProfile.getmName();
//        Toast.makeText(this, "Welcome, " + username + "!", Toast.LENGTH_LONG).show();

        messageInput = (EditText) findViewById(R.id.et_message_input_main_chat_activity);
//        messageInput.setOnKeyListener(this);


        sendButton = (Button) findViewById(R.id.btn_send_button_main_chat_activity);
        if(messageInput.getText().equals(""))
        {

            sendButton.setEnabled(false);
            sendButton.setClickable(false);
        }
        else
        {
            sendButton.setEnabled(true);
            sendButton.setClickable(true);
            sendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("TAG","btn clicked");
                    postMessage();
                }
            });
        }

        messageAdapter = new MessageAdapter(this, new ArrayList<Message>());
        final ListView messagesView = (ListView) findViewById(R.id.lv_messages_view_main_chat_activity);
        messagesView.setAdapter(messageAdapter);

        Pusher pusher = new Pusher("faa685e4bb3003eb825c");

        pusher.connect();

        Channel channel = pusher.subscribe("messages");

        channel.bind("new_message", new SubscriptionEventListener() {
            @Override
            public void onEvent(String channelName, String eventName, final String data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Gson gson = new Gson();
                        Message message = gson.fromJson(data, Message.class);
                        messageAdapter.add(message);
                        messagesView.setSelection(messageAdapter.getCount() - 1);
                    }

                });
            }

        });
    }



    private void postMessage() {
        String text = messageInput.getText().toString();

        if (text.equals("")) {
            return;
        }

        RequestParams params = new RequestParams();

        params.put("text", text);
        params.put("name", username);
        params.put("time", new Date().getTime());

        AsyncHttpClient client = new AsyncHttpClient();

        client.post(MESSAGES_ENDPOINT + "/messages", params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messageInput.setText("");
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getApplicationContext(), "Something went wrong ", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP) {
            Log.i("TAG","onkey triggered");
            postMessage();
        }
        return true;
    }

}
