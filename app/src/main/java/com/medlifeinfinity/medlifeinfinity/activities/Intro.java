package com.medlifeinfinity.medlifeinfinity.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.fragments.SampleSlide;

/**
 * Created by Vishal Chugh on 028 , 28/Apr/2016.
 */
public class Intro extends AppIntro {
    @Override
    public void init(@Nullable Bundle savedInstanceState) {

        addSlide(new SampleSlide().newInstance(R.layout.fragment_slide1));
        addSlide(new SampleSlide().newInstance(R.layout.fragment_slide2));
        addSlide(new SampleSlide().newInstance(R.layout.fragment_slide3));
        addSlide(new SampleSlide().newInstance(R.layout.fragment_slide4));

        if (Build.VERSION.SDK_INT >= 21) {

            // Set the status bar to dark-semi-transparentish
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
//        addSlide(AppIntroFragment.newInstance("First", "Welcome to MedLife Infinity", R.drawable.intro_pic1, Color.parseColor("#ffeb3b")));
//        addSlide(AppIntroFragment.newInstance("Second", "VIshal Chugh", R.drawable.intro_pic2, Color.parseColor("#b2ff59")));
//
        setFadeAnimation();

        // Hide Skip/Done button.
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permisssion in Manifest.
        showStatusBar(false);

        setVibrate(true);
        setVibrateIntensity(30);

    }

    public void getStarted(View v)
    {
        Intent i = new Intent(Intro.this, LoginAndRegisterActivity.class);
        startActivity(i);
        finish();
    }
    @Override
    public void onSkipPressed() {
        Intent i = new Intent(Intro.this, LoginAndRegisterActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    public void onNextPressed() {

    }

    @Override
    public void onDonePressed() {
        Intent i = new Intent(Intro.this, LoginAndRegisterActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSlideChanged() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
