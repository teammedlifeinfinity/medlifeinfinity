package com.medlifeinfinity.medlifeinfinity.activities.appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.activities.findDoctors.DoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.profile.UserProfileActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.AppointmentViewerAdapter;
import com.medlifeinfinity.medlifeinfinity.adapters.BookedAppointmentAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BookedAppointmentActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String TAG = BookedAppointmentActivity.class.getSimpleName();
    private Toolbar mToolBar;
    private ListView lv_slotPreview;
    private LinearLayout mRootLayout;
    private Snackbar mSnkBar;
    private ProgressDialog progressDialog;
    private BookedAppointmentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_appointment);
        setup();
        getBookedSlots();
    }

    private void getBookedSlots() {
        if (Utility.isNetworkAvailable(this)) {
            String url = ApiCall.HOST_URL + "SelectFromAppointmentByPatient/" + User.getInstance().getmUserId();
            createProgressDialog(this, "getting Booked Time Slot....", false);
            JsonArrayRequest objectRequest = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.i(TAG, response.toString());
                    parseResponse(response);
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSnkBar.dismiss();
                            getBookedSlots();

                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });

            VolleySingleton.getInstance(this).getRequestQueue().add(objectRequest);


        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    getBookedSlots();
                }
            });
            mSnkBar.show();
        }
    }

    private void parseResponse(JSONArray response) {
        mAdapter = new BookedAppointmentAdapter(getApplicationContext(), response);
        lv_slotPreview.setAdapter(mAdapter);
        if (response.length() == 0) {
            mSnkBar = Snackbar.make(mRootLayout, "No data found", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("dismiss", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private void setup() {
        mRootLayout = (LinearLayout) findViewById(R.id.root_booked_appointment_activity);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("View Booked Appointments");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        lv_slotPreview = (ListView) findViewById(R.id.lv_slots_preview_booked_appointment_activity);
        lv_slotPreview.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        long viewId = view.getId();
        if (viewId == R.id.ib_overflow_menu_booked_appointment_row_item) {
            Log.i(TAG, " over flow menu Position " + position);
            ImageButton ib_overflow = (ImageButton) view;
            PopupMenu popupMenu = new PopupMenu(getApplicationContext(), ib_overflow);
            popupMenu.inflate(R.menu.menu_booked_appointment_item_overflow);
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.menu_unbook) {

                        if(mAdapter.getmAppointmentList().get(position).getmAppointmentStatus()) {
                            unbookeSlot(mAdapter.getmAppointmentList().get(position).getmSlotId());
                        }
                        else {
                            Toast.makeText(BookedAppointmentActivity.this,"This Slot is Free",Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (item.getItemId() == R.id.menu_view_doctor) {
                        if(mAdapter.getmAppointmentList().get(position).getmAppointmentStatus()) {
                            Log.i(TAG, "view patient");
                            Intent intent = new Intent(BookedAppointmentActivity.this, UserProfileActivity.class);
                            intent.putExtra("class",DashboardActivity.class.getSimpleName());
                            intent.putExtra("userId", mAdapter.getmAppointmentList().get(position).getmBookedBy());
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(BookedAppointmentActivity.this,"This Slot is Free",Toast.LENGTH_SHORT).show();
                        }
                    }
                    return true;
                }
            });
            popupMenu.show();
        }
    }

    private void unbookeSlot(final String slotId) {
        JSONObject object = new JSONObject();
        try {
            object.put("Slot_Id", slotId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG, object.toString());

        String url = ApiCall.HOST_URL + "CancelAppointment";
        createProgressDialog(this, "canceling Time Slot....", false);
        if (Utility.isNetworkAvailable(this)) {
            final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    String result = "";
                    Log.i(TAG, response.toString());
                    try {
                        result = response.getString("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (result.equals("Updated")) {
                        Toast.makeText(BookedAppointmentActivity.this, "Appointment canceled", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                        getBookedSlots();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something Went Wrong", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            unbookeSlot(slotId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    unbookeSlot(slotId);
                }
            });
            mSnkBar.show();
        }

    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
