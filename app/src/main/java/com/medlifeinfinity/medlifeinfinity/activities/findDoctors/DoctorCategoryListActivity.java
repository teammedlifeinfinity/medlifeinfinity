package com.medlifeinfinity.medlifeinfinity.activities.findDoctors;

/**
 * Created by Vaio on 4/23/2016.
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.medlifeinfinity.medlifeinfinity.R;

public class DoctorCategoryListActivity extends AppCompatActivity {

    private static final String TAG =DoctorCategoryListActivity.class.getSimpleName() ;
    String[] mDoctorSpecialzationList=null;
    ListView lv_doctorCategoryList;
    TextView tv_cityName;
    Toolbar mToolbar;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_category_list);
        setup();
        mDoctorSpecialzationList=getResources().getStringArray(R.array.doctor_specialization);

    }

    private void setup() {
        mToolbar= (Toolbar) findViewById(R.id.app_bar);
        mToolbar.setTitle("Find Doctor");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lv_doctorCategoryList= (ListView) findViewById(R.id.lv_doctor_category_doctor_category_list_activity);
        tv_cityName = (TextView) findViewById(R.id.actv_city_name_doctor_category_list_activity);
        lv_doctorCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(!(tv_cityName.getText().toString().equals(""))) {
                    Intent intent = new Intent(DoctorCategoryListActivity.this, DoctorListActivity.class);
                    intent.putExtra("doctorSpecialization", mDoctorSpecialzationList[position]);
                    intent.putExtra("cityName", tv_cityName.getText().toString());
                    startActivity(intent);
                }
                else {
                    Toast.makeText(DoctorCategoryListActivity.this,"Please provide city name",Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_cityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(DoctorCategoryListActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                tv_cityName.setText(place.getName());
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

}
