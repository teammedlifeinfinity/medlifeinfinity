package com.medlifeinfinity.medlifeinfinity.activities.nearBySearch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.AlertDialogListOfAddressInNearBy;
import com.medlifeinfinity.medlifeinfinity.fragments.nearBySearch.PopUpDialogForChangeLocationInNearBy;

import java.util.ArrayList;

public class MainActivityOfFindNearByDoctors extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public GoogleApiClient mGoogleApiClientInNearBy;
    ProgressBar mProgressBarInNearby;
    public static final String TAG = MainActivityOfFindNearByDoctors.class.getSimpleName();
    ArrayList<String> mPlaceLikelihoodArrayInNearBy = new ArrayList<String>(10);
    DialogFragment dialogFragmentShowingListOfAddressInNearBy;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    Button btn_search_MainActivityOfFindNearBy;
    PopUpDialogForChangeLocationInNearBy popUpDialogForChangeLocationInNearBy;


    private ArrayList<String> currentPlaceLatLng = new ArrayList<>();

    ArrayList<String> placeLikelihoodArrayOfLat = new ArrayList<>();
    ArrayList<String> placeLikelihoodArrayOfLng = new ArrayList<>();


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClientInNearBy.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_of_find_near_by_doctors);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Doctor NearBy");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getResourceReferences();
        mGoogleApiClientInNearBy = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();


    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, connectionResult.toString());
        Toast.makeText(MainActivityOfFindNearByDoctors.this, " Connection Failed " + connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    //following function retrieves the list of places where the device is most likely to be located,
    // and logs the name and likelihood for each place
    public void findCurrentPlaces(View view) {
        if (popUpDialogForChangeLocationInNearBy != null) {
            popUpDialogForChangeLocationInNearBy.dismiss();
        }
        mProgressBarInNearby.setVisibility(View.VISIBLE);
        mProgressBarInNearby.bringToFront();
        showingCurrentPlaces();
    }

    public void showingCurrentPlaces() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                .getCurrentPlace(mGoogleApiClientInNearBy, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            int i = 0;

            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG, String.format("Place '%s' has likelihood: %g",
                            placeLikelihood.getPlace().getName(),
                            placeLikelihood.getLikelihood()));

                    placeLikelihoodArrayOfLat.add(String.valueOf(placeLikelihood.getPlace().getLatLng().latitude));
                    placeLikelihoodArrayOfLng.add(String.valueOf(placeLikelihood.getPlace().getLatLng().longitude));


                    mPlaceLikelihoodArrayInNearBy.add(String.valueOf(placeLikelihood.getPlace().getName()));
                }
                if (!mPlaceLikelihoodArrayInNearBy.isEmpty()) {
                    Log.i(TAG, "mPlaceLikelihoodArrayInNearBy" + mPlaceLikelihoodArrayInNearBy);

                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("KeyForPlaces", mPlaceLikelihoodArrayInNearBy);
                    bundle.putStringArrayList("KeyForLat", placeLikelihoodArrayOfLat);
                    bundle.putStringArrayList("KeyForLng", placeLikelihoodArrayOfLng);


                    dialogFragmentShowingListOfAddressInNearBy = new AlertDialogListOfAddressInNearBy();
                    dialogFragmentShowingListOfAddressInNearBy.setArguments(bundle);
                    mProgressBarInNearby.setVisibility(View.GONE);
                    dialogFragmentShowingListOfAddressInNearBy.show(getSupportFragmentManager(), "DialogFragmentShowingListOfAddress");


                    //mPlaceLikelihoodArrayInNearBy.clear();
                    likelyPlaces.release();
                } else {
                    mProgressBarInNearby.setVisibility(View.GONE);
                    Toast.makeText(MainActivityOfFindNearByDoctors.this, "Cannot fetch your current location", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void getResourceReferences() {
        // autocompleteFragment=(PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        mProgressBarInNearby = (ProgressBar) findViewById(R.id.progressBar_find_near_by_doctors);
        btn_search_MainActivityOfFindNearBy = (Button) findViewById(R.id.btn_serach_find_nearby_doctors);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mProgressBarInNearby.setVisibility(View.GONE);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Toast.makeText(MainActivityOfFindNearByDoctors.this, " You selected " + place.getName(), Toast.LENGTH_SHORT).show();

                Bundle bundle = new Bundle();
                currentPlaceLatLng.clear();
                currentPlaceLatLng.add(String.valueOf(place.getLatLng().latitude));
                currentPlaceLatLng.add(String.valueOf(place.getLatLng().longitude));
                bundle.putStringArrayList("mCurrentPlaceLatLngTabTestInNearBy", currentPlaceLatLng);
                bundle.putString("currentPlace", String.valueOf(place.getName()));
                Intent intent = new Intent(MainActivityOfFindNearByDoctors.this, TabTestInNearByActivity.class);
                intent.putExtra("bundle", bundle);
                startActivity(intent);


                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                mProgressBarInNearby.setVisibility(View.GONE);
                Toast.makeText(MainActivityOfFindNearByDoctors.this, " Error : " + status.getStatusMessage(), Toast.LENGTH_SHORT).show();
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
                mProgressBarInNearby.setVisibility(View.GONE);
                Toast.makeText(MainActivityOfFindNearByDoctors.this, "User canceled the operation", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void startPlaceAutoCompleteIntent(View view) {
        mProgressBarInNearby.setVisibility(View.VISIBLE);
        mProgressBarInNearby.bringToFront();
        if (popUpDialogForChangeLocationInNearBy != null) {
            popUpDialogForChangeLocationInNearBy.dismiss();
        }

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

}
