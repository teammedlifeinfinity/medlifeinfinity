package com.medlifeinfinity.medlifeinfinity.activities.feedback;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GiveFeedbackToDrActivity extends AppCompatActivity {

    public static final String TAG = GiveFeedbackToDrActivity.class.getSimpleName();
    Button btn_submit;
    RatingBar rb_ratingBar;
    EditText et_Title, et_Comment;
    Boolean mFlag = false;
    float mRatings;
    String mTitle, mComment;
    int mFeedbackBY;
    String mDoctorId;
    private ProgressDialog progressDialog;
    private Toolbar mToolBar;
    User mUserProfile;
    private String mPatientId;
    private int flag;
    private Snackbar mSnkBar;
    private LinearLayout mRootLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_give_feedback_to_dr);
        mUserProfile = User.getInstance();
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        flag = getIntent().getFlags();
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Feedback");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setup();
    }


    private void setup() {
        mRootLayout= (LinearLayout) findViewById(R.id.root_give_feedback_to_dr_activity);
        et_Title = (EditText) findViewById(R.id.et_title_give_feedback_to_dr_activity);
        et_Comment = (EditText) findViewById(R.id.et_comment_give_feedback_to_dr_activity);
        rb_ratingBar = (RatingBar) findViewById(R.id.rb_rating_give_feedback_to_dr_activity);
        rb_ratingBar.setNumStars(5);
        rb_ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mRatings = Float.parseFloat(String.valueOf(ratingBar.getRating()));
                mFlag = true;
            }
        });

        btn_submit = (Button) findViewById(R.id.btn_submit_give_feedback_to_dr_activity);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTitle = et_Title.getText().toString();
                mComment = et_Comment.getText().toString();

                //      Validation
                if (mTitle.length() == 0) {
                    et_Title.requestFocus();

                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("TITLE CANNOT BE EMPTY");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            et_Title.setText("");
                            et_Title.requestFocus();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (!mTitle.matches("[a-zA-Z ]+")) {
                    et_Title.requestFocus();

                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("ENTER ONLY ALPHABETICAL CHARACTER");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button

                            et_Title.requestFocus();

                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mTitle.length() < 4) {
                    et_Title.requestFocus();

                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("TITLE CANNOT BE LESS THAN 4 CHARACTER");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            et_Title.requestFocus();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mFlag == false) {
                    rb_ratingBar.requestFocus();
                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("PLEASE RATE THE DOCTOR");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mComment.length() == 0) {
                    et_Comment.requestFocus();

                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("PLEASE ENTER SOME COMMENTS");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked OK button
                            et_Comment.setText("");
                            et_Comment.requestFocus();
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else if (mComment.length() <= 14) {
                    et_Comment.requestFocus();

                    AlertDialog.Builder builder = new AlertDialog.Builder(GiveFeedbackToDrActivity.this);
                    builder.setMessage("COMMENT SHOULD BE AT LEAST 15 CHARACTER");
                    builder.setTitle("Error Occur");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            et_Comment.requestFocus();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if (Utility.isNetworkAvailable(GiveFeedbackToDrActivity.this)) {
                        createProgressDialog(GiveFeedbackToDrActivity.this, "Loading...", false);

                        JSONObject requestJSONObject = createRequestJSON(Integer.parseInt(mDoctorId), mTitle, mRatings, mComment, Integer.parseInt(mPatientId));
                        String url = ApiCall.HOST_URL + "InsertPatientFeedback";
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestJSONObject,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.e(TAG, "response " + response);
                                        Toast.makeText(getApplicationContext(), "Successfully Feedback Submitted", Toast.LENGTH_LONG).show();
                                        hideProgressDialog();
                                        cancel();
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Log.e(TAG, "Error here " + error);
                                        hideProgressDialog();
                                    }
                                });
                        VolleySingleton.getInstance(GiveFeedbackToDrActivity.this).getRequestQueue().add(request);
//                    Toast.makeText(getApplicationContext(), mRatings + "\n" + mDoctorId + "\n" + mTitle + "\n" + mComment, Toast.LENGTH_LONG).show();
                    }
                    else {
                        mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
                        mSnkBar.setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mSnkBar.dismiss();
                            }
                        });
                        mSnkBar.show();
                    }
                }
            }
        });
    }

    private void cancel() {
        et_Title.setText("");
        et_Comment.setText("");
        rb_ratingBar.setRating(0F);
    }

    private JSONObject createRequestJSON(int vDoctorId, String vTitle, float vRating, String vComment, int vFeedBackBy) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userdoc_id", vDoctorId);
            jsonObject.put("Behaviour", vTitle);
            jsonObject.put("Rating", vRating);
            jsonObject.put("Comment", vComment);
            jsonObject.put("Entry_by", vFeedBackBy);
        } catch (JSONException exp) {
            Log.e(TAG, " Error : " + exp);
        }
        return jsonObject;
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
