package com.medlifeinfinity.medlifeinfinity.activities.history.viewHistory;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;


import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.activities.history.uploadHistory.HistoryUploadActivity;
import com.medlifeinfinity.medlifeinfinity.customClasses.GetHistory;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

public class ViewHistoryActivity extends AppCompatActivity {

    //---Arraylist of type GetHistory
    ArrayList<GetHistory> arrayOfWebApiData = new ArrayList<GetHistory>();
    //adapter
    CustomAdapterForGridView adapter = null;

    //-----api urls-----//;

    private ProgressDialog mProgressDialog;
    private static final String TAG = ViewHistoryActivity.class.getSimpleName();

    //----arrays to store json objects---//
    private String[] mAllDiseasesNames;
    private String[] mAllDoctorsNames;
    //---auto complete text views for taking input from user
    public AutoCompleteTextView actv_mInputDisease;
    public AutoCompleteTextView actv_mInputDoctorName;
    private Toolbar mToolBar;
    private FloatingActionButton fab_addHistory;
    private String mLauncherClass;
    String mJSON_URL_DEFAULT;
    private String mJSON_URL_TYPE;
    private String mJSON_URL_DISEASE;
    private String mJSON_URL_DOCTOR_NAME;
    private String mJSON_URL_GET_ALL_DOCTOR_NAMES;
    private String mJSON_URL_GET_ALL_DISEASE_NAMES;
    private GridView mGridView;
    private String mUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history);
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("View History");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(mToolBar);

        mLauncherClass=getIntent().getStringExtra("class");
        mUserId=getIntent().getStringExtra("userId");
        Log.i(TAG, mLauncherClass + "");

        setup();

    }

    private void setup() {

        mGridView = (GridView) findViewById(R.id.grid_view_history_activity);
        fab_addHistory = (FloatingActionButton) findViewById(R.id.fab_add_History_view_history_activity);
        fab_addHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewHistoryActivity.this, HistoryUploadActivity.class);
                startActivity(intent);
            }
        });



        mJSON_URL_DEFAULT = "http://mani11-001-site1.ctempurl.com/api/WebApi/SelectFromUploadHistory/" + mUserId+ "/";
        mJSON_URL_TYPE = "http://mani11-001-site1.ctempurl.com/api/WebApi/SelectFromUploadHistoryByType/" + mUserId+ "/";
        mJSON_URL_DISEASE = "http://mani11-001-site1.ctempurl.com/api/WebApi/SelectFromUploadHistoryBySymptom/" + mUserId+ "/";
        mJSON_URL_DOCTOR_NAME = "http://mani11-001-site1.ctempurl.com/api/WebApi/SelectFromUploadHistoryByDocName/" + mUserId+ "/";
        mJSON_URL_GET_ALL_DOCTOR_NAMES = "http://mani11-001-site1.ctempurl.com/api/WebApi/GetDoctorFromUploadHistory";
        mJSON_URL_GET_ALL_DISEASE_NAMES = "http://mani11-001-site1.ctempurl.com/api/WebApi/GetSymtomsFromUploadHistory";


        sendRequestForDefaultUrls(mJSON_URL_DEFAULT);

        if(mLauncherClass.equals(MiniDashboradActivity.class.getSimpleName()))
        {
            fab_addHistory.setVisibility(View.GONE);
        }
    }
    

    private void sendRequestForDefaultUrls(final String dataUrl) {

        mProgressDialog = new ProgressDialog(ViewHistoryActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, dataUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "Response " + response);
                //   Toast.makeText(ViewHistoryActivity.this, "Response Received Successfully", Toast.LENGTH_SHORT).show();
                showDefaultJSON(response);
                hidePDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //         Toast.makeText(ViewHistoryActivity.this, "Please Connect to Internet to continue ", Toast.LENGTH_LONG).show();
                        hidePDialog();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        stringRequest.setShouldCache(true);
    }

    private void showDefaultJSON(String json) {
        try {
            JSONArray users = new JSONArray(json);
            for (int i = 0; i < users.length(); i++) {
                JSONObject json_data = users.getJSONObject(i);
                GetHistory mResultRow = new GetHistory();
                mResultRow.mUserId = json_data.getString("user_id");
                mResultRow.mDocName = json_data.getString("Name");
                mResultRow.mEntryDate = json_data.getString("Entry_Date");
                mResultRow.mDisease = json_data.getString("Symtoms");
                mResultRow.mType = json_data.getString("Type");
                mResultRow.mImageName = json_data.getString("Image_Name");
                mResultRow.mImageUrl = json_data.getString("Image_URL");
                arrayOfWebApiData.add(mResultRow);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setGridData();
    }

    private void setGridData() {
        mGridView.setAdapter(null);
        adapter = new CustomAdapterForGridView();
        mGridView.setAdapter(adapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), OpenImageActivity.class);
                intent.putParcelableArrayListExtra("img", arrayOfWebApiData);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

    private void hidePDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    ////----------------------------------------------------------------------------------------------------
    private void sendRequestForDiseaseUrl(final String dataUrl) {

        mProgressDialog = new ProgressDialog(ViewHistoryActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, dataUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "Response " + response);
                //Toast.makeText(ViewHistoryActivity.this, "Response Received Successfully", Toast.LENGTH_SHORT).show();

                showDiseaseJSON(response);
                hidePDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getActivity(), "Response Received With Error" + error.getMessage(), Toast.LENGTH_LONG).show();
//                        Toast.makeText(ViewHistoryActivity.this, "Please Connect to Internet to continue ", Toast.LENGTH_LONG).show();
                        hidePDialog();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        stringRequest.setShouldCache(true);
    }

    //------------------------------------------------------------
    private void showDiseaseJSON(String json) {

        try {
            JSONArray users = new JSONArray(json);
            mAllDiseasesNames = new String[users.length()];
            for (int i = 0; i < users.length(); i++) {
                JSONObject json_data = users.getJSONObject(i);
                mAllDiseasesNames[i] = json_data.getString("Symtoms");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        actv_mInputDisease = (AutoCompleteTextView) findViewById(R.id.actv_input_view_history_activity);
        actv_mInputDisease.setVisibility(View.VISIBLE);
        actv_mInputDisease.setHint("Enter Disease Name");
        ArrayAdapter<String> mDiseaseAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAllDiseasesNames);
        actv_mInputDisease.setAdapter(mDiseaseAdapter);
        actv_mInputDisease.setThreshold(0);
        actv_mInputDisease.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                actv_mInputDisease.showDropDown();
                return false;
            }
        });
    }

    ////-------------------------------------------------------------
    private void sendRequestForDoctorNameUrl(final String dataUrl) {

        mProgressDialog = new ProgressDialog(ViewHistoryActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, dataUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e(TAG, "Response " + response);
                // Toast.makeText(ViewHistoryActivity.this, "Response Received Successfully", Toast.LENGTH_SHORT).show();
                showDoctorNamesJSON(response);
                hidePDialog();
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(getActivity(), "Response Received With Error" + error.getMessage(), Toast.LENGTH_LONG).show();
                        //   Toast.makeText(ViewHistoryActivity.this, "Please Connect to Internet to continue ", Toast.LENGTH_LONG).show();
                        hidePDialog();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
        stringRequest.setShouldCache(true);
    }

    ///---------------------------------------------------------------------------
    private void showDoctorNamesJSON(String json) {
        try {
            JSONArray users = new JSONArray(json);
            mAllDoctorsNames = new String[users.length()];
            for (int i = 0; i < users.length(); i++) {
                JSONObject json_data = users.getJSONObject(i);
                mAllDoctorsNames[i] = json_data.getString("Name");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        actv_mInputDoctorName = (AutoCompleteTextView) findViewById(R.id.actv_input_view_history_activity);
        actv_mInputDoctorName.setVisibility(View.VISIBLE);
        actv_mInputDoctorName.setHint("Enter Doctor Name");
        ArrayAdapter<String> mDoctorNameAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mAllDoctorsNames);
        actv_mInputDoctorName.setAdapter(mDoctorNameAdapter);
        actv_mInputDoctorName.setThreshold(1);
        actv_mInputDoctorName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //----shows all the doctor names in the drop down
                actv_mInputDoctorName.showDropDown();
                return false;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_view_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_default: {
                sendRequestForDefaultUrls(mJSON_URL_DEFAULT);
                adapter.clear();
            }
            break;
            case R.id.menu_disease:
                onDiseaseClicked();
                break;
            case R.id.menu_type: {
                onOptionTypeClicked();
                break;
            }
            case R.id.menu_doctor:
                onDoctorNameClicked();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    ///----When DoctorName option is Selected from options Menu
    private void onDoctorNameClicked() {
        sendRequestForDoctorNameUrl(mJSON_URL_GET_ALL_DOCTOR_NAMES);
        final Button mOkButton = (Button) findViewById(R.id.btn_ok_input_view_history_activity);
        mOkButton.setVisibility(View.VISIBLE);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!actv_mInputDoctorName.getText().toString().isEmpty()) {
                    String inputData = mJSON_URL_DOCTOR_NAME + actv_mInputDoctorName.getText().toString();
                    try {
                        URL url = new URL(inputData);
                        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                        url = uri.toURL();
                        sendRequestForDefaultUrls(String.valueOf(url));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    adapter.clear();
                    actv_mInputDoctorName.setText("");
                    mOkButton.setVisibility(View.GONE);

                    actv_mInputDoctorName.setVisibility(View.GONE);
                } else {
                    final AlertDialog.Builder showError = new AlertDialog.Builder(ViewHistoryActivity.this);
                    showError.setTitle("Error");
                    showError.setCancelable(true);
                    showError.setMessage("Please fill the doctor name before submitting");
                    showError.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = showError.create();
                    dialog.show();
                }
            }
        });

    }

    //------Method Called When Disease Option is selected from options Menu
    private void onDiseaseClicked() {
        //---- generate Request Queue
        sendRequestForDiseaseUrl(mJSON_URL_GET_ALL_DISEASE_NAMES);
        final Button mOkButton = (Button) findViewById(R.id.btn_ok_input_view_history_activity);
        mOkButton.setVisibility(View.VISIBLE);
        mOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!actv_mInputDisease.getText().toString().isEmpty()) {
                    String inputData = mJSON_URL_DISEASE + actv_mInputDisease.getText().toString();
                    try {
                        URL url = new URL(inputData);
                        URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                        url = uri.toURL();
                        sendRequestForDefaultUrls(String.valueOf(url));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    adapter.clear();
                    actv_mInputDisease.setText("");
                    mOkButton.setVisibility(View.GONE);
                    actv_mInputDisease.setVisibility(View.GONE);
                } else {
                    AlertDialog.Builder showError = new AlertDialog.Builder(ViewHistoryActivity.this);
                    showError.setTitle("Error");
                    showError.setCancelable(true);
                    showError.setMessage("Please fill the disease name before submitting");
                    AlertDialog dialog = showError.create();
                    dialog.show();
                }
            }

        });
    }

    //-----Method called when Type Option is selected from Menu-----//
    private void onOptionTypeClicked() {
        final CharSequence typesList[] = {"Prescription", "X-Ray", "OtherTypes"};
        final AlertDialog.Builder dialogBox = new AlertDialog.Builder(ViewHistoryActivity.this);
        dialogBox.setTitle("Select Type");
        dialogBox.setCancelable(false);
        dialogBox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogBox.setSingleChoiceItems(typesList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int typeSelected) {

                if (typesList[typeSelected] == "Prescription") {
                    //------will display all records of Type Prescription
                    dialog.dismiss();
                    sendRequestForDefaultUrls(mJSON_URL_TYPE + typesList[typeSelected]);
                    adapter.clear();
                } else if (typesList[typeSelected] == "X-Ray") {
                    //------will display all records of Type X-Ray
                    dialog.dismiss();
                    sendRequestForDefaultUrls(mJSON_URL_TYPE + typesList[typeSelected]);
                    adapter.clear();
                } else if (typesList[typeSelected] == "OtherTypes") {
                    //------will display all records of Type Others
                    dialog.dismiss();
                    sendRequestForDefaultUrls(mJSON_URL_TYPE + typesList[typeSelected]);
                    adapter.clear();
                }

            }
        });

        AlertDialog dialog = dialogBox.create();
        dialog.show();
    }

    //----------------------//
//------Custom Adapter for grid view-----//
    class CustomAdapterForGridView extends ArrayAdapter<GetHistory> {


        CustomAdapterForGridView() {
            super(ViewHistoryActivity.this, R.layout.custom_layout_grid_items, arrayOfWebApiData);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                LayoutInflater layoutInflater = getLayoutInflater();
                convertView = layoutInflater.inflate(R.layout.custom_layout_grid_items, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.populateFrom(arrayOfWebApiData.get(position));
            return convertView;
        }
    }

    //----View Holder Classs for all the views used-----///
    class ViewHolder {
        ImageLoader mImageLoader;
        TextView tv_docName = null;
        TextView tv_entryDate = null;
        TextView tv_disease = null;
        TextView tv_type = null;
        NetworkImageView niv_gridViewImages;

        ViewHolder(View view) {
            tv_docName = (TextView) view.findViewById(R.id.tv_doc_name_custom_layout_grid_items);
            tv_entryDate = (TextView) view.findViewById(R.id.tv_entry_date_custom_layout_grid_items);
            tv_disease = (TextView) view.findViewById(R.id.tv_disease_custom_layout_grid_items);
            tv_type = (TextView) view.findViewById(R.id.tv_type_custom_layout_grid_items);
            niv_gridViewImages = (NetworkImageView) view.findViewById(R.id.niv_images_custom_layout_grid_items);
        }

        public void populateFrom(GetHistory r) {
            tv_docName.setText(r.mDocName);
            tv_entryDate.setText((r.mEntryDate).substring(0, 10));
            tv_disease.setText(r.mDisease);
            tv_type.setText(r.mType);
            mImageLoader = VolleySingleton.getInstance(getApplicationContext()).getImageLoader();
            niv_gridViewImages.setImageUrl("http://mani11-001-site1.ctempurl.com/images/" + r.mImageUrl, mImageLoader);
        }
    }


}
