package com.medlifeinfinity.medlifeinfinity.activities.rms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.activities.prescription.AddPrescriptionActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.RMSDoctorDisplayActivityAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RMSDoctorDisplayActivity extends AppCompatActivity {


    public static final String TAG = RMSDoctorDisplayActivity.class.getSimpleName();
    public static final String KEY_DESCRIPTION = "Description";
    public static final String KEY_MESSAGE_TYPE = "Message_Type";
    public static final String KEY_ID = "Rms_Id";
    public static String[] Id;
    public static String[] Description;
    public static String[] MessageType;
    ListView lv_displayData;
    private ProgressDialog progressDialog;
    private User mUserProfile;
    private Toolbar mToolBar;
    private String mPatientId;
    private String mDoctorId;
    private int flag;
    private FloatingActionButton fab_addRMS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rmsdoctor_display);
        flag = getIntent().getFlags();
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("RMS");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mUserProfile=User.getInstance();
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        setup();
        sendRequest();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        sendRequest();
    }

    private void sendRequest() {
        String userId = mUserProfile.getmUserId();
        String url = ApiCall.HOST_URL + "RMSFeedbackDoctorView/" + mDoctorId+"/"+mPatientId;

        createProgressDialog(this, "Loading...", false);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                parserResponse(response);
                RMSDoctorDisplayActivityAdapter customDoctorViewAdapter = new RMSDoctorDisplayActivityAdapter(getApplicationContext(), Description, MessageType,Id);
                lv_displayData.setAdapter(customDoctorViewAdapter);
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(TAG, error.toString());
                hideProgressDialog();
            }
        });

        VolleySingleton.getInstance(this).getRequestQueue().add(request);

    }

    private void parserResponse(JSONArray response) {
        try {
            Id = new String[response.length()];
            Description = new String[response.length()];
            MessageType = new String[response.length()];
            for (int i = 0; i < response.length(); i++) {
                JSONObject jo = response.getJSONObject(i);
                Id[i] = jo.getString(KEY_ID);
                Description[i] = jo.getString(KEY_DESCRIPTION);
                MessageType[i] = jo.getString(KEY_MESSAGE_TYPE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setup() {
        lv_displayData = (ListView) findViewById(R.id.lv_displayAllRecord_rms_doctor_display_activity);
        lv_displayData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(RMSDoctorDisplayActivity.this, RMSDoctorDisplayDetailsActivity.class);
                intent.putExtra("doctorId", mDoctorId);
                intent.putExtra("patientId", mPatientId);
                intent.putExtra("RMSId",Id[position]);
                intent.putExtra("Description",Description[position]);
                intent.putExtra("MessageType",MessageType[position]);
                intent.setFlags(flag);
                startActivity(intent);
            }
        });
        fab_addRMS = (FloatingActionButton) findViewById(R.id.fab_add_rms_rms_doctor_display_activity);
        if (flag == MiniDashboradActivity.DOCTOR) {
            fab_addRMS.setVisibility(View.INVISIBLE);
        } else if (flag == MiniDashboradActivity.PATIENT) {
            fab_addRMS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), RMSPostPtActivity.class);
                    intent.putExtra("patientId", mPatientId);
                    intent.putExtra("doctorId", mDoctorId);
                    startActivity(intent);
                }
            });
        }
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


}
