package com.medlifeinfinity.medlifeinfinity.activities.nearBySearch;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.medlifeinfinity.medlifeinfinity.R;

public class FindDoctorOrChemistActivity extends AppCompatActivity {

    static int mRollIdInFindDoctorOrChemist;
    public static String  mSelectedUserToSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_doctor_or_chemist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void radioButtonClickedInFindDoctorOrChemist(View view) {
        RadioButton radioButton = (RadioButton) view;
        if (radioButton.isChecked())
        {
            mSelectedUserToSearch= (String) radioButton.getText();
            if (radioButton.getText().equals("Doctors")) {
                mRollIdInFindDoctorOrChemist =1;
                Toast.makeText(FindDoctorOrChemistActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
                Intent intent =new Intent(FindDoctorOrChemistActivity.this,MainActivityOfFindNearByDoctors.class);
                startActivity(intent);
            } else if (radioButton.getText().equals("Chemists")) {
                Toast.makeText(FindDoctorOrChemistActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
                mRollIdInFindDoctorOrChemist =3;
                Intent intent =new Intent(FindDoctorOrChemistActivity.this,MainActivityOfFindNearByDoctors.class);
                startActivity(intent);
            }
        }
    }
}
