package com.medlifeinfinity.medlifeinfinity.activities.rms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RMSDoctorDisplayDetailsActivity extends AppCompatActivity {

    public static final String TAG = RMSDoctorDisplayDetailsActivity.class.getSimpleName();
    ImageView iv_DisplayImage;
    TextView tv_Date, tv_MessageType, tv_Complain;
    EditText et_Reply;
    String reply;
    String mDate, mName, mComplain;
    int entryById;
    User mUserProfile;
    private Toolbar mToolBar;
    private Button btn_submit, btn_cancel;
    private String mPatientId;
    private String mDoctorId;
    private int flag;
    private String mRMSId;
    private String mDescription;
    private String mMessageType;
    private Snackbar mSnkBar;
    private LinearLayout mRootLayout;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rmsdoctor_display_details);

        mUserProfile = User.getdumyInstance(User.DOCTOR_ROLE);
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        mRMSId = getIntent().getStringExtra("RMSId");
        mDescription = getIntent().getStringExtra("Description");
        mMessageType = getIntent().getStringExtra("MessageType");
        flag = getIntent().getFlags();
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Reply RMS");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getAllReference();
        getDataInLarge();

    }


    public void getAllReference() {
        mRootLayout = (LinearLayout) findViewById(R.id.root_rmsdoctor_disply_details_activity);
        tv_MessageType = (TextView) findViewById(R.id.tv_message_type_rmsdoctor_display_details_activity);
        tv_Complain = (TextView) findViewById(R.id.tv_complain_rmsdoctor_display_details_activity);
        iv_DisplayImage = (ImageView) findViewById(R.id.iv_first_letter_rmsdoctor_display_details_activity);
        et_Reply = (EditText) findViewById(R.id.et_reply_rmsdoctor_display_details_activity);
        btn_submit = (Button) findViewById(R.id.btn_submit_rmsdoctor_display_details_activity);
        btn_cancel = (Button) findViewById(R.id.btn_cancel_rmsdoctor_display_details_activity);
        if (flag == MiniDashboradActivity.PATIENT) {
            btn_submit.setVisibility(View.GONE);
            btn_cancel.setVisibility(View.GONE);
            et_Reply.setFocusable(false);
            getRMSReply(mRMSId);
        }
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReply();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    private void getRMSReply(final String mRMSId) {
        Log.i(TAG,"RMS ID "+mRMSId);
        String url = ApiCall.HOST_URL + "GetFromRMSReply/" + mRMSId;
        JsonArrayRequest request;
        createProgressDialog(this, "Loading...", false);
        if ((Utility.isNetworkAvailable(this))) {
            request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.i(TAG, response + "");
                    if(response.length()==0)
                    {
                        et_Reply.setText("No Reply");
                    }
                    try {
                        parseResponse(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG, error + "");
                    mSnkBar = Snackbar.make(mRootLayout, "You are offline", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getRMSReply(mRMSId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);
        } else {
            mSnkBar = Snackbar.make(mRootLayout, "You are offline", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getRMSReply(mRMSId);
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private void parseResponse(JSONArray response) throws JSONException {
        JSONObject object=response.getJSONObject(0);
        String reply=object.getString("Reply_Message");
        et_Reply.setText(reply);
    }

    public void getDataInLarge() {
        try {
            tv_MessageType.setText(mMessageType);
            tv_Complain.setText(mDescription);
            String letter = String.valueOf(tv_MessageType.getText().charAt(0));
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(letter, color);
            iv_DisplayImage.setImageDrawable(drawable);
        } catch (Exception e) {
            Log.d("Problem Here", e.toString());
        }
    }

    public void addReply() {
        reply = et_Reply.getText().toString();
        if (reply.length() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(RMSDoctorDisplayDetailsActivity.this);
            builder.setTitle("Error Find");
            builder.setMessage("Enter Reply");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    et_Reply.requestFocus();

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            JSONObject requestJSONObject = createRequestJSON(Integer.parseInt(mPatientId), Integer.parseInt(mRMSId), reply, Integer.parseInt(mDoctorId));
            RequestQueue queue = Volley.newRequestQueue(this); //Creates a default instance of the worker pool and calls
            String url = ApiCall.HOST_URL + "InsertIntoRMSReply";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestJSONObject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e(TAG, "response " + response);
                            Toast.makeText(getApplicationContext(), "Successfully Reply Send", Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {    //not response the show the error
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Error here " + error);
                            Toast.makeText(RMSDoctorDisplayDetailsActivity.this, "Response Received with Error", Toast.LENGTH_SHORT).show();

                        }
                    });
            queue.add(request);
            cancel();
        }
    }

    private JSONObject createRequestJSON(int v_UserId, int v_RmsId, String v_Reply, int v_EntryById) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", v_UserId);
            jsonObject.put("Rms_Id", v_RmsId);
            jsonObject.put("Reply_Message", v_Reply);
            jsonObject.put("EntryByDoc", v_EntryById);
        } catch (JSONException exp) {
            Log.e(TAG, " Error : " + exp);
        }
        return jsonObject;
    }

    public void cancel() {
        et_Reply.setText("");
        et_Reply.requestFocus();
    }


    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
