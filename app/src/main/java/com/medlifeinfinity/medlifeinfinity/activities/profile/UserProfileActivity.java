package com.medlifeinfinity.medlifeinfinity.activities.profile;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.DashboardActivity;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vishal Chugh on 016 , 16/May/2016.
 */
public class UserProfileActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 103;
    private static final String TAG = UserProfileActivity.class.getSimpleName();
    Drawable toggle_image_up = null, toggle_image_down = null, im = null;
    private CoordinatorLayout mRootLayout;
    private CollapsingToolbarLayout mCollapsingToolbarLayoutUserProfileMainLayout;
    private int mutedColor, lightMutedColor;
    private LinearLayout linearLayoutAddressUserProfileMainLayout;
    private TextView tvAddressToggleSwitchUserProfileMainLayout;
    private EditText etUserNameUserProfileMainLayout, etUserEmailIdUserProfileMainLayout, etUserContactUserProfileMainLayout;
    private EditText etUserAddressLine1UserProfileMainLayout, etUserAddressLine2UserProfileMainLayout, etUserAddressCityUserProfileMainLayout, etUserAddressStateUserProfileMainLayout;
    private EditText etUserDoctorSpecializationUserProfileMainLayout, etUserDoctorQualificationUserProfileMainLayout;
    private EditText etUserBloodGroupUserProfileMainLayout, etUserDobUserProfileMainLayout, etUserGenderUserProfileMainLayout;

    private TextInputLayout tilUserNameUserProfileMainLayout, tilUserEmailIdUserProfileMainLayout, tilUserContactUserProfileMainLayout;
    private TextInputLayout tilUserAddressLine1UserProfileMainLayout, tilUserAddressLine2UserProfileMainLayout, tilUserAddressCityUserProfileMainLayout, tilUserAddressStateUserProfileMainLayout;
    private TextInputLayout tilUserBloodGroupUserProfileMainLayout, tilUserDobUserProfileMainLayout, tilUserGenderUserProfileMainLayout;
    private TextInputLayout tilUserDoctorSpecializationUserProfileMainLayout, tilUserDoctorQualificationUserProfileMainLayout;

    private Toolbar toolbar;
    private ImageView ivUserProfilePictureUserProfileMainLayout;
    private Bitmap bitmap;
    private User mUserInstance;
    private CardView cvUserDoctorDetailsUserProfileMainLayout;
    private String ImageResponse;
    private String userId;
    private Snackbar mSnkBar;
    private ProgressDialog progressDialog;
    private String mLuncherClass;

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        userId=getIntent().getStringExtra("userId");
        mLuncherClass=getIntent().getStringExtra("class");
        toolbar = (Toolbar) findViewById(R.id.appBar);
        toolbar.setTitle("Profile");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(mLuncherClass.equals(DashboardActivity.class.getSimpleName())) {
            setSupportActionBar(toolbar);
        }
//        mCoordinator = (CoordinatorLayout) findViewById(R.id.root_layout);
        mCollapsingToolbarLayoutUserProfileMainLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);
        mCollapsingToolbarLayoutUserProfileMainLayout.setTitle("Profile");

        Log.i(TAG, "Loading "+userId);
        getmUserProfile(userId);
        generateView();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void getmUserProfile(final String userId) {

        Log.i(TAG,"Loading profile");
        if ((Utility.isNetworkAvailable(this))) {
            String url = ApiCall.HOST_URL + "GetUserDetailswithId/" + userId;
            createProgressDialog(this,"Loading Profile",false);
            JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.i(TAG, response.toString());
                    try {
                        mUserInstance=User.getUserObjectFromJsonObject(response.getJSONObject(0));
                        setUpProfile();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    hideProgressDialog();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i(TAG,error.toString());
                    mSnkBar = Snackbar.make(mRootLayout, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
                    mSnkBar.setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getmUserProfile(userId);
                            mSnkBar.dismiss();
                        }
                    });
                    mSnkBar.show();
                    hideProgressDialog();
                }
            });
            VolleySingleton.getInstance(this).getRequestQueue().add(request);
        }
        else {
            mSnkBar = Snackbar.make(mRootLayout, "Not connected To internet", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getmUserProfile(userId);
                    mSnkBar.dismiss();
                }
            });
            mSnkBar.show();
        }
    }

    private void setUpProfile() {
//        Log.i("USER DETAILS ", mUserInstance.toString());
        if (checkEmptyFields(mUserInstance.getmName())) {
            etUserNameUserProfileMainLayout.setText("");
        } else {
            etUserNameUserProfileMainLayout.setText(mUserInstance.getmName());
        }

        if (checkEmptyFields(mUserInstance.getmEmail())) {
            etUserEmailIdUserProfileMainLayout.setText("");
        } else {
            etUserEmailIdUserProfileMainLayout.setText(mUserInstance.getmEmail());
        }
        switch (mUserInstance.getmRole()) {
            case User.DOCTOR_ROLE: //Doctor Profile
            {
                Log.i("ASUS", mUserInstance.getmRole().toString());
                cvUserDoctorDetailsUserProfileMainLayout.setVisibility(View.VISIBLE);
                if (checkEmptyFields(mUserInstance.getmSpecialization())) {
                    etUserDoctorSpecializationUserProfileMainLayout.setText("");
                } else {
                    etUserDoctorSpecializationUserProfileMainLayout.setText(mUserInstance.getmSpecialization());
                }

                if (checkEmptyFields(mUserInstance.getmQualification())) {
                    etUserDoctorQualificationUserProfileMainLayout.setText("");
                } else {
                    etUserDoctorQualificationUserProfileMainLayout.setText(mUserInstance.getmQualification());
                }
                break;
            }
            case User.PATIENT_ROLE: //Patient Profile
            {
                Log.i("ASUS", mUserInstance.getmRole().toString());
                cvUserDoctorDetailsUserProfileMainLayout.setVisibility(View.GONE);
                break;
            }
            case User.CHEMIST_ROLE: //Chemist Profile
            {
                Log.i("ASUS", mUserInstance.getmRole().toString());
                cvUserDoctorDetailsUserProfileMainLayout.setVisibility(View.GONE);
                // TODO: 021 , 21/Apr/2016 Chemist Layout to be added in future and set its visibility to visible here.and gone elsewhere.
                break;
            }
        }


        String ProfilePictureUrl = mUserInstance.getmProfliePhoto();
        Log.i("ProfilePictureUrl", ProfilePictureUrl);
        ImageStringRequestAndSetImage(ProfilePictureUrl);

//        ivUserProfilePictureUserProfileMainLayout.setImageBitmap(bm);

        if (checkEmptyFields(mUserInstance.getmBloodGroup())) {
            etUserBloodGroupUserProfileMainLayout.setText("");
        } else {
            etUserBloodGroupUserProfileMainLayout.setText(mUserInstance.getmBloodGroup());
        }
        if (checkEmptyFields(mUserInstance.getmContactNumber())) {
            etUserContactUserProfileMainLayout.setText("");
        } else {
            etUserContactUserProfileMainLayout.setText(mUserInstance.getmContactNumber());
        }
        if (checkEmptyFields(mUserInstance.getmGender())) {
            etUserGenderUserProfileMainLayout.setText("");
        } else {
            etUserGenderUserProfileMainLayout.setText(mUserInstance.getmGender());
        }
        if (checkEmptyFields(mUserInstance.getmCity())) {
            etUserAddressCityUserProfileMainLayout.setText("");
        } else {
            etUserAddressCityUserProfileMainLayout.setText(mUserInstance.getmCity());
        }
        if (checkEmptyFields(mUserInstance.getmDob())) {
            etUserDobUserProfileMainLayout.setText("");
        } else {
            etUserDobUserProfileMainLayout.setText(mUserInstance.getmDob());
        }
        if (checkEmptyFields(mUserInstance.getmState())) {
            etUserAddressStateUserProfileMainLayout.setText("");
        } else {
            etUserAddressStateUserProfileMainLayout.setText(mUserInstance.getmState());
        }

        String fullAddress = mUserInstance.getmAddress().toString();
        if (checkEmptyFields(fullAddress)) {
            etUserAddressLine1UserProfileMainLayout.setText("");
            etUserAddressLine2UserProfileMainLayout.setText("");

        } else {

            if (fullAddress.contains(",")) {
                String addressLine1 = fullAddress.substring(0, fullAddress.lastIndexOf(","));
                String addressLine2 = fullAddress.substring(fullAddress.lastIndexOf(",") + 1, fullAddress.length());
                etUserAddressLine1UserProfileMainLayout.setText(addressLine1);
                etUserAddressLine2UserProfileMainLayout.setText(addressLine2);
            } else {
                etUserAddressLine1UserProfileMainLayout.setText(fullAddress);
            }
        }


        setProminentColors();
    }

    private Bitmap ImageDecode(String encodedImage) {
//        byte[] imageAsBytes = Base64.decode(encodedImage.getBytes(), Base64.DEFAULT);
//        ImageView image = (ImageView)this.findViewById(R.id.ImageView);

//        Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        return decodedBitmap;
    }

    private void ImageStringRequestAndSetImage(String profilePictureUrl) {
        String imageURL = ApiCall.IMAGE_URL.toString() + profilePictureUrl;

        ImageLoader mImageLoader = VolleySingleton.getInstance(this).getImageLoader();
        mImageLoader.get(imageURL, ImageLoader.getImageListener(ivUserProfilePictureUserProfileMainLayout, R.drawable.ic_loading_animation, R.drawable.ic_no_image_found));

    }


    private boolean checkEmptyFields(String str) {
        if (str.equals("NA") || str.equals("null")) {
            return true;
        } else {
            return false;
        }

    }

    private void setProminentColors() {

        bitmap = drawableToBitmap(ivUserProfilePictureUserProfileMainLayout.getDrawable());

//        Bitmap bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), ivUserProfilePictureUserProfileMainLayout.getId());

        if (bitmap == null || bitmap.getHeight() <= 0 || bitmap.getWidth() <= 0) {
            //do something
            Log.i("TAG", "Bitmap is Null");
        } else {
            Log.i("TAG", bitmap.toString());
            //This resized bitmap is just an example: you should keep your bitmap
            //aspect ratio, and keep width and height < 100
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
            new Palette.Builder(resizedBitmap).generate(new Palette.PaletteAsyncListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                public void onGenerated(Palette p) {
                    // Use generated instance
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        lightMutedColor = p.getLightMutedColor(getResources().getColor(R.color.colorPrimary));
                        mutedColor = p.getMutedColor(getResources().getColor(R.color.colorPrimaryDark));

                        Log.i("TAG", "Color code " + lightMutedColor);

                        getWindow().setStatusBarColor(mutedColor);
                        mCollapsingToolbarLayoutUserProfileMainLayout.setContentScrimColor(lightMutedColor);
                    }
                }
            });
        }
    }


    private void generateView() {
        mRootLayout= (CoordinatorLayout) findViewById(R.id.root_user_profile_activity);
        ivUserProfilePictureUserProfileMainLayout = (ImageView) findViewById(R.id.iv_user_profile_picture_user_profile_main_layout);
        tvAddressToggleSwitchUserProfileMainLayout = (TextView) findViewById(R.id.tv_user_address_label);
        etUserNameUserProfileMainLayout = (EditText) findViewById(R.id.et_user_name_user_profile_main_layout);
        etUserEmailIdUserProfileMainLayout = (EditText) findViewById(R.id.et_user_email_user_profile_main_layout);
        etUserContactUserProfileMainLayout = (EditText) findViewById(R.id.et_user_contact_number_user_profile_main_layout);
        etUserAddressLine1UserProfileMainLayout = (EditText) findViewById(R.id.et_user_address_line1_user_profile_main_layout);
        etUserAddressLine2UserProfileMainLayout = (EditText) findViewById(R.id.et_user_address_line2_user_profile_main_layout);
        etUserAddressCityUserProfileMainLayout = (EditText) findViewById(R.id.et_user_address_city_user_profile_main_layout);
        etUserAddressStateUserProfileMainLayout = (EditText) findViewById(R.id.et_user_address_state_user_profile_main_layout);
        etUserBloodGroupUserProfileMainLayout = (EditText) findViewById(R.id.et_user_blood_group_user_profile_main_layout);
        etUserDobUserProfileMainLayout = (EditText) findViewById(R.id.et_user_dob_user_profile_main_layout);
        etUserGenderUserProfileMainLayout = (EditText) findViewById(R.id.et_user_gender_user_profile_main_layout);
        etUserDoctorSpecializationUserProfileMainLayout = (EditText) findViewById(R.id.et_user_doctor_specialization_user_profile_main_layout);
        etUserDoctorQualificationUserProfileMainLayout = (EditText) findViewById(R.id.et_user_doctor_qualification_user_profile_main_layout);

        tilUserNameUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_name_user_profile_main_layout);
        tilUserEmailIdUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_email_user_profile_main_layout);
        tilUserContactUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_contact_number_user_profile_main_layout);
        tilUserAddressLine1UserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_address_line1_user_profile_main_layout);
        tilUserAddressLine2UserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_address_line2_user_profile_main_layout);
        tilUserAddressCityUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_address_city_user_profile_main_layout);
        tilUserAddressStateUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_address_state_user_profile_main_layout);
        tilUserBloodGroupUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_blood_group_user_profile_main_layout);
        tilUserDobUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_dob_user_profile_main_layout);
        tilUserGenderUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_gender_user_profile_main_layout);
        tilUserDoctorSpecializationUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_doctor_specialization_user_profile_main_layout);
        tilUserDoctorQualificationUserProfileMainLayout = (TextInputLayout) findViewById(R.id.til_user_doctor_qualification_user_profile_main_layout);

        cvUserDoctorDetailsUserProfileMainLayout = (CardView) findViewById(R.id.cv_user_doctor_details_user_profile_main_layout);


        linearLayoutAddressUserProfileMainLayout = (LinearLayout) findViewById(R.id.l_layout_user_address_user_profile_main_layout);
        linearLayoutAddressUserProfileMainLayout.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            toggle_image_up = getDrawable(R.drawable.ic_arrow_up);
            toggle_image_down = getDrawable(R.drawable.ic_arrow_down);
            im = getDrawable(R.drawable.ic_location);
        }

        tvAddressToggleSwitchUserProfileMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutAddressUserProfileMainLayout.setVisibility(linearLayoutAddressUserProfileMainLayout.isShown() ? View.GONE : View.VISIBLE);
                tvAddressToggleSwitchUserProfileMainLayout.setCompoundDrawablesWithIntrinsicBounds(im, null, linearLayoutAddressUserProfileMainLayout.isShown() ? toggle_image_up : toggle_image_down, null);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_profile_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.editProfileMenuOption: {
                startActivityForResult(new Intent(getApplicationContext(), EditProfileActivity.class), REQUEST_CODE);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
