package com.medlifeinfinity.medlifeinfinity.activities.qrcode;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

import java.io.InputStream;
import java.net.URL;

public class GenerateQRCodeActivity extends AppCompatActivity {

    public static final String TAG = GenerateQRCodeActivity.class.getCanonicalName();

    private ImageView img_qrCode;
    private Toolbar mToolBar;
    private User mUserProfile;
    private ProgressDialog progressDialog;
    private Bitmap bitmap;
    private LinearLayout rootLayout;
    private Snackbar snkBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qrcode);

        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("QR Code");
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUserProfile = User.getInstance();
        generateQRCode(mUserProfile.getmUserId(), mUserProfile.getmName());
        setup();
    }

    private void generateQRCode(String userId, String userName) {
        String mid = "MED" + userName.substring(0, 3) + "_" + userId;
        Log.i(TAG, mid);
        new LoadImage().execute("http://www.barcodes4.me/barcode/qr/qr.png?value=" + mid + "&size=10&ecclevel=3");
    }

    private void setup() {
        img_qrCode = (ImageView) findViewById(R.id.img_QrCode_generate_qr_code_activity);
        rootLayout= (LinearLayout) findViewById(R.id.root_qrcode_generate_activity);
    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public class LoadImage extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
                snkBar= Snackbar.make(rootLayout,"something went wrong.",Snackbar.LENGTH_INDEFINITE);
                snkBar.setAction("Try again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        generateQRCode(mUserProfile.getmUserId(), mUserProfile.getmName());
                        snkBar.dismiss();
                    }
                });
                snkBar.show();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap image) {
            if (image != null) {
                img_qrCode.setImageBitmap(image);
                progressDialog.dismiss();

            } else {
                progressDialog.dismiss();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(GenerateQRCodeActivity.this);
            progressDialog.setMessage("Loading Image ....");
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }
    }
}
