package com.medlifeinfinity.medlifeinfinity.activities.emergency;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.adapters.ContactListViewAdapter;
import com.medlifeinfinity.medlifeinfinity.customClasses.ContactsInformation;

import java.io.BufferedReader;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

//import android.os.StrictMode;

/**
 * Created by Vishal Chugh on 011 , 04/Mar/2016.
 */
public class EmergencyActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String DEFAULT = "N/A";
    public final int PICK_CONTACT = 0;
    private final String EmergencyContactListFileName = "MyEmergencyContactList";
    ArrayList<String> tempStringContactName, tempStringPhoneNumber;
    File fileName;
    private int loc;
    private ContactsInformation mContactsInformation;
    private FloatingActionButton fab;
    private AlertDialog.Builder builder;
    private View view;
    private CoordinatorLayout coordinatorLayout;
    private ContactListViewAdapter mAdapter;
    private AlertDialog OptionDialog;
    private ListView mContactListView;
    private ArrayList<ContactsInformation> mContactList;
    private Button mButtonSendSOS;
    private boolean tempFlag1, tempFlag2, tempFlag3;
    private View mViewContactAdd = null;
    private String mKeyName, mKeyPhoneNumber;
    private String mValuePhoneNumber, mValueName;
    private SharedPreferences prefsContactsList;
    private Cursor cursor;
    private CardView mCardViewInformation;
    private String information;
    private TextView mTextViewInfo;
    private Boolean mTemp = false;
    private boolean previouslyStarted;
    private SharedPreferences prefs;
    private String keyTemp;
    private ImageButton ibEditContact;
    private boolean mEditFlag = false;
    private String mNewValueName, mOldValueName, mNewValuePhoneNumber, mOldValuePhoneNumber;
    private AlertDialog.Builder editBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("SOS");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        information = getString(R.string.InformationGuidelineMessage);
        tempStringContactName = new ArrayList<String>();
        tempStringPhoneNumber = new ArrayList<String>();
        mContactList = new ArrayList<ContactsInformation>();
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_activity_layout);

        //fileName = getDatabasePath(EmergencyContactListFileName + ".xml");
        fileName = new File("/data/data/" + getPackageName() + "/databases/" + EmergencyContactListFileName + ".xml");
        findView(); // code for adding object to views
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
        checkForFirstTime();
    }

    private void checkForFirstTime() {
        try {

            prefs = getSharedPreferences(EmergencyContactListFileName, MODE_PRIVATE);
            logMessage("checked for first time run ");
            keyTemp = prefs.getString("Key1contactName", DEFAULT);
            logMessage("HELLO " + keyTemp);
            if (keyTemp.equals(DEFAULT)) {
                logMessage("File Exist but Data Not available");
                mTemp = false;
            } else {
                logMessage("File Exist and Data also available");
                mTemp = true;
            }

            previouslyStarted = prefs.getBoolean(getString(R.string.pref_previously_started), mTemp);
            if (!previouslyStarted) {
                logMessage("prefs not stored");
                fileName = getDatabasePath(EmergencyContactListFileName + ".xml");
                Boolean m = fileName.delete();
                logMessage(m.toString());
                mCardViewInformation.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);
            } else if (previouslyStarted) {
                mCardViewInformation.setVisibility(View.GONE);
                fab.setVisibility(View.GONE);
                logMessage("prefs Data stored");
                loadData(prefs);
                mContactListView.setVisibility(View.VISIBLE);
                mButtonSendSOS.setVisibility(View.VISIBLE);

            }
        } catch (Exception ex) {
            logMessage("In Catch Block EXCEPTION :  " + ex);
        }

    }

    private void logMessage(String str) {
        Log.d("TAG", str);
    }

    private void loadData(SharedPreferences tempprefs) {
        String tName, tPhoneNumber;
        for (int i = 1; i <= 3; i++) {
            mContactsInformation = new ContactsInformation();
            mKeyName = "Key" + i + "contactName";
            mKeyPhoneNumber = "Key" + i + "PhoneNumber";

            tName = tempprefs.getString(mKeyName, DEFAULT);
            tPhoneNumber = tempprefs.getString(mKeyPhoneNumber, DEFAULT);
            logMessage(tName);
            logMessage(tPhoneNumber);
            mContactsInformation.setmPersonName(tName);
            mContactsInformation.setmPersonPhoneNumber(tPhoneNumber);
            mContactList.add(mContactsInformation);
        }
        mAdapter = new ContactListViewAdapter(this, mContactList);
        mContactListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void check() {
        if (tempFlag1 && tempFlag2 && tempFlag3) {
            storeContactsDataInPhoneSharedPrefs();
            mCardViewInformation.setVisibility(View.GONE);
            mButtonSendSOS.setVisibility(View.VISIBLE);
            mAdapter.notifyDataSetChanged();
            mContactListView.setVisibility(View.VISIBLE);
            OptionDialog.dismiss();
        }
    }

    private void findView() {
        mAdapter = new ContactListViewAdapter(this, mContactList);
        mContactListView = (ListView) findViewById(R.id.lv_contactList);
        mContactListView.setAdapter(mAdapter);
        mTextViewInfo = (TextView) findViewById(R.id.tv_information);
        mButtonSendSOS = (Button) findViewById(R.id.btn_sos);
        mCardViewInformation = (CardView) findViewById(R.id.cv_information);


        mButtonSendSOS.setVisibility(View.GONE);
        mButtonSendSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String m1, m2, m3;
                m1 = mContactList.get(0).getmPersonPhoneNumber();
                m2 = mContactList.get(1).getmPersonPhoneNumber();
                m3 = mContactList.get(2).getmPersonPhoneNumber();

                ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                try {


                    if (connec != null && (
                            (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) ||
                                    (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED))) {

                        //You are connected, do something online.
                        Toast.makeText(getApplicationContext(), "Connected to Internet", Toast.LENGTH_LONG).show();
                        send_SOS_MessagesByInternet(m1, m2, m3);

                    } else if (connec != null && (
                            (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) ||
                                    (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED))) {

                        //Not connected.
                        Toast.makeText(getApplicationContext(), "Internet Connection Not available.\n Sending message by phone account Balance", Toast.LENGTH_LONG).show();
                        send_SOS_MessageByPhoneBalance(m1, m2, m3);
                    }
//                Toast.makeText(EmergencyModuleMainActivity.this, "Send SOS Button pressed But it's working is disabled by admin<VishalChugh>", Toast.LENGTH_SHORT).show();

                } catch (Exception ex) {
                    Log.i("TAG", ex.getStackTrace().toString());
                }
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab.setVisibility(View.GONE);
                showContactsAdd();
            }
        });

        mContactListView.setOnItemClickListener(this);
    }

    private void send_SOS_MessageByPhoneBalance(String m1, String m2, String m3) {
        final Intent intentSendSMS = new Intent(Intent.ACTION_VIEW);
        String message = getString(R.string.EmergencyMessage);
        intentSendSMS.putExtra("Address", m1 + ";" + m2 + ";" + m3);
        intentSendSMS.putExtra("sms_body", message);
        intentSendSMS.setType("vnd.android-dir/mms-sms");

        builder = new AlertDialog.Builder(this);
        builder.setMessage("Some Data has changed. do you want to save it.");
        builder.setTitle("Warning");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(EmergencyActivity.this, "sending Message by phone balance.", Toast.LENGTH_SHORT).show();

                startActivity(intentSendSMS);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(EmergencyActivity.this, "Send Message Canceled", Toast.LENGTH_SHORT).show();

            }
        });
        builder.show();


    }

    private void send_SOS_MessagesByInternet(String m1, String m2, String m3) {
        //Your authentication key
        String authkey = getString(R.string.msg91_auth_key);

        //Multiple mobiles numbers separated by comma
        String mobiles = m1 + "," + m2 + "," + m3;
        //Sender ID,While using route4 sender id should be 6 characters long.
        String senderId = getString(R.string.SENDER_ID);
        //Your message to send, Add URL encoding here.
        String message = getString(R.string.EmergencyMessage);
        //define route
        String route = "4";
        URLConnection myURLConnection = null;
        URL myURL = null;
        BufferedReader reader = null;
        //encoding message
        String encoded_message = URLEncoder.encode(message);

        //Send SMS API
        String mainUrl = "http://api.msg91.com/api/sendhttp.php?";

        //Prepare parameter string
        StringBuilder sbPostData = new StringBuilder(mainUrl);
        sbPostData.append("authkey=" + authkey);
        sbPostData.append("&mobiles=" + mobiles);
        sbPostData.append("&message=" + encoded_message);
        sbPostData.append("&route=" + route);
        sbPostData.append("&sender=" + senderId);

        //final string
        mainUrl = sbPostData.toString();
        try {
            //prepare connection

            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, mainUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(EmergencyActivity.this, "Response is :" + response.toString(), Toast.LENGTH_SHORT).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(EmergencyActivity.this, "Error is " + error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            queue.add(stringRequest);

//            myURL = new URL(mainUrl);
//            myURLConnection = myURL.openConnection();
//            myURLConnection.connect();
//            reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
//
//            //reading response
//            String response;
//            while ((response = reader.readLine()) != null)
//                //print response
//                Log.d("RESPONSE", "" + response);
//
//            //finally close connection
//            reader.close();
            mButtonSendSOS.setVisibility(View.GONE);
        } catch (Exception e) {
            Log.e("TAG Error", e.toString());
            e.printStackTrace();
        }
    }

    public void pickContact() {
        try {
            Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(i, PICK_CONTACT);
//            Toast.makeText(EmergencyModuleMainActivity.this, "successful", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.d("TAG", "Pick Contact ERROR" + e.toString());
        }
    }

    public void showContactsAdd() {
        view = getLayoutInflater().inflate(R.layout.dialog_emergency_contacts, null);
        (view.findViewById(R.id.tv_label_1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact();
                mViewContactAdd = v;

            }
        });
        (view.findViewById(R.id.tv_label_2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact();
                mViewContactAdd = v;
                tempFlag2 = true;
            }
        });
        (view.findViewById(R.id.tv_label_3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickContact();
                mViewContactAdd = v;
                tempFlag3 = true;
            }
        });
        builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle("Enter Emergency Details");
        OptionDialog = builder.create();
        OptionDialog.setCancelable(true);
        OptionDialog.setCanceledOnTouchOutside(true);
        OptionDialog.show();
    }

    private void storeContactsDataInPhoneSharedPrefs() {
        String tempName = "";
        String tempPhoneNumber = "";
        int c = 1;
        try {
            prefsContactsList = getSharedPreferences(EmergencyContactListFileName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefsContactsList.edit();
            for (ContactsInformation temp : mContactList) {
                tempName = temp.getmPersonName();
                tempPhoneNumber = temp.getmPersonPhoneNumber();

                mKeyName = "Key" + c + "contactName";
                mValueName = tempName;

                mKeyPhoneNumber = "Key" + c + "PhoneNumber";
                mValuePhoneNumber = tempPhoneNumber;


                logMessage("SAVE  " + mKeyName + " -> " + mValueName);
                logMessage("SAVE  " + mKeyPhoneNumber + " -> " + mValuePhoneNumber);

                editor.putString(mKeyName, mValueName);
                editor.putString(mKeyPhoneNumber, mValuePhoneNumber);
                editor.apply();
                c++;
            }

            Snackbar.make(coordinatorLayout, "Sucessfully Stored Data.", Snackbar.LENGTH_SHORT).show();
            fab.hide();
        } catch (Exception ex) {
            Log.e("TAG", "Error while saving contacts data in prefs : \n" + ex.toString());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PICK_CONTACT && resultCode == RESULT_OK) {
            mContactsInformation = new ContactsInformation();
            Uri contactUri = data.getData();
            cursor = getContentResolver().query(contactUri, null, null, null, null);
            cursor.moveToFirst();
            String unformattedPhoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            String formattedPhoneNumber = phoneNumberSequencer(unformattedPhoneNumber);
            mContactsInformation.setmPersonName(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
            mContactsInformation.setmPersonPhoneNumber(formattedPhoneNumber);
            cursor.close();
            if (formattedPhoneNumber.length() == 10 && (formattedPhoneNumber.startsWith("7") || formattedPhoneNumber.startsWith("8") || formattedPhoneNumber.startsWith("9"))) {
                if (mEditFlag) {
                    editContact(loc);
                } else if (!mEditFlag) {
                    mContactList.add(mContactsInformation);
                    mViewContactAdd.setVisibility(View.GONE);
                    changeFlags(mViewContactAdd);
                    check();
                }

            } else {
                Toast.makeText(this, "This contact has no phone number\nPlease choose another contact", Toast.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Please select a contact", Toast.LENGTH_SHORT).show();
        }

    }

    private void editContact(int pos) {
        prefsContactsList = getSharedPreferences(EmergencyContactListFileName, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefsContactsList.edit();
        final int b = pos - 1;
        mKeyName = "Key" + pos + "contactName";
        mNewValueName = mContactsInformation.getmPersonName();
        mOldValueName = mContactList.get(b).getmPersonName();
        mKeyPhoneNumber = "Key" + pos + "PhoneNumber";
        mNewValuePhoneNumber = mContactsInformation.getmPersonPhoneNumber();
        mOldValuePhoneNumber = mContactList.get(b).getmPersonPhoneNumber();
        logMessage("EDITED  " + mOldValueName + " -> " + mNewValueName);
        logMessage("EDITED  " + mOldValuePhoneNumber + " -> " + mNewValuePhoneNumber);

        editBuilder = new AlertDialog.Builder(this).setIcon(R.drawable.ic_warning);
        editBuilder.setCancelable(false);
        editBuilder.setTitle("Confirmation");
        editBuilder.setMessage("Do you want to replace your old Contact : " + mOldValueName + " to " + mNewValueName);
        editBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logMessage("CHANGED");
                editor.putString(mKeyName, mNewValueName);
                editor.putString(mKeyPhoneNumber, mNewValuePhoneNumber);
                editor.apply();
                mContactList.remove(b);
                mContactList.add(b, mContactsInformation);
                mAdapter.notifyDataSetChanged();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logMessage("NOT CHANGED");
            }
        }).create().show();

    }

    private void changeFlags(View viewAdd) {
        if (viewAdd.getId() == R.id.tv_label_1) {
            tempFlag1 = true;
        }
        if (viewAdd.getId() == R.id.tv_label_2) {
            tempFlag2 = true;
        }
        if (viewAdd.getId() == R.id.tv_label_3) {
            tempFlag3 = true;
        }
    }

    private String phoneNumberSequencer(String number) {

        if (number.startsWith("+91")) {
            number = number.substring(3, number.length());
            Log.d("TAG without +91", number);
        } else if (number.startsWith("0")) {
            number = number.substring(1, number.length());
            Log.d("TAG without 0", number);
        }
        number = number.replaceAll("\\D+", "");
        Log.d("TAG ", number);
        return number;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        long viewId = view.getId();
        if (viewId == R.id.ib_edit_contact) {
            mEditFlag = true;
            loc = position + 1;
            logMessage("Edit Button Clicked with position no " + loc);
            pickContact();
        } else {
            logMessage("hello");
        }
    }
}