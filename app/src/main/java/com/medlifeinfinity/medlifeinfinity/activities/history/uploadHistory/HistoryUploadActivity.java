package com.medlifeinfinity.medlifeinfinity.activities.history.uploadHistory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class HistoryUploadActivity extends AppCompatActivity {
    private static final String TAG = HistoryUploadActivity.class.getSimpleName();
    public static final String UPLOAD_URL = "http://mani11-001-site1.ctempurl.com/api/WebApi/InsertIntoUpdateHistory";


    Bitmap mThumbnail, mBitmap;
    private Uri mFilePath;
    int mRequestCamera = 0, mSelectedFile = 1;
    ByteArrayOutputStream mByteArrayOutputStream;

    CharSequence[] mItems;
    File mImageDestination;
    EditText et_DoctorName,et_Disease;

    ImageView iv_UploadedImage;
    Spinner spn_uploadType;
    Button btn_Upload, btn_Cancel, btn_SelectImageDialog;
    String mDoctorName, mDate, mDocumentType,mDisease;
    String mSelectedImagePath,mActualImageName ="",mImageName ="",mEncodedImage ="";
    private ArrayList<String> selectedImagesPaths=new ArrayList<>();


    GridView mGridViewShowingSelectedImages;
    private ProgressDialog progressDialog;
    private Toolbar mToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_upload);

        getAllReferences();
        addDocumentType();

        btn_SelectImageDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btn_Upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });



    }

    public void getAllReferences()
    {
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Upload History");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_UploadedImage = (ImageView) findViewById(R.id.iv_uploadImage_history_upload);
        btn_SelectImageDialog = (Button) findViewById(R.id.btn_selectImage_history_upload);
        spn_uploadType = (Spinner) findViewById(R.id.spn_upload_type_history_upload);
        et_DoctorName = (EditText) findViewById(R.id.et_doctorName_history_upload);
        et_Disease = (EditText) findViewById(R.id.et_disease_history_upload);
        btn_Upload = (Button) findViewById(R.id.btn_upload_history_upload);
        btn_Cancel = (Button) findViewById(R.id.btn_cancel_history_upload);
    }

    public void addDocumentType()
    {
        List<String> list=new ArrayList<>();

        list.add("Select Type");
        list.add("Prescription");
        list.add("X-Ray");
        list.add("OtherTypes");

        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(HistoryUploadActivity.this,R.layout.support_simple_spinner_dropdown_item,list);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_uploadType.setAdapter(arrayAdapter);

    }


    /*   Code for Dialog Display, Camera Start, Picture select From Gallery and display picture in Image View                     */
    private void selectImage() {
        mItems = new CharSequence[]{"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(mItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (mItems[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, mRequestCamera);
                } else if (mItems[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            mSelectedFile);
                }
                else if (mItems[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG,"ResultCode "+requestCode+" requestCode "+requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == mSelectedFile)
              //  OnSelectImageFromGalleryResultMultiple(data);
            onSelectFromGalleryResult(data);
            else if (requestCode == mRequestCamera)
                onCaptureImageResult(data);
        }



    }


    private void onCaptureImageResult(Intent data) {
        mThumbnail = (Bitmap) data.getExtras().get("data");
        mByteArrayOutputStream = new ByteArrayOutputStream();
        mImageDestination = new File(Environment.getExternalStorageDirectory(),System.currentTimeMillis() + ".jpg");
        mFilePath = data.getData();
        FileOutputStream fo;
        try {
            mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mFilePath);
            mImageDestination.createNewFile();
            fo = new FileOutputStream(mImageDestination);
            fo.write(mByteArrayOutputStream.toByteArray());
            Log.i(TAG, mImageDestination.toString());      // Print Destination Here
            getBitmapImage(mThumbnail);
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        iv_UploadedImage.setVisibility(View.VISIBLE);
        iv_UploadedImage.setImageBitmap(mThumbnail);

        //      No need of Image Name But Due to API need to use
        mImageName = String.valueOf(mImageDestination);

        String[] separated = mImageName.split("/");
        separated[0] = separated[0].trim();
        separated[1] = separated[1].trim();
        separated[2] = separated[2].trim();
        separated[3] = separated[3].trim();
        separated[4] = separated[4].trim();
        mActualImageName = separated[4];

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        mByteArrayOutputStream = new ByteArrayOutputStream();
        mFilePath = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(mFilePath, projection, null, null,null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        mSelectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mSelectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(mSelectedImagePath, options);
        getBitmapImage(bm);
        iv_UploadedImage.setImageBitmap(bm);
//        Toast.makeText(getApplicationContext(),"Your selected Path = "+ mSelectedImagePath,Toast.LENGTH_SHORT).show();
             Log.i(TAG, mSelectedImagePath.toString());

//      No need of Image Name But Due to API need to use
        mImageName = mSelectedImagePath;

        String[] separated = mImageName.split("/");
        separated[0] = separated[0].trim();
        separated[1] = separated[1].trim();
        separated[2] = separated[2].trim();
        separated[3] = separated[3].trim();
        separated[4] = separated[4].trim();
        separated[5] = separated[5].trim();
        mActualImageName = separated[5];
  //       mActualImageName = mImageName;
        Log.i("Actual ImageName", mActualImageName.toString());
    }

    public void addData()
    {

        mDoctorName = et_DoctorName.getText().toString();
        mDocumentType = spn_uploadType.getSelectedItem().toString();
        mDisease = et_Disease.getText().toString();

        //      Validation

        if(mDoctorName.length()==0)
        {
            et_DoctorName.requestFocus();

            AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
            builder.setMessage("DOCTOR NAME CANNOT BE EMPTY");
            builder.setTitle("Error Occur");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    et_DoctorName.setText("");
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        else if(mDoctorName.length()<=3)
        {
            et_DoctorName.requestFocus();

            AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
            builder.setMessage("DOCTOR NAME CANNOT BE LESS THAN 4 CHARACTER");
            builder.setTitle("Error Occur");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else if(mDisease.length()==0)
        {
            et_Disease.requestFocus();

            AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
            builder.setMessage("DISEASE CANNOT BE EMPTY");
            builder.setTitle("Error Occur");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    et_Disease.requestFocus();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        else if(mDocumentType.equals("")|| mDocumentType.equalsIgnoreCase("Select Type"))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
            builder.setMessage("SELECT DOCUMENT TYPE");
            builder.setTitle("Error Occur");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    spn_uploadType.setFocusable(true);
                    spn_uploadType.setFocusableInTouchMode(true);
                    spn_uploadType.requestFocus();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        else if(iv_UploadedImage.getDrawable()== null)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(HistoryUploadActivity.this);
            builder.setMessage("PLEASE UPLOAD THE IMAGE");
            builder.setTitle("Error Occur");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    btn_SelectImageDialog.requestFocus();
                    btn_SelectImageDialog.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        else
        {
                uploadImage();
//            new UploadImage().execute();

        }
    }

    public class UploadImage extends AsyncTask<Void, Void, Void>
    {

//        private final ProgressDialog dialog = new ProgressDialog(HistoryUploadActivity.this);

        protected void onPreExecute() {
            progressDialog = new ProgressDialog(HistoryUploadActivity.this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.show();
        progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {

            uploadImage(); // inside the method paste your file uploading code

            return null;
        }

        protected void onPostExecute(Void result) {

            // Here if you wish to do future process for ex. move to another activity do here

            if (progressDialog.isShowing()) {
                hideProgressDialog();
            }

        }
    }
    public void uploadImage()
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait.....");
        progressDialog.show();
        progressDialog.setCancelable(false);

        JSONObject requestJSONObject = createRequestJSON(mDisease,mDocumentType, mDoctorName,Integer.parseInt(User.getInstance().getmUserId()), mActualImageName, mEncodedImage);

        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, UPLOAD_URL,requestJSONObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(TAG,"response "+response);
                        Toast.makeText(getApplicationContext(),"Successfully Data Inserted",Toast.LENGTH_LONG).show();
                        hideProgressDialog();
                        cancel();
                    }
                },
                new Response.ErrorListener() {//not response the show the error
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error here " + error);
                        Toast.makeText(HistoryUploadActivity.this, "Response Received with Error", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                //        cancel();
                    }
                });

        queue.add(request);
        request.setShouldCache(true);
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    public String getBitmapImage(Bitmap bmp){
    //    mByteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, mByteArrayOutputStream);
        byte[] imageBytes = mByteArrayOutputStream.toByteArray();
        mEncodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return mEncodedImage;
    }


    private JSONObject createRequestJSON( String v_Disease, String v_DocumentType, String v_DoctorName, int v_EntryBy, String v_Destination, String v_ImageUrl){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", "NA");
            jsonObject.put("Symtoms", v_Disease);
            jsonObject.put("Type", v_DocumentType);
            jsonObject.put("Name", v_DoctorName);
            jsonObject.put("EntryBy_Id", v_EntryBy);
            jsonObject.put("Image_Name",v_Destination);
            jsonObject.put("Image_URL",v_ImageUrl);
        }catch (JSONException exp){
            Log.e(TAG," Error : "+exp);
        }
        return jsonObject;
    }


    private void hideProgressDialog()
    {
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog=null;
        }
    }

    // For Cancel Button
    public void cancel()
    {

        et_DoctorName.setText("");
        et_Disease.setText("");
        spn_uploadType.setSelection(0);
        iv_UploadedImage.setImageResource(0);
        et_DoctorName.requestFocus();
        mDoctorName = "";
        mDate = "";
        selectedImagesPaths = null;
        mImageDestination = null;

    }

}
