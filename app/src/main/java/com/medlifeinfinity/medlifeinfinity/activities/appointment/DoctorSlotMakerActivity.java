package com.medlifeinfinity.medlifeinfinity.activities.appointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.adapters.AppointmentViewerAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.Appointment;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import hirondelle.date4j.DateTime;

public class DoctorSlotMakerActivity extends AppCompatActivity {

    private static final String TAG = DoctorSlotMakerActivity.class.getSimpleName();
    private ListView lv_slotsPreview;
    private Button btn_addSlots, btn_punchSlots;
    private Spinner spn_slotSize;
    private EditText et_startTime;
    private Toolbar mToolBar;
    private DateTime startTime;
    private AppointmentViewerAdapter adapter;
    private User mUserProfile;
    FloatingActionButton fab_uploadSlots;
    private String mSelectedDate;
    private java.lang.String mDay;
    private ProgressDialog progressDialog;
    private Snackbar mSnkBar;
    private CoordinatorLayout mRootLayout;
    private int counter =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_slot_maker);
        mSelectedDate = getIntent().getStringExtra("select date");
        setup();
    }

    private void setup() {
        mRootLayout = (CoordinatorLayout) findViewById(R.id.root_doctor_slot_maker_activity);

        mToolBar = (Toolbar)findViewById(R.id.app_bar);
        mToolBar.setTitle("Appointments");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        adapter = new AppointmentViewerAdapter(this, DoctorSlotMakerActivity.class.getSimpleName());
        fab_uploadSlots = (FloatingActionButton) findViewById(R.id.fab_upload_slots_doctor_slot_maker_activity);
        fab_uploadSlots.setVisibility(View.INVISIBLE);
        lv_slotsPreview = (ListView) findViewById(R.id.lv_slots_preview_doctor_slot_maker_activity);
        et_startTime = (EditText) findViewById(R.id.et_date_doctor_slot_maker_activity);
        btn_addSlots = (Button) findViewById(R.id.btn_add_slots_doctor_slot_maker_activity);
        spn_slotSize = (Spinner) findViewById(R.id.spn_slots_size_doctor_slot_maker_activity);
        lv_slotsPreview.setAdapter(adapter);
        fab_uploadSlots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    counter=0;
                    for (int i = 0; i < adapter.getmAppointmentList().size(); i++) {

                        JSONObject body = Appointment.getJSONObject(adapter.getmAppointmentList().get(i));
                        uploadSlots(body);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        et_startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                        Boolean flagAmPm = true; //Default is morning A.M.
                        if (hourOfDay > 12) {
                            hourOfDay = hourOfDay - 12;
                            flagAmPm = false; // changed to p.m.
                        }
                        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
                        String minuteString = minute < 10 ? "0" + minute : "" + minute;
                        String selectedStartTime = hourString + " : " + minuteString + "" + (flagAmPm ? " a.m." : " p.m.");
                        et_startTime.setText(selectedStartTime);
                        startTime = DateTime.forTimeOnly(hourOfDay, minute, second, 0);
                    }
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                tpd.setTitle("Select Start Time");
                tpd.vibrate(true);
                tpd.dismissOnPause(true);
                tpd.show(getFragmentManager(), "TimePickerDialog");
                btn_addSlots.setEnabled(true);
            }
        });

        btn_addSlots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slotsize = Integer.parseInt(spn_slotSize.getSelectedItem().toString());
                String slotTime = addTime(slotsize);
                getTimeStringFromHandM(startTime.getHour(), startTime.getMinute());
                adapter.addAppointment(new Appointment(slotTime, mSelectedDate, User.getInstance().getmUserId()));
                adapter.notifyDataSetChanged();
                adapter.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        if (adapter.getCount() > 0) {
                            fab_uploadSlots.setVisibility(View.VISIBLE);
                        } else {
                            fab_uploadSlots.setVisibility(View.INVISIBLE);
                        }
                    }
                });

            }
        });


    }

    private void uploadSlots(final JSONObject body) throws JSONException {
        String url = ApiCall.HOST_URL + "InsertIntoAppointment";
            if ((Utility.isNetworkAvailable(this))) {
                createProgressDialog(this,"Punching Appointment..", false);
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, body, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String result="";
                        Log.i(TAG, response.toString());
                        try {
                            result = response.getString("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(result.equals("Record Successfully Saved"))
                        {
                            counter++;
                            if(counter==adapter.getmAppointmentList().size()-1);
                            {
                                Toast.makeText(DoctorSlotMakerActivity.this,counter +" Slots inserted",Toast.LENGTH_SHORT).show();
                                adapter.getmAppointmentList().clear();
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else {
                            mSnkBar = Snackbar.make(mRootLayout, "Something went wrong", Snackbar.LENGTH_INDEFINITE);
                            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        uploadSlots(body);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSnkBar.dismiss();
                                }
                            });
                            mSnkBar.show();
                        }
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(TAG, error.toString());
                        hideProgressDialog();
                    }
                });
                VolleySingleton.getInstance(this).getRequestQueue().add(request);
            } else {
                mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
                mSnkBar.setAction("Try Again", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSnkBar.dismiss();
                    }
                });
                mSnkBar.show();
            }
        hideProgressDialog();
    }

    private String addTime(int slotSize) {
        startTime.plus(0, 0, 0, 0, 0, 0, 0, DateTime.DayOverflow.FirstDay);
        String time = startTime.getHour().toString() + ":" + startTime.getMinute().toString();
        startTime = startTime.plus(0, 0, 0, 0, slotSize, 0, 0, DateTime.DayOverflow.FirstDay);
        time = time + "-" + startTime.getHour().toString() + ":" + startTime.getMinute().toString();
        return time;
    }

    private void getTimeStringFromHandM(int hourOfDay, int minute) {
        Boolean flagAmPm = true; //Default is morning A.M.
        if (hourOfDay > 12) {
            hourOfDay = hourOfDay - 12;
            flagAmPm = false; // changed to p.m.
        }
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String selectedStartTime = hourString + " : " + minuteString + "" + (flagAmPm ? " a.m." : " p.m.");
        et_startTime.setText(selectedStartTime);
    }

    private void createProgressDialog(Context context,String msg, Boolean cancelable) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
