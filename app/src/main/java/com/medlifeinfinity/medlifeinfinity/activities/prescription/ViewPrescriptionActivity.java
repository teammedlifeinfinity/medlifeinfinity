package com.medlifeinfinity.medlifeinfinity.activities.prescription;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.MiniDashboradActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.ViewPrescriptionListAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ViewPrescriptionActivity extends AppCompatActivity {

    public static final String TAG = ViewPrescriptionActivity.class.getSimpleName();
    public static final String JSON_ARRAY = "data";
    public static final String KEY_MEDICINE_NAME = "Medicine_Name";
    public static final String KEY_QUANTITY = "Quantity";
    public static final String KEY_DOSE = "DoseDuration";
    public static final String KEY_MORNING = "Morning";
    public static final String KEY_NOON = "Afternoon";
    public static final String KEY_NIGHT = "Night";
    public static final String KEY_CREATED_BY = "Entry_ByDoc";
    //    public static final String KEY_CREATED_AT = "EntryDate";
    public static final String KEY_PATIENT_ID = "Patient_id";
    public static String[] MedicineName;
    public static String[] Quantity;
    public static String[] Dose;
    public static String[] CreatedBy;
    public static String[] CreatedAt;
    public static String[] PatentId;

    ListView lv_medical_details;
    User mUserProfile;
    private ProgressDialog progressDialog;
    private FloatingActionButton fab_addPrescription;
    private Toolbar mToolBar;
    String mDoctorId;
    String mPatientId;
    private int flag;
    private Boolean[] Noon;
    private Boolean[] Morning;
    private Boolean[] Night;
    private Snackbar mSnkBar;
    private CoordinatorLayout mRootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_prescription);
        mPatientId = getIntent().getStringExtra("patientId");
        mDoctorId = getIntent().getStringExtra("doctorId");
        flag = getIntent().getFlags();
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        mToolBar.setTitle("Prescription");
        mToolBar.setNavigationIcon(R.drawable.ic_arrow_back);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setup();
        getAllMedicineList();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        getAllMedicineList();
    }

    private void setup() {
        mRootLayout= (CoordinatorLayout) findViewById(R.id.root_view_prescription_activity);
        lv_medical_details = (ListView) findViewById(R.id.lv_medicine_details_view_prescription_activity);
        fab_addPrescription = (FloatingActionButton) findViewById(R.id.fab_add_prescription_view_prescription_activity);
        if (flag == MiniDashboradActivity.DOCTOR) {
            fab_addPrescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddPrescriptionActivity.class);
                    intent.putExtra("patientId", mPatientId);
                    intent.putExtra("doctorId", mDoctorId);
                    startActivity(intent);
                }
            });
        } else if (flag == MiniDashboradActivity.PATIENT) {
            fab_addPrescription.setVisibility(View.INVISIBLE);
        }
    }

    public void getAllMedicineList() {
        if (Utility.isNetworkAvailable(ViewPrescriptionActivity.this)) {
            String url = ApiCall.HOST_URL + "GetPriscriptionToDoctor/" + mDoctorId + "/" + mPatientId;
            createProgressDialog(this, "Loading...", false);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Response " + response);
//                Toast.makeText(ViewPrescriptionActivity.this, "Response Received Successfully", Toast.LENGTH_SHORT).show();
                    parseJSON(response);
                    ViewPrescriptionListAdapter listAdapter = new ViewPrescriptionListAdapter(getApplicationContext(), MedicineName, Quantity, Dose, CreatedBy, CreatedAt, PatentId, Noon, Morning, Night);
                    lv_medical_details.setAdapter(listAdapter);
                    hideProgressDialog();
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mSnkBar= Snackbar.make(mRootLayout,"Something went wrong",Snackbar.LENGTH_INDEFINITE);
                            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getAllMedicineList();
                                    mSnkBar.dismiss();
                                }
                            });
                            mSnkBar.show();
//                        Toast.makeText(ViewPrescriptionActivity.this, "Here" + error.getMessage(), Toast.LENGTH_LONG).show();
                            hideProgressDialog();
                        }

                    });
            VolleySingleton.getInstance(this).getRequestQueue().add(stringRequest);
        }
        else
        {
            mSnkBar = Snackbar.make(mRootLayout, "You are Offline.", Snackbar.LENGTH_INDEFINITE);
            mSnkBar.setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSnkBar.dismiss();
                    getAllMedicineList();
                }
            });
            mSnkBar.show();
        }
    }

    protected void parseJSON(String response) {

        try {
            JSONArray users = new JSONArray(response);
            MedicineName = new String[users.length()];
            Quantity = new String[users.length()];
            Dose = new String[users.length()];
            CreatedBy = new String[users.length()];
            PatentId = new String[users.length()];
            Noon = new Boolean[users.length()];
            Morning = new Boolean[users.length()];
            Night = new Boolean[users.length()];
            for (int i = 0; i < users.length(); i++) {
                JSONObject jo = users.getJSONObject(i);
                MedicineName[i] = jo.getString(KEY_MEDICINE_NAME);
                Quantity[i] = jo.getString(KEY_QUANTITY);
                Dose[i] = jo.getString(KEY_DOSE);
                Noon[i] = jo.getBoolean(KEY_NOON);
                Morning[i] = jo.getBoolean(KEY_MORNING);
                Night[i] = jo.getBoolean(KEY_NIGHT);
//                Log.i("Test",Morning[i].toString());
//                Log.i("Test",Noon[i].toString());
//                Log.i("Test",Night[i].toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void createProgressDialog(Context context, String msg, Boolean cancelable) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(cancelable);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
