package com.medlifeinfinity.medlifeinfinity.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.BookedAppointmentActivity;
import com.medlifeinfinity.medlifeinfinity.activities.appointment.ViewAppointmentActivity;
import com.medlifeinfinity.medlifeinfinity.activities.chat.MainChatActivity;
import com.medlifeinfinity.medlifeinfinity.activities.emergency.EmergencyActivity;
import com.medlifeinfinity.medlifeinfinity.activities.findDoctors.DoctorCategoryListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.history.viewHistory.ViewHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.activities.myDoctorList.MyDoctorListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.myPatientList.MyPatientListActivity;
import com.medlifeinfinity.medlifeinfinity.activities.nearBySearch.MainActivityOfFindNearByDoctors;
import com.medlifeinfinity.medlifeinfinity.activities.pillReminder.PillReminderActivity;
import com.medlifeinfinity.medlifeinfinity.activities.profile.UserProfileActivity;
import com.medlifeinfinity.medlifeinfinity.activities.qrcode.GenerateQRCodeActivity;
import com.medlifeinfinity.medlifeinfinity.activities.qrcode.QRCodeScannerActivity;
import com.medlifeinfinity.medlifeinfinity.activities.reaction.ReactionHistoryActivity;
import com.medlifeinfinity.medlifeinfinity.adapters.DashboardGridViewAdapter;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;
import com.medlifeinfinity.medlifeinfinity.customClasses.Constants;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;
import com.medlifeinfinity.medlifeinfinity.customClasses.VolleySingleton;

import java.util.Arrays;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardActivity extends AppCompatActivity {

    public final static String TAG = DashboardActivity.class.getCanonicalName();

    private Toolbar mToolBar;
    private NavigationView mNavigationDrawer;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private User mUserProfile;
    private CircleImageView img_userProfileImage;
    private TextView txt_userName, txt_userEmail;
    private GridView gv_dashboard;
    private DashboardGridViewAdapter mGridViewAdapter;
    private AlertDialog.Builder PickerDialog;
    private AlertDialog OptionDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_dashboard);
        if (!Utility.isNetworkAvailable(this)) {
            Toast.makeText(getApplicationContext(),"You are offline",Toast.LENGTH_SHORT).show();
        }
        mUserProfile = User.getInstance();
        Log.i(TAG, mUserProfile.toString());
        setUp();
        loadProfile();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mUserProfile=User.getInstance();
        loadProfile();
    }

    private void ImageStringRequestAndSetImage(String profilePictureUrl) {
        String imageURL = ApiCall.IMAGE_URL.toString() + profilePictureUrl;

        ImageLoader mImageLoader = VolleySingleton.getInstance(this).getImageLoader();
        mImageLoader.get(imageURL, ImageLoader.getImageListener(img_userProfileImage, R.drawable.ic_loading_animation, R.drawable.ic_no_image_found));
    }
    private void loadProfile() {
        Log.i(TAG, "loading image");
        txt_userEmail.setText(mUserProfile.getmEmail());
        txt_userName.setText(mUserProfile.getmName());

        if (mUserProfile.getmProfliePhoto() != null) {
            ImageStringRequestAndSetImage(mUserProfile.getmProfliePhoto());
        } else {

        }

        if (mUserProfile.getmRole().equals(User.DOCTOR_ROLE)) {
            String[] titleList = Constants.DASHBOARD_ITEM_DOCTOR.clone();
            Integer[] iconList = Constants.DASHBOARD_IMAGES_DOCTOR.clone();
            mGridViewAdapter = new DashboardGridViewAdapter(getApplicationContext(), Arrays.asList(titleList), Arrays.asList(iconList));
            gv_dashboard.setAdapter(mGridViewAdapter);
            gv_dashboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String temp = gv_dashboard.getAdapter().getItem(position).toString();
//                    Toast.makeText(getApplicationContext(), temp + " Clicked position " + position, Toast.LENGTH_SHORT).show();
                    Log.i("TAG", temp + " Id : " + id);

                    if (temp.equals("Find Doctors")) {
                        startActivity(new Intent(DashboardActivity.this, DoctorCategoryListActivity.class));
                    }
                    if (temp.equals("Emergency")) {
                        startActivity(new Intent(DashboardActivity.this, EmergencyActivity.class));
                    }
                    if (temp.equals("Pills Reminder")) {
                        startActivity(new Intent(DashboardActivity.this, PillReminderActivity.class));
                    }
                    if (temp.equals("QR Code")) {
                        PickerDialog = new AlertDialog.Builder(DashboardActivity.this);
                        View viewDialog = getLayoutInflater().inflate(R.layout.custom_dialog_qrcode_selection, null);
                        PickerDialog.setView(viewDialog);
                        viewDialog.findViewById(R.id.cv_scan_qrcode_qrcode_selection_custom_dialog).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(DashboardActivity.this, QRCodeScannerActivity.class));
                                OptionDialog.dismiss();

                            }
                        });
                        viewDialog.findViewById(R.id.cv_generate_qrcode_qrcode_selection_custom_dialog).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(DashboardActivity.this, GenerateQRCodeActivity.class));
                                OptionDialog.dismiss();
                            }
                        });
                        OptionDialog = PickerDialog.create();
                        OptionDialog.show();

                    }
                    if (temp.equals("Near By")) {
                        Intent intent = new Intent(DashboardActivity.this, MainActivityOfFindNearByDoctors.class);
                        startActivity(intent);
                    }
                    if (temp.equals("Profile")) {
                        Intent intent=new Intent(DashboardActivity.this, UserProfileActivity.class);
                        intent.putExtra("userId",User.getInstance().getmUserId());
                        intent.putExtra("class",DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                    if (temp.equals("History")) {
                        Intent intent = new Intent(DashboardActivity.this, ViewHistoryActivity.class);
                        intent.putExtra("userId", User.getInstance().getmUserId());
                        intent.putExtra("class", DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                    if (temp.equals("My Appointment")) {

                        Log.i(TAG,"appointment is clicked");
                        PickerDialog = new AlertDialog.Builder(DashboardActivity.this);
                        View viewDialog = getLayoutInflater().inflate(R.layout.custom_dialog_appointment, null);
                        PickerDialog.setView(viewDialog);
                        viewDialog.findViewById(R.id.cv_add_appointment_appointment_custom_dialog).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(DashboardActivity.this, ViewAppointmentActivity.class);
                                intent.putExtra("doctorId", mUserProfile.getmUserId());
                                intent.putExtra("class", DashboardActivity.class.getSimpleName());
                                startActivity(intent);
                                OptionDialog.dismiss();
                            }
                        });
                        viewDialog.findViewById(R.id.cv_view_appointment_appointment_custom_dialog).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(DashboardActivity.this, BookedAppointmentActivity.class);
                                intent.putExtra("doctorId", mUserProfile.getmUserId());
                                intent.putExtra("class", DashboardActivity.class.getSimpleName());
                                startActivity(intent);
                                OptionDialog.dismiss();
                            }
                        });
                        OptionDialog = PickerDialog.create();
                        OptionDialog.show();
                    }
                    if (temp.equals("Chat")) {
                        startActivity(new Intent(DashboardActivity.this, MainChatActivity.class));
                    }
                    if (temp.equals("My Patient List")) {

                        startActivity(new Intent(DashboardActivity.this, MyPatientListActivity.class));
                    }
                    if (temp.equals("My Doctor List")) {
                        startActivity(new Intent(DashboardActivity.this, MyDoctorListActivity.class));
                    }
                    if (temp.equals("Reaction")) {
                        Intent intent = new Intent(DashboardActivity.this, ReactionHistoryActivity.class);
                        intent.putExtra("userId", User.getInstance().getmUserId());
                        intent.putExtra("class", DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                }
            });
        } else if (mUserProfile.getmRole().equals(User.PATIENT_ROLE)) {
            String[] titleList = Constants.DASHBOARD_ITEM_PATIENT.clone();
            Integer[] iconList = Constants.DASHBOARD_IMAGES_PATIENT.clone();
            mGridViewAdapter = new DashboardGridViewAdapter(getApplicationContext(), Arrays.asList(titleList), Arrays.asList(iconList));
            gv_dashboard.setAdapter(mGridViewAdapter);
            gv_dashboard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String temp = gv_dashboard.getAdapter().getItem(position).toString();
//                    Toast.makeText(getApplicationContext(), temp + " Clicked position " + position, Toast.LENGTH_SHORT).show();
                    Log.i("TAG", temp + " Id : " + id);
                    if (temp.equals("Find Doctors")) {
                        startActivity(new Intent(DashboardActivity.this, DoctorCategoryListActivity.class));
                    }
                    if (temp.equals("Reaction")) {
                        Intent intent = new Intent(DashboardActivity.this, ReactionHistoryActivity.class);
                        intent.putExtra("userId", User.getInstance().getmUserId());
                        intent.putExtra("class", DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                    if (temp.equals("Emergency")) {
                        startActivity(new Intent(DashboardActivity.this, EmergencyActivity.class));
                    }
                    if (temp.equals("Pills Reminder")) {
                        startActivity(new Intent(DashboardActivity.this, PillReminderActivity.class));
                    }
                    if (temp.equals("Near By")) {
                        Intent intent = new Intent(DashboardActivity.this, MainActivityOfFindNearByDoctors.class);
                        startActivity(intent);
                    }
                    if (temp.equals("QR Code")) {
                        startActivity(new Intent(DashboardActivity.this, GenerateQRCodeActivity.class));
                    }
                    if (temp.equals("Profile")) {
                        Intent intent=new Intent(DashboardActivity.this, UserProfileActivity.class);
                        intent.putExtra("class",DashboardActivity.class.getSimpleName());
                        intent.putExtra("userId",User.getInstance().getmUserId());
                        startActivity(intent);
                    }
                    if (temp.equals("History")) {
                        Intent intent = new Intent(DashboardActivity.this, ViewHistoryActivity.class);
                        intent.putExtra("userId", User.getInstance().getmUserId());
                        intent.putExtra("class", DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                    if (temp.equals(("My Appointment"))) {
                        Log.i(TAG,"appointment is clicked");
                        Intent intent = new Intent(DashboardActivity.this, BookedAppointmentActivity.class);
                        intent.putExtra("doctorId", mUserProfile.getmUserId());
                        intent.putExtra("class", DashboardActivity.class.getSimpleName());
                        startActivity(intent);
                    }
                    if (temp.equals("My Doctor List")) {
                        startActivity(new Intent(DashboardActivity.this, MyDoctorListActivity.class));
                    }
                }
            });
        }
    }


    private void setUp() {
        mToolBar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolBar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolBar, R.string.drawer_open, R.string.drawer_close);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationDrawer = (NavigationView) findViewById(R.id.navigation_drawer);
        mNavigationDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.nav_menu_profile) {
                    Intent intent=new Intent(DashboardActivity.this, UserProfileActivity.class);
                    intent.putExtra("class",DashboardActivity.class.getSimpleName());
                    intent.putExtra("userId",User.getInstance().getmUserId());
                    startActivity(intent);
                }
                if (id == R.id.nav_menu_about_us) {
                    startActivity(new Intent(DashboardActivity.this, AboutUsActivity.class));
                }
                if (id == R.id.nav_menu_change_password) {
                    startActivity(new Intent(DashboardActivity.this, ChangePasswordActivity.class));
                }
                if (id == R.id.nav_menu_share) {
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this application\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=com.medlifeinfinity \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) { //e.toString();
                    }
                    Toast.makeText(getApplicationContext(), "Share clicked", Toast.LENGTH_SHORT).show();
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });

        View headerView = mNavigationDrawer.inflateHeaderView(R.layout.nav_header_dashboard_layout);
        img_userProfileImage = (CircleImageView) headerView.findViewById(R.id.nav_profile_image);
        txt_userName = (TextView) headerView.findViewById(R.id.nav_username);
        txt_userEmail = (TextView) headerView.findViewById(R.id.nav_email);
        gv_dashboard = (GridView) findViewById(R.id.gv_dashboard_dashboard_activity);


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard_overflow, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (R.id.menu_logout == item.getItemId()) {
//            Toast.makeText(getApplicationContext(), "Logout", Toast.LENGTH_SHORT).show();
            clearPreference();
            finish();
        }
        return true;
    }

    private void clearPreference() {
        SharedPreferences sharedPreferences = getSharedPreferences("LoginProfile", MODE_PRIVATE);
        if (sharedPreferences.edit().clear().commit()) {
            Log.i(TAG, "file deleted");
            startActivity(new Intent(getApplicationContext(), LoginAndRegisterActivity.class));
        } else
            Log.i(TAG, "file not deleted");
    }

}
