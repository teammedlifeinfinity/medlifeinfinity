package com.medlifeinfinity.medlifeinfinity.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.medlifeinfinity.medlifeinfinity.R;
import com.medlifeinfinity.medlifeinfinity.customClasses.User;
import com.medlifeinfinity.medlifeinfinity.customClasses.Utility;

import org.json.JSONArray;
import org.json.JSONException;

import io.saeid.fabloading.LoadingView;

public class SplashScreenActivity extends AppCompatActivity {

    public final static String TAG = SplashScreenActivity.class.getSimpleName();

    User mUserProfile;

    private LoadingView mLoadingView;

    private int[] splashBackgroundColors = {
            Color.parseColor("#BBDEFB"), Color.parseColor("#C8E6C9"),
            Color.parseColor("#FFF9C4"), Color.parseColor("#BBDEFB"),
            Color.parseColor("#FFCDD2"), Color.parseColor("#C8E6C9"),
            Color.parseColor("#FFF9C4"), Color.parseColor("#BBDEFB"),
            Color.parseColor("#FFCDD2"), Color.parseColor("#C8E6C9"),
            Color.parseColor("#FFF9C4"), Color.parseColor("#BBDEFB"),
            Color.parseColor("#FFCDD2"), Color.parseColor("#C8E6C9"),
            Color.parseColor("#FFF9C4"), Color.parseColor("#BBDEFB")};
    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        setContentView(R.layout.activity_splash_screen);
        createViewsObject();

    }

    private boolean checkExistingUserLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("LoginProfile", MODE_PRIVATE);
        if (sharedPreferences.getString("User", null) == null) {
            return false;
        } else {
            String temp = sharedPreferences.getString("User", null);
            Log.i(TAG, temp);
            try {
                User.getUserFromJsonArray(new JSONArray(temp));
                Log.i(TAG, mUserProfile.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    private void createViewsObject() {
        mUserProfile = User.getInstance();
        final RelativeLayout mRelativeLayout = (RelativeLayout) findViewById(R.id.relative_layout_splash_activity);
        mLoadingView = (LoadingView) findViewById(R.id.splash_loading_view_splash_activity);
        mLoadingView.addAnimation(Color.parseColor("#bb9900"), R.drawable.ic_doctor_loading1, LoadingView.FROM_TOP);
        mLoadingView.addAnimation(Color.parseColor("#0099BB"), R.drawable.ic_doctor_loading2, LoadingView.FROM_RIGHT);
        mLoadingView.addAnimation(Color.parseColor("#00AA99"), R.drawable.ic_doctor_loading3, LoadingView.FROM_BOTTOM);
        mLoadingView.addAnimation(Color.parseColor("#9900BB"), R.drawable.ic_doctor_loading4, LoadingView.FROM_LEFT);

        //also you can add listener for getting callback (optional)

        mLoadingView.addListener(new LoadingView.LoadingListener() {

            @Override
            public void onAnimationStart(int currentItemPosition) {
                if (i < 8) {
                    mRelativeLayout.setBackgroundColor(splashBackgroundColors[i]);
                    i++;
                }
            }

            @Override
            public void onAnimationRepeat(int nextItemPosition) {

                if (i >= 8) {
                    if (checkExistingUserLogin()) {
                        Log.i(TAG, "User Exist");
                        startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                    } else {
                        Log.i(TAG, "new User");
                        startActivity(new Intent(getApplicationContext(), LoginAndRegisterActivity.class));
                    }
                    finish();
                }
            }

            @Override
            public void onAnimationEnd(int nextItemPosition) {

            }

        });
        mLoadingView.startAnimation();
    }
}
