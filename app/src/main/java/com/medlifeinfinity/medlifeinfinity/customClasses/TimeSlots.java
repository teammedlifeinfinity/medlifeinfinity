package com.medlifeinfinity.medlifeinfinity.customClasses;

import java.util.Locale;

import hirondelle.date4j.DateTime;

/**
 * Created by Vaio on 4/12/2016.
 */
public class TimeSlots {
    DateTime mStartTime, mEndTime;

    public TimeSlots() {
    }

    public TimeSlots(DateTime mStartTime, DateTime mEndTime) {
        this.mStartTime = mStartTime;
        this.mEndTime = mEndTime;
    }

    public DateTime getmStartTime() {
        return mStartTime;
    }

    public void setmStartTime(DateTime mStartTime) {
        this.mStartTime = mStartTime;
    }

    public DateTime getmEndTime() {
        return mEndTime;
    }

    public void setmEndTime(DateTime mEndTime) {
        this.mEndTime = mEndTime;
    }

    @Override
    public String toString() {
        return this.mStartTime.format("h12:mm a", Locale.US) + " - " + this.getmEndTime().format("h12:mm a", Locale.US);
    }
}
