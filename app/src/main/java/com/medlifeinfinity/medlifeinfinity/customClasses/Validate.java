package com.medlifeinfinity.medlifeinfinity.customClasses;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validate {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String PHONE_NUMBER_PATTERN = "^[987]\\d{9}$";
    public static final String NAME_PATTERN = "^[\\p{L} .'-]+$";
    public static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$";


    /**
     * validate paassed string with passed validation pattern.
     *
     * @param stringToMatch     The String that needs to be validate .
     * @param validationPattern The String of pattern ,that is used to validate passed string.
     * @return return true if pattern matches with string otherwise false
     */
    public static boolean isValid(String stringToMatch, String validationPattern) {
        Pattern pattern = Pattern.compile(validationPattern);
        Matcher matcher = pattern.matcher(stringToMatch);
        return matcher.matches();
    }

}
