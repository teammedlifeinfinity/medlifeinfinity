package com.medlifeinfinity.medlifeinfinity.customClasses;

/**
 * Created by Vishal Chugh on 024 , 24/Feb/2016.
 */
public class ContactsInformation {

    private String mPersonName;
    private String mPersonPhoneNumber;

    public ContactsInformation() {
    }

    public ContactsInformation(String personName, String personPhoneNumber) {
        this.mPersonName = personName;
        this.mPersonPhoneNumber = personPhoneNumber;
    }

    @Override
    public String toString() {
        return "\nParson Name " + mPersonName + " Parson Phone Number " + mPersonPhoneNumber + "\n";
    }

    public String getmPersonName() {
        return mPersonName;
    }

    public void setmPersonName(String mPersonName) {
        this.mPersonName = mPersonName;
    }

    public String getmPersonPhoneNumber() {
        return mPersonPhoneNumber;
    }

    public void setmPersonPhoneNumber(String mPersonPhoneNumber) {
        this.mPersonPhoneNumber = mPersonPhoneNumber;
    }


}
