package com.medlifeinfinity.medlifeinfinity.customClasses;

import com.medlifeinfinity.medlifeinfinity.R;

/**
 * Created by Vaio on 4/14/2016.
 */

public class Constants {
    public static final String[] DASHBOARD_ITEM_DOCTOR = {
            "My Patient List",
            "My Doctor List",
            "My Appointment",
            "Find Doctors",
            "Emergency",
            "Pills Reminder",
            "QR Code",
            "Near By",
            "Profile",
            "History",
            "Chat",
            "Reaction",
    };

    public static final Integer[] DASHBOARD_IMAGES_DOCTOR = {
            R.drawable.ic_patient_list,
            R.drawable.ic_doctor_list,
            R.drawable.ic_appointment,
            R.drawable.ic_doctors,
            R.drawable.ic_emergency,
            R.drawable.ic_pill,
            R.drawable.ic_qr_code_generate,
            R.drawable.ic_nearby_doctors,
            R.drawable.ic_profile,
            R.drawable.ic_history,
            R.drawable.ic_chat,
            R.drawable.ic_reaction,
    };

    public static final String[] DASHBOARD_ITEM_PATIENT = {
            "My Doctor List",
            "My Appointment",
            "Find Doctors",
            "Emergency",
            "Pills Reminder",
            "Near By",
            "QR Code",
            "Profile",
            "History",
            "Reaction",
    };

    public static final Integer[] DASHBOARD_IMAGES_PATIENT = {
            R.drawable.ic_doctor_list,
            R.drawable.ic_appointment,
            R.drawable.ic_doctors,
            R.drawable.ic_emergency,
            R.drawable.ic_pill,
            R.drawable.ic_nearby_doctors,
            R.drawable.ic_qr_code_generate,
            R.drawable.ic_profile,
            R.drawable.ic_history,
            R.drawable.ic_reaction,
    };


    public static final String[] MINI_DASHBOARD_ITEM_DOCTOR_LIST = {
            "Diet Plan",
            "RMS",
            "Prescription",
            "Feedback",
    };

    public static final Integer[] MINI_DASHBOARD_IMAGES_DOCTOR_LIST = {
            R.drawable.ic_diet_plan,
            R.drawable.ic_rms,
            R.drawable.ic_prescription,
            R.drawable.ic_feedback,
    };

    public static final String[] MINI_DASHBOARD_ITEM_PATIENT_LIST = {
            "Diet Plan",
            "RMS",
            "Prescription",
            "Feedback",
            "Reaction",
            "History"
    };
    public static final Integer[] MINI_DASHBOARD_IMAGES_PATIENT_LIST = {
            R.drawable.ic_diet_plan,
            R.drawable.ic_rms,
            R.drawable.ic_prescription,
            R.drawable.ic_feedback,
            R.drawable.ic_reaction,
            R.drawable.ic_history
    };

}
