package com.medlifeinfinity.medlifeinfinity.customClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class GetHistory implements Parcelable {
    public String mUserId;//doctor's user id
    public String mDocName;
    public String mEntryDate;
    public String mDisease;
    public String mType;
    public String mImageName;
    public String mImageUrl;

    public GetHistory() {
    }

    public GetHistory(String mUserId, String mDocName, String mEntryDate, String mDisease, String mType, String mImageName, String mImageUrl) {
        this.mUserId = mUserId;
        this.mDocName = mDocName;
        this.mEntryDate = mEntryDate;
        this.mDisease = mDisease;
        this.mType = mType;
        this.mImageName = mImageName;
        this.mImageUrl = mImageUrl;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmDocName() {
        return mDocName;
    }

    public void setmDocName(String mDocName) {
        this.mDocName = mDocName;
    }

    public String getmEntryDate() {
        return mEntryDate;
    }

    public void setmEntryDate(String mEntryDate) {
        this.mEntryDate = mEntryDate;
    }

    public String getmDisease() {
        return mDisease;
    }

    public void setmDisease(String mDisease) {
        this.mDisease = mDisease;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmImageName() {
        return mImageName;
    }

    public void setmImageName(String mImageName) {
        this.mImageName = mImageName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    protected GetHistory(Parcel in) {
        mUserId = in.readString();
        mDocName = in.readString();
        mEntryDate = in.readString();
        mDisease = in.readString();
        mType = in.readString();
        mImageName = in.readString();
        mImageUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUserId);
        dest.writeString(mDocName);
        dest.writeString(mEntryDate);
        dest.writeString(mDisease);
        dest.writeString(mType);
        dest.writeString(mImageName);
        dest.writeString(mImageUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GetHistory> CREATOR = new Parcelable.Creator<GetHistory>() {
        @Override
        public GetHistory createFromParcel(Parcel in) {
            return new GetHistory(in);
        }

        @Override
        public GetHistory[] newArray(int size) {
            return new GetHistory[size];
        }
    };
}