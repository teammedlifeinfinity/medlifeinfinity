package com.medlifeinfinity.medlifeinfinity.customClasses;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Vishal Chugh on 031 , 31/Mar/2016.
 */
public class PillsReminderInformation implements Parcelable {
    @SuppressWarnings("unused")
    public static final Creator<PillsReminderInformation> CREATOR = new Creator<PillsReminderInformation>() {
        @Override
        public PillsReminderInformation createFromParcel(Parcel in) {
            return new PillsReminderInformation(in);
        }

        @Override
        public PillsReminderInformation[] newArray(int size) {
            return new PillsReminderInformation[size];
        }
    };
    private String mMedicineName;
    private String mMedicineNextTime;
    private String mMedicineDosage;
    private Integer mMedicineColor;
    private String mMedicineInterval;

    public PillsReminderInformation(String mMedicineName, String mMedicineNextTime, String mMedicineDosage, Integer mMedicineColor, String mMedicineInterval) {
        this.mMedicineName = mMedicineName;
        this.mMedicineNextTime = mMedicineNextTime;
        this.mMedicineDosage = mMedicineDosage;
        this.mMedicineColor = mMedicineColor;
        this.mMedicineInterval = mMedicineInterval;
    }

    public PillsReminderInformation() {
    }


    protected PillsReminderInformation(Parcel in) {
        mMedicineName = in.readString();
        mMedicineNextTime = in.readString();
        mMedicineDosage = in.readString();
        mMedicineColor = in.readByte() == 0x00 ? null : in.readInt();
        mMedicineInterval = in.readString();
    }

    public String getmMedicineInterval() {
        return mMedicineInterval;
    }

    public void setmMedicineInterval(String mMedicineInterval) {
        this.mMedicineInterval = mMedicineInterval;
    }

    public String getmMedicineName() {
        return mMedicineName;
    }

    public void setmMedicineName(String mMedicineName) {
        this.mMedicineName = mMedicineName;
    }

    public void setAll(String mMedicineName, String mMedicineNextTime, String mMedicineDosage, String mMedicineInterval, Integer mMedicineColor) {
        this.setmMedicineName(mMedicineName);
        this.setmMedicineDosage(mMedicineDosage);
        this.setmMedicineStartDateTime(mMedicineNextTime);
        this.setmMedicineInterval(mMedicineInterval);
        this.setmMedicineColor(mMedicineColor);
    }

    public String getmMedicineNextTime() {
        return mMedicineNextTime;
    }

    public void setmMedicineStartDateTime(String mMedicineNextTime) {
        this.mMedicineNextTime = mMedicineNextTime;
    }

    public String getmMedicineDosage() {
        return mMedicineDosage;
    }

    public void setmMedicineDosage(String mMedicineDosage) {
        this.mMedicineDosage = mMedicineDosage;
    }

    public Integer getmMedicineColor() {
        return mMedicineColor;
    }

    public void setmMedicineColor(Integer mMedicineColor) {
        this.mMedicineColor = mMedicineColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMedicineName);
        dest.writeString(mMedicineNextTime);
        dest.writeString(mMedicineDosage);
        dest.writeString(mMedicineInterval);
        if (mMedicineColor == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(mMedicineColor);
        }
    }

    @Override
    public String toString() {
        return "PillsReminderInformation{" +
                "mMedicineName='" + mMedicineName + '\'' +
                ", mMedicineNextTime='" + mMedicineNextTime + '\'' +
                ", mMedicineDosage='" + mMedicineDosage + '\'' +
                ", mMedicineColor=" + mMedicineColor +
                ", mMedicineInterval='" + mMedicineInterval + '\'' +
                '}';
    }
}