package com.medlifeinfinity.medlifeinfinity.customClasses;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Vaio on 4/15/2016.
 */
public class Appointment {

    public final static String BOOKED = "BOOKED";
    public final static String FREE = "FREE";
    public final static String AVAILABLE = "AVAILABLE";
    public final static String UNAVAILABLE = "UNAVAILABLE";

    private String mSlotId;
    private String mSlotTime;
    private Boolean mAppointmentStatus;
    private String mBookingDate;
    private String mEntryBy;
    private String mBookedBy;


    public Appointment() {
    }

    public Appointment(String mSlotId, String mSlotTime, Boolean mAppointmentStatus, String mBookingDate, String mEntryBy, String mBookedBy) {
        this.mSlotId = mSlotId;
        this.mSlotTime = mSlotTime;
        this.mAppointmentStatus = mAppointmentStatus;
        this.mBookingDate = mBookingDate;
        this.mEntryBy = mEntryBy;
        this.mBookedBy = mBookedBy;
    }

    public Appointment(JSONObject object) throws JSONException {
        mSlotId = object.getString("Slot_Id");
        mSlotTime = object.getString("Slot_Time");
        mAppointmentStatus = object.getBoolean("Status");
        mBookingDate = object.getString("Booking_Date");
        mEntryBy = object.getString("EntryBy_Id");
        mBookedBy = object.getString("BookBy_Id");
    }

    public  Appointment(String slotTime, String mSelectedDate, String userId) {
        this.mSlotTime = slotTime;
        this.mBookingDate = mSelectedDate;
        this.mEntryBy = userId;
        this.mAppointmentStatus=false;
    }

    public static JSONObject getJSONObject(Appointment appointment) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("Slot_Time",appointment.getmSlotTime());
        object.put("Booking_Date",appointment.getmBookingDate());
        object.put("EntryBy_Id",appointment.getmEntryBy());
        return object;
    }


    public String getmBookedBy() {
        return mBookedBy;
    }

    public void setmBookedBy(String mBookedBy) {
        this.mBookedBy = mBookedBy;
    }


    public String getmSlotId() {
        return mSlotId;
    }

    public void setmSlotId(String mSlotId) {
        this.mSlotId = mSlotId;
    }

    public String getmSlotTime() {
        return mSlotTime;
    }

    public void setmSlotTime(String mSlotTime) {
        this.mSlotTime = mSlotTime;
    }

    public Boolean getmAppointmentStatus() {
        return mAppointmentStatus;
    }

    public void setmAppointmentStatus(Boolean mAppointmentStatus) {
        this.mAppointmentStatus = mAppointmentStatus;
    }

    public String getmBookingDate() {
        return mBookingDate;
    }

    public void setmBookingDate(String mBookingDate) {
        this.mBookingDate = mBookingDate;
    }

    public String getmEntryBy() {
        return mEntryBy;
    }

    public void setmEntryBy(String mEntryBy) {
        this.mEntryBy = mEntryBy;
    }


}
