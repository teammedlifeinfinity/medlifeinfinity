package com.medlifeinfinity.medlifeinfinity.customClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.medlifeinfinity.medlifeinfinity.api.ApiCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class User {

    public static final String DOCTOR_ROLE = "1";
    public static final String PATIENT_ROLE = "2";
    public static final String CHEMIST_ROLE = "3";
    public static final String PEFERENCE_KEY = "User_preference";
    public static final String USER_PROFILE_KEY = "profilekey";
    private static User mUserInstance;

    private String mUserId;
    private String mName;
    private String mEmail;
    private String mProfliePhoto;
    private String mRole;
    private String mSpecialization;
    private String mQualification;
    private String mContactNumber;
    private String mBloodGroup;
    private String mAddress;
    private String mCity;
    private String mState;
    private String mGender;
    private String mDob;
    private String mPassword;
    private String mCountry;
    private String mLatitude;

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    @Override
    public String toString() {
        return "User{" +
                "mUserId='" + mUserId + '\'' +
                ", mName='" + mName + '\'' +
                ", mEmail='" + mEmail + '\'' +
                ", mProfliePhoto='" + mProfliePhoto + '\'' +
                ", mRole='" + mRole + '\'' +
                ", mSpecialization='" + mSpecialization + '\'' +
                ", mQualification='" + mQualification + '\'' +
                ", mContactNumber='" + mContactNumber + '\'' +
                ", mBloodGroup='" + mBloodGroup + '\'' +
                ", mAddress='" + mAddress + '\'' +
                ", mCity='" + mCity + '\'' +
                ", mState='" + mState + '\''  +
                ", mGender='" + mGender + '\'' +
                ", mDob='" + mDob + '\'' +
                ", mPassword='" + mPassword + '\'' +
                ", mCountry='" + mCountry + '\'' +
                ", mLatitude='" + mLatitude + '\'' +
                ", mLongitude='" + mLongitude + '\'' +
                '}';
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    private String mLongitude;

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    private User() {
        this.mName = "NA";
        this.mEmail = "NA";
        this.mProfliePhoto = "NA";
        this.mRole = DOCTOR_ROLE;
        this.mContactNumber = "NA";
        this.mAddress = "NA";
        this.mBloodGroup = "NA";
        this.mQualification = "NA";
        this.mDob = "NA";
        this.mCity = "NA";
        this.mState = "NA";
        this.mGender = "NA";
        this.mSpecialization = "NA";
        this.mCountry = "NA";
        this.mLatitude = "NA";
        this.mLongitude = "NA";
    }

    public static User getInstance() {
        if (mUserInstance == null) {
            mUserInstance = new User();
            return mUserInstance;
        } else {
            return mUserInstance;
        }
    }

    public static User getdumyInstance(String role) {
        User user = new User();
        user.setmUserId("1");
        user.setmName("sachin");
        user.setmEmail("sachin@gmail.com");
        user.setmSpecialization("abc");
        user.setmContactNumber("0123456789");
        user.setmRole(role);
        return user;
    }
    public static void getUserFromJsonObject(JSONObject userObject) throws JSONException {
        mUserInstance=new User();
        mUserInstance.setmUserId(userObject.getString("user_id"));
        mUserInstance.setmName(userObject.getString("Name"));
        mUserInstance.setmEmail(userObject.getString("Email"));
        mUserInstance.setmProfliePhoto(userObject.getString("Image"));
        mUserInstance.setmContactNumber(userObject.getString("Contact_No"));
        mUserInstance.setmGender(userObject.getString("Gender"));
        mUserInstance.setmRole(userObject.getString("Roll_Id"));
        mUserInstance.setmAddress(userObject.getString("Address"));
        mUserInstance.setmCity(userObject.getString("City"));
        mUserInstance.setmState(userObject.getString("State"));
        mUserInstance.setmCountry(userObject.getString("Country"));
        mUserInstance.setmLatitude(userObject.getString("Latitude"));
        mUserInstance.setmLongitude(userObject.getString("Longitude"));
        mUserInstance.setmSpecialization(userObject.getString("Specialization"));
        mUserInstance.setmQualification(userObject.getString("Qualification"));
        mUserInstance.setmBloodGroup(userObject.getString("Blood_Group"));
        mUserInstance.setmDob(userObject.getString("Age"));
        mUserInstance.setmPassword(userObject.getString("Password"));
    }

    public static User getUserObjectFromJsonObject(JSONObject userObject) throws JSONException {
        User userInstance=new User();
        userInstance.setmUserId(userObject.getString("user_id"));
        userInstance.setmName(userObject.getString("Name"));
        userInstance.setmEmail(userObject.getString("Email"));
        userInstance.setmState(userObject.getString("State"));
        userInstance.setmProfliePhoto(userObject.getString("Image"));
        userInstance.setmContactNumber(userObject.getString("Contact_No"));
        userInstance.setmSpecialization(userObject.getString("Specialization"));
        userInstance.setmRole(userObject.getString("Roll_Id"));
        userInstance.setmAddress(userObject.getString("Address"));
        userInstance.setmBloodGroup(userObject.getString("Blood_Group"));
        userInstance.setmQualification(userObject.getString("Qualification"));
        userInstance.setmPassword(userObject.getString("Password"));
        userInstance.setmCountry(userObject.getString("Country"));
        userInstance.setmLatitude(userObject.getString("Latitude"));
        userInstance.setmLongitude(userObject.getString("Longitude"));
        userInstance.setmDob(userObject.getString("Age"));
        userInstance.setmGender(userObject.getString("Gender"));
        return userInstance;
    }

    public static void getUserFromJsonArray(JSONArray response) throws JSONException {
        JSONObject userObject = response.getJSONObject(0);
        getUserFromJsonObject(userObject);
    }

    public static ArrayList<User> getUserArrayListFromJsonArray(JSONArray response) throws JSONException {
        ArrayList<User> userArrayList=new ArrayList<User>();
        for(int i=0;i<response.length();i++) {
            JSONObject userObject = response.getJSONObject(i);
            userArrayList.add(getUserObjectFromJsonObject(userObject));
        }
        return userArrayList;
    }

    public String getmQualification() {
        return mQualification;
    }

    public void setmQualification(String mQualification) {
        this.mQualification = mQualification;
    }

    public static void storeInPreference(String user, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("LoginProfile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("User", user);
        editor.apply();
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmSpecialization() {
        return mSpecialization;
    }

    public void setmSpecialization(String mSpecialization) {
        this.mSpecialization = mSpecialization;
    }

    public String getmContactNumber() {
        return mContactNumber;
    }

    public void setmContactNumber(String mContactNumber) {
        this.mContactNumber = mContactNumber;
    }

    public String getmBloodGroup() {
        return mBloodGroup;
    }

    public void setmBloodGroup(String mBloodGroup) {
        this.mBloodGroup = mBloodGroup;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmProfliePhoto() {
        if (mProfliePhoto != null)
            return mProfliePhoto;
        else
            throw new NullPointerException();
    }

    public void setmProfliePhoto(String mProfliePhoto) {
        this.mProfliePhoto = mProfliePhoto;
    }

    public String getmName() {
        if (mName != null)
            return mName;
        else
            throw new NullPointerException();
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmEmail() {
        if (mEmail != null)
            return mEmail;
        else
            throw new NullPointerException();
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public String getmGender() {
        return mGender;
    }

    public void setmGender(String mGender) {
        this.mGender = mGender;
    }

    public String getmDob() {
        return mDob;
    }

    public void setmDob(String mDob) {
        this.mDob = mDob;
    }
}